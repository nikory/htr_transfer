#include "adsorb.h"

void Tadsorbtion_graph::cook_a(int i, real Seff,real Qin)//! ����� ����� ���������� (�������� BFS) � ������ ����� �������, �����������, ������������ �����, �� ������� ���������. ������� ��������� ����� ���������, ��������� � ����������� ���� ��������� ������� ���� � ����� ���������-������.
{
 int y,n,*ord;
 std::set<Tadsorbtion_link_info>::iterator from;
 real pxy,dQ,N0;
 ord=&bfs_order[0];
 Tvol_adsorbtion_info *info_k,*info_y;
 y=ord[0];
 info_y=&(infos[y]);
 N0=1./info_y->count;
 info_y->pin=1;
 info_y->Sg=Seff;
 info_y->Qvol_in=Qin;
 info_y->update();
 n=size();
 for(i=1;i<n;i++)
 {
  y=ord[i];
  info_y=&(infos[y]);
  info_y->pin=0;
  info_y->Sg=0;
  info_y->Qvol_in=0;
  for(from=links_from[y].begin();from!=links_from[y].end();++from)
  {
   info_k=&(infos[from->node]);
   pxy=from->koef/info_k->count;
   info_y->pin+=info_k->pout*pxy;
   info_y->Sg+=info_k->Sg*pxy;
   dQ=info_k->Qvol_out*pxy;
//   dQ=Qin*from->koef*N0;
   info_y->Qvol_in+=dQ;
  }
  info_y->update();
 }
}

void Tadsorbtion_graph::dfs(int v)
{
 used[v] = true;
 for (std::set<Tadsorbtion_link_info>::iterator i=links[v].begin(); i!=links[v].end(); ++i)
 {
  if (!used[i->node])
  {
   dfs (i->node);
  }
 }
 bfs_order.push_back (v);
}

void Tadsorbtion_graph::topo_sort()
{
 int n=size(),i,j;
 bfs_order.clear();
 used.assign (n, false);
 for (i=0; i<n; ++i)
 if (!used[i])
  dfs (i);
 for(i=0;;i++)
 {
  j=n-i-1;
  if(j<=i)break;
  std::swap(bfs_order[i],bfs_order[j]);
 }
}

void Tadsorbtion_graph::bfs(int i)//! ����� ����� ���������� (�������� BFS) � ������ ����� �������, �����������, ������������ �����, �� ������� ���������. ������� ��������� ����� ���������, ��������� � ����������� ���� ��������� ������� ���� � ����� ���������-������.
 {
  Tvol_adsorbtion_info *info_x,*info_y,*info_k;
  std::queue<int> open;
  open.push(i);
  info_x=&(infos[i]);
  int x,y;
  real pxy;
  for(x=0;x<infos.size();x++)
  {
   used[x]=false;
  }
  while(!open.empty())
  {
   x=open.front();
   info_x=&(infos[x]);
   bfs_order.push_back(x);
   open.pop();
   for(auto yi=links[x].begin();yi!=links[x].end();yi++)
   {
    y=yi->node;
    if(!used[y])
    {
     used[y]=true;
     open.push(y);
    }
   }
  }
 }

void Tadsorbtion_graph::insert_fw(Tadsorbtion_graph::zone_iter ii, Tadsorbtion_graph::zone_iter jj)
 {
  Treg_zone_vol  *v,*vnext;
  Tadsorbtion_graph::zone_iter beg,end,curr,next;
  Tadsorbtion_link_info f;
  std::set<Tadsorbtion_link_info>::iterator fl;
  int k,knext,size;
  beg=ii;
  end=jj;
  if(ii->rz->my_type()==RZ_SURF)  {   ++beg;  }
  if(beg->rz->my_type()==RZ_SURF) { set_invalid(); return; }
  if(jj->rz->my_type()==RZ_VOL )  {   ++end;  }
// ��������� � ���� ����� ����; ��������� �������� �����������
  for(curr=beg;curr!=end;++curr)
  {
   if(curr->rz->my_type()==RZ_SURF) { break; }
   v=static_cast<Treg_zone_vol*>(curr->rz);
   size=zind.size();
   k=zind.push(v);
   if(k==size)
   {
    infos.push_back(Tvol_adsorbtion_info(v));
    infos.back().raylen_sum=curr->len;
    links.push_back(std::set<Tadsorbtion_link_info>());
   }
   else
   {
    infos[k].count+=1.;
    infos[k].raylen_sum+=curr->len;
   }
  }
// ��������� �����-�������� ����� ������
  for(curr=beg;curr!=end;++curr)
  {
   next=curr+1;
   vnext=static_cast<Treg_zone_vol*>(next->rz);
   if(vnext->my_type()==RZ_SURF) { break; }
   else { if(next==end) break; }
   v=static_cast<Treg_zone_vol*>(curr->rz);
   k=zind[v];
   knext=zind[vnext];
   f.node=knext;
   fl=links[k].find(f);
   if(fl==links[k].end())
   {
    f.koef=1.;
    links[k].insert(f);
   }
   else
   {
    fl->grow(1.);
   }
  }
 }

void Tadsorbtion_graph::insert_bw(Tadsorbtion_graph::zone_iter ii, Tadsorbtion_graph::zone_iter jj)
 {
  Treg_zone_vol  *v,*vnext;
  Tadsorbtion_graph::zone_iter beg,end,curr,next;
  Tadsorbtion_link_info f;
  std::set<Tadsorbtion_link_info>::iterator fl;
  int k,knext,size;
  beg=ii;
  end=jj;
  if(ii->rz->my_type()==RZ_SURF)  {   --beg;  }
  if(beg->rz->my_type()==RZ_SURF) { set_invalid(); return; }
  if(jj->rz->my_type()==RZ_VOL )  {   --end;  }
// ��������� � ���� ����� ����; ��������� �������� �����������
  for(curr=beg;curr!=end;--curr)
  {
   if(curr->rz->my_type()==RZ_SURF) { break; }
   v=static_cast<Treg_zone_vol*>(curr->rz);
   size=zind.size();
   k=zind.push(v);
   if(k==size)
   {
    infos.push_back(Tvol_adsorbtion_info(v));
    infos.back().raylen_sum=curr->len;
    links.push_back(std::set<Tadsorbtion_link_info>());
   }
   else
   {
    infos[k].count+=1.;
   }
   infos[k].raylen_sum+=curr->len;
  }
// ��������� �����-�������� ����� ������
  for(curr=beg;curr!=end;--curr)
  {
   next=curr-1;
   vnext=static_cast<Treg_zone_vol*>(next->rz);
   if(vnext->my_type()==RZ_SURF) { break; }
   else { if(next==end) break; }
   v=static_cast<Treg_zone_vol*>(curr->rz);
   k=zind[v];
   knext=zind[vnext];
   f.node=knext;
   fl=links[k].find(f);
   if(fl==links[k].end())
   {
    f.koef=1.;
    links[k].insert(f);
   }
   else
   {
    fl->grow(1.);
   }
  }
 }

void Tadsorbtion_graph::update()
 {
  int i,n;
  std::set<Tadsorbtion_link_info>::iterator vto;
  n=links.size();
  links_from.resize(n);
  for(i=0;i<n;i++)
  {
   for(vto=links[i].begin();vto!=links[i].end();vto++)
   {
    links_from[vto->node].insert(Tadsorbtion_link_info(i,vto->koef));
   }
  }
  n=infos.size();
//  cook(0,0);
  for(i=0;i<n;i++)
  {
   infos[i].update();
   if(links[i].size()==0)
   {
    last_vol=zind[i];
   }
   if(links_from[i].size()==0)
   {
    first_vol=zind[i];
    index_begin=zind[first_vol];
   }
  }
  used.resize(zind.size(),false);
 }

void Tadsorbtion_graph::fin()
 {
  int i,n;
  n=infos.size();
  for(i=0;i<n;i++)
  {
   infos[i].fin();
  }
 }

void Tadsorbtion_graph::cook(real x, real Qin)
 {
//  cook_a(index_begin);
  cook_a(index_begin,x*zfrom->get_vol_corr(), Qin);
 }

void Tadsorbtion_graph::cook_bfs_order()
 {
  topo_sort();
//  bfs(index_begin);
 }

Tvol_adsorbtion_info* Tadsorbtion_graph::get_adsorbtion_info(int v)
 {
  return &(infos[v]);
 }

Tvol_adsorbtion_info* Tadsorbtion_graph::get_adsorbtion_info(Treg_zone_vol* v)
 {
  int i=zind[v];
  if(i!=0xffffff)
  {
   return get_adsorbtion_info(i);
  }
  else
  {
   return 0;
  }
 }

void Tadsorbtion_graph::chk_infos()
{
 int i;
 real res=0;
 for(i=0;i<infos.size();i++)
 {
  res+=infos[i].pads;
 }
 res+=get_adsorbtion_info(last_vol)->pout;
 return;
}

real Tadsorbtion_graph::s2s_ads(Treg_zone_vol* v)
{
 if(zfrom->my_type()==RZ_VOL || zto->my_type()==RZ_VOL)
 {
  return 0;
 }
 Tvol_adsorbtion_info *info;
 info=get_adsorbtion_info(v);
 if(info!=0)
 {
  return info->weak;
 }
 else
 {
  return 0;
 }
}

bool check_valid(const Tadsorbtion_graph* g)
{
 if(g!=NULL)
 {
  return g->valid();
 }
 return false;
}
