#ifndef TOPKA_MODEL_H
#define TOPKA_MODEL_H

#include "cintintf/externs.h"
#include "serialize_inc.h"
#include <exception>
#include <iostream>

struct topka_model
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch& a,int)
 {
  a.template register_type<Tlim_domain>();
  a.template register_type<Treg_zone_surf>();
  a.template register_type<Treg_zone_vol>();
  a.template register_type<VolumeZone>();
  a.template register_type<SurfaceZone>();
  a.template register_type<Tand>();
  a.template register_type<Tor>();
  a.template register_type<Tnegation>();
  try
  {
  a & ARC_ADAPTOR(formulas);
  a & ARC_ADAPTOR(gabarites);
  a & ARC_ADAPTOR(domains);
  a & ARC_ADAPTOR(Rubik);
  a & ARC_ADAPTOR(tracker);
  a & ARC_ADAPTOR(rcoefs);
  }
  catch(std::exception &e)
  {
   std::cout<<e.what()<<endl;
  }
  catch(...)
  {
   cout<<"some";
  }
 }
protected:
 std::set<Tcondition*> &formulas;
 std::vector<Tlim_domain*> &domains;
 Touter_cube &gabarites;
 rubik &Rubik;
 Tray_tracker &tracker;
 Tradiation_coefs &rcoefs;
public:
 topka_model():
  formulas(FORMULASET),
  domains(ZONE_DOMAIN),
  gabarites(SGAB),
  Rubik(GRUBIK),
  tracker(GTRACKER),
  rcoefs(GRCOEFS){}
};

#endif