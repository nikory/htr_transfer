#include "Tradiation_coefs.h"
#include <math.h>
#include <fstream>

void Tradiation_coefs::check_x0(const char *fn, Tray_tracker& rt)
{
 using namespace std;
 rt.correct_vols();
 build_matrices_Glr(rt);
 ofstream out;
 out.open(fn);
 int i,j,n;
 real sumi,sumq,x,Vcor,V;
 Treg_zone* zi,*zj;
 out<<"Summ_str="<<endl;
 n=matr_X.dim();
 for(i=0;i<n;i++)
 {
  zi=matr_X.get_zone(i);
  if(zi->my_type()==RZ_SURF)
  {
   out<<zi->name<<": ";
   sumi=0;
   for(j=0;j<n;j++)
   {
    zj=matr_X.get_zone(j);
    if(zj->my_type()==RZ_SURF)
    {
     sumi+=rt(zi,zj)/rt(zi);
    }
   }
   Vcor=zi->get_vol_corr();
   V=zi->get_vol();
   if(abs(Vcor)>1e-10)
   {
    out<<sumi<<" Vi/Vi_corr="<<V/Vcor<<" Vi="<<V<<endl;
   }
   else
   {
    cout<<"Posible Error in "<<zi->name<<endl;
   }
  }
 }
 out<<"����� �� ��������:"<<endl;
 for(i=0;i<n;i++)
 {
  zi=matr_X.get_zone(i);
  if(zi->my_type()==RZ_SURF)
  {
   out<<zi->name<<": ";
   sumi=0;
   for(j=0;j<n;j++)
   {
    zj=matr_X.get_zone(j);
    if(zj->my_type()==RZ_SURF)
    {
     x=rt.X0(zj,zi);
     sumi+=x*zj->get_vol_corr()/zi->get_vol_corr();
//     sumi+=matr_X[j][i]*rt(zj)/rt(zi);
    }
   }
   out<<sumi<<endl;
  }
 }
 out<<"����� �� ������� �������� ���:"<<endl;
 for(i=0;i<n;i++)
 {
  zi=matr_X.get_zone(i);
  if(zi->my_type()==RZ_VOL)
  {
   out<<zi->name<<": ";
   sumi=0;
   for(j=0;j<n;j++)
   {
    zj=matr_X.get_zone(j);
    if(zj->my_type()==RZ_SURF)
    {
     sumi+=rt.X0(zi,zj);//*rt(zj)/rt(zi);
//     sumi+=matr_X[i][j]*exp(rt.adsl(zi,zj));
//     sumi+=matr_X[j][i]*rt(zj)/rt(zi);
    }
   }
   out<<sumi<<" Vi/Vi_corr="<<zi->get_vol()/zi->get_vol_corr()<<" Vi="<<zi->get_vol()<<" Vicorr="<<zi->get_vol_corr()<<endl;
  }
 }
 out<<"����� �� �������� �������� ���:"<<endl;
 sumi=0;
 sumq=0;
 for(i=0;i<n;i++)
 {
  zi=matr_X.get_zone(i);
  if(zi->my_type()==RZ_SURF)
  {
   out<<zi->name<<": ";
   sumq=0;
   for(j=0;j<n;j++)
   {
    zj=matr_X.get_zone(j);
    if(zj->my_type()==RZ_VOL)
    {
     x=rt(zj,zi)/rt(zj);
     sumi+=x*zj->get_vol_corr() * static_cast<Treg_zone_vol*>(zj)->props().Qv;
     sumq+=zj->get_vol_corr() * static_cast<Treg_zone_vol*>(zj)->props().Qv;
//     sumi+=matr_X[j][i]*exp(rt.adsl(zi,zj))*zj->get_vol()/zi->get_vol();
    }
   }
  out<<sumi<<" "<<sumq<<endl;
  }
 }
 out.close();
}

Tradiation_coefs::Tradiation_coefs():solver(1),solver_V(1),areas(1,1){}
void Tradiation_coefs::build_matrices_msq(Tray_tracker &rc)
{
/* const rubik* my_rubik;
 rubik::rzs_set_t::const_iterator zi,zj,zk,zone_begin,zone_end;
 rubik::rz_set_t::const_iterator szb,sze,szi,szj;
 Treg_zone_surf *i,*j,*k;
 Treg_zone *si,*sj;
 real regV,regS,inv_regS,x,x1,sqrtS,alph,SiSj,Si,Sj,al_i,al_j;
 my_rubik=rc.get_rubik();
 matr_A.set_index(my_rubik->zone_index_surf);
 matr_P.set_index(my_rubik->zone_index_surf);
 matr_R.set_index(my_rubik->zone_index_surf);
 matr_X.set_index(my_rubik->zone_index_com);
 regS=my_rubik->region_surface();
 regV=my_rubik->region_volume();
 inv_regS=1./regS;
 zone_begin=my_rubik->rzs_set.begin();
 zone_end=my_rubik->rzs_set.end();
 szb=my_rubik->rz_set.begin();
 sze=my_rubik->rz_set.end();
 //���������� ������� ������� ������������� X
 for(szi=szb;szi!=sze;szi++)
 {
  si=*szi;
  for(szj=szb;szj!=sze;szj++)
  {
   sj=*szj;
   x=rc(si,sj)/(rc(si)+rc(sj))*2;
   matr_X(si,sj)=x;
  }
 }
 //���������� ������� ������� ������������� R
 for(zi=zone_begin;zi!=zone_end;zi++)
 {
  i=*zi;
  sqrtS=M_PI*sqrt(i->get_vol()/regS);
  for(zj=zone_begin;zj!=zone_end;zj++)
  {
   j=*zj;
   if(i==j)x=0;
   else x=matr_X(i,j);
   matr_R(i,j)=sqrtS*x;
  }
 }
 //���������� ������� ������������� P � ������� A
 for(zi=zone_begin;zi!=zone_end;zi++)
 {
  i=*zi;
  Si=i->get_vol();
  Si=sqrt(regS/Si);
  al_i=i->props().alpha;
  for(zj=zone_begin;zj!=zone_end;zj++)
  {
   j=*zj;
   Sj=j->get_vol();
   Sj=sqrt(regS/Sj);
   al_j=j->props().alpha;
   matr_P(i,j)=0.;
   for(zk=zone_begin;zk!=zone_end;zk++)
   {
    k=*zk;
    alph=sqr(k->props().alpha);
    matr_P(i,j)+=matr_R(i,k)*matr_R(j,k)*alph;
   }
   SiSj=Si*Sj;
   matr_A(i,j)=(SiSj*matr_P(i,j)-(Sj*al_i*matr_R(i,j)+Si*al_j*matr_R(j,i)));//*inv_regS;
   if(i==j)matr_A(i,j)+=1.;
  }
 } */
}

void Tradiation_coefs::alloc_matrices(Tray_tracker& rt)
{
 rubik *my_rubik=rt.get_rubik();
 matr_A.set_index(my_rubik->zone_index_surf);
 matr_X.set_index(my_rubik->zone_index_com);
 matr_S.set_index(my_rubik->zone_index_com);
 matr_V.set_index(my_rubik->zone_index_vol);
 map_C.set_index(my_rubik->zone_index_surf);
 map_V.set_index(my_rubik->zone_index_vol);
 areas.reset_to_size(my_rubik->rzv_set.size(),my_rubik->rzs_set.size());
}

void Tradiation_coefs::build_matrices_Glr(Tray_tracker& rt)
{
 rubik *my_rubik;
 rubik::rz_set_t::const_iterator czb,cze,czi,czj;
 rubik::rzs_set_t::const_iterator szb,sze,szi,szj;
 Treg_zone *zi,*zj;
 Treg_zone_surf *si,*sj;
 real alpha_i,alpha_j,A_i,A_j,x, eps_i,eps_j;
 my_rubik=rt.get_rubik();
 czb=my_rubik->rz_set.begin();
 cze=my_rubik->rz_set.end();
 szb=my_rubik->rzs_set.begin();
 sze=my_rubik->rzs_set.end();
 for(czi=czb;czi!=cze;czi++)
 {
  zi=*czi;
  for(czj=czb;czj!=cze;czj++)
  {
   zj=*czj;
   x=rt.X0(zi,zj);
   matr_X(zi,zj)=x;
  }
 }
 account_ads(rt);
 for(szi=szb;szi!=sze;szi++)
 {
  si=*szi;
  A_i=si->get_vol_corr();
  alpha_i=si->props().alpha;
  eps_i=si->props().epsilon;
  matr_A(si,si)=1;
  for(szj=szb;szj!=sze;szj++)
  {
   sj=*szj;
   if(si!=sj)
   {
    eps_j=sj->props().epsilon;
    alpha_j=sj->props().alpha;
    A_j=sj->get_vol_corr();
    if(eps_j<1e-20)eps_j=1e-20;
//    matr_A(si,sj)=-alpha_i*A_j*matr_X(sj,si)/A_i;
    matr_A(si,sj)=-eps_i*alpha_j*A_j*matr_X(sj,si)/(A_i*eps_j);
   }
  }
 }
}

void Tradiation_coefs::build_matrices(Tray_tracker &rt)
{
 build_matrices_Glr(rt);
}
void Tradiation_coefs::factor_system()
{
 solver.reset_to_dim(matr_A.dim());
 solver.update(matr_A);
}   

void Tradiation_coefs::solve_system(ssReal_map_t& out)
{
 out=map_C;
 solver.mul(&map_C[0],&out[0]);
}

void Tradiation_coefs::factor_system_V()
{
 solver_V.reset_to_dim(matr_V.dim());
 solver_V.update(matr_V);
}

void Tradiation_coefs::solve_system_V(vvReal_map_t& out)
{
 out=map_V;
 solver_V.mul(&map_V[0],&out[0]);
}

void Tradiation_coefs::build_freecf_msq(Tray_tracker &rc)
{
/* rubik::rzv_set_t::const_iterator vi,vb,ve;
 rubik::rzs_set_t::const_iterator sk,ak,sb,se;
 Treg_zone_surf *k,*j;
 Treg_zone_vol *i;
 const rubik* my_rubik;
 real hv,y,sqrtS,regS,eIok,Sk,Sk1,Lk,alpha_k,epsilon_k;
 my_rubik=rc.get_rubik();
 regS=my_rubik->region_surface();
 map_C.set_index(my_rubik->zone_index_surf);
 map_F.set_index(my_rubik->zone_index_surf);
 map_L.set_index(my_rubik->zone_index_surf);
 vb=my_rubik->rzv_set.begin();
 ve=my_rubik->rzv_set.end();
 sb=my_rubik->rzs_set.begin();
 se=my_rubik->rzs_set.end();
 for(sk=sb;sk!=se;sk++)
 {
  k=*sk;
  alpha_k=k->props().alpha;
  epsilon_k=k->props().epsilon;
  hv=0;
  for(vi=vb;vi!=ve;vi++)
  {
   i=*vi;
   y=matr_X(i,k);
   hv+=i->props().Qv*i->get_vol()*y;
  }
  Sk=k->get_vol();
  Sk1=1./Sk;
  sqrtS=sqrt(regS*Sk1);
  eIok=k->props().Qs * epsilon_k*Sk;
  map_F(k)=sqrtS*(eIok + alpha_k*hv)/regS;
 }
 for(sk=sb;sk!=se;sk++)
 {
  k=*sk;
  Lk=0;
  for(ak=sb;ak!=se;ak++)
  {
   j=*ak;
   Lk+=map_F(j)*matr_R(j,k);
  }
  map_L(k)=Lk;
  map_C(k)=map_F(k)+sqrtS*alpha_k*Lk;
 }*/
}

void Tradiation_coefs::build_freecf_Glr(Tray_tracker &rc)
{
 int iv,nv;
 rubik *my_rubik;
 rubik::rzv_set_t::const_iterator vzb,vze,vzi,vzj;
 rubik::rzs_set_t::const_iterator szb,sze,szi,szj;
 Treg_zone_vol *vi,*vj,*firstvol;
 Treg_zone_surf *si,*sj;
 Tvol_adsorbtion_info *info;
 real alpha_i,A_i,A_j,V_j,eps_i,eps_j,L,K,Qi,Qj,Qj1,L_jij,Tj,x0,x1,kads,Aji;
 Tadsorbtion_graph *ra;
 my_rubik=rc.get_rubik();
 vzb=my_rubik->rzv_set.begin();
 vze=my_rubik->rzv_set.end();
 szb=my_rubik->rzs_set.begin();
 sze=my_rubik->rzs_set.end();
 for(szi=szb;szi!=sze;szi++)
 {
  si=*szi;
  eps_i=si->props().epsilon;
  alpha_i=si->props().alpha;
  Qi=si->props().Qs;
  A_i=si->get_vol_corr();
  L=0;
  for(szj=szb;szj!=sze;szj++)
  {
   sj=*szj;
   if(si!=sj)
   {
    x0=rc.X0(sj,si);
    ra=rc.get_adsorbtion_graph(sj,si);
    if(!check_valid(ra))
    {
     continue;
    }
    Qj=ra->get_adsorbtion_info(ra->last_vol)->Qvol_out;
    L+=Qj;
   }
  }
  map_C(si)=-Qi+eps_i/A_i*L;
 }
}

void Tradiation_coefs::build_freecf(Tray_tracker& rt)
{
 build_freecf_Glr(rt);
}

//#define exp(x) (1+(x))

void Tradiation_coefs::volume_rad(Tray_tracker& rt)
{
/* rubik* r=rt.get_rubik();
 real Sfull=r->estimate_full_Sq();
 rubik::rzs_vec_t::iterator sii,sji,sbi,sei;
 rubik::rzv_vec_t::iterator vii,vbi,vei;
 Tray_adsorbtion* ra;
 Tray_adsorbtion::len_iter lb,le,li;
 real aji,Sj,lji,siga;
 Treg_zone_surf *si,*sj;
 Treg_zone_vol *vi;
 sbi=r->rzs_vec.begin();
 sei=r->rzs_vec.end();
 vbi=r->rzv_vec.begin();
 vei=r->rzv_vec.end();
 for(vii=vbi;vii!=vei;vii++)
 {
  vi=*vii;
  vi->props().sigma_a_eff=0;
 }
 for(sii=sbi;sii!=sei;sii++)
 {
  si=*sii;
  for(sji=sbi;sji!=sei;sji++)
  {
   sj=*sji;
   if(si!=sj)
   {
    ra=rt.get_ray_adsorbtion(sj,si);
    if(ra!=NULL)
    {
     aji=rt.X0(sj,si);
     Sj=sj->get_vol_corr();
     lb=ra->begin();
     le=ra->end();
     for(li=lb;li!=le;li++)
     {
      vi=li->first;
      lji=ra->lsa(vi);
      vi->props().sigma_a_eff+=aji*Sj*exp(-lji);
     }
    }
   }
  }
 }
 for(vii=vbi;vii!=vei;vii++)
 {
  vi=*vii;
  siga=vi->props().sigma_a_eff;
  vi->props().sigma_a_eff=(1-siga/Sfull)*Sfull/vi->get_vol_corr();
 }*/
}


/*void Tradiation_coefs::account_ads(Tray_tracker &rt)
{
 rubik *my_rubik;
 rubik::rz_set_t::const_iterator czb,cze,czi,czj;
 Treg_zone *zi,*zj;
 real alpha_i,A_i,A_j,x;
 my_rubik=rt.get_rubik();
 matr_S.set_index(my_rubik->zone_index_com);
 czb=my_rubik->rz_set.begin();
 cze=my_rubik->rz_set.end();
 for(czi=czb;czi!=cze;czi++)
 {
  zi=*czi;
  if(zi->my_type()==RZ_SURF)
  {
   for(czj=czb;czj!=cze;czj++)
   {
    zj=*czj;
    matr_S(zj,zi)=rt.adsl(zj,zi);
    matr_X(zj,zi)=rt(zj,zi)/rt(zj)*exp(-matr_S(zj,zi));
   }
  }
 }
}*/

void Tradiation_coefs::account_ads(Tray_tracker& rt)
{
 int is,js,nz,iv,nv;
 real p,x0;
 Treg_zone *si,*sj;
 Treg_zone_vol *lastvol;
 Tvol_adsorbtion_info *info;
 Tadsorbtion_graph *ga;
 rubik *r;
 r=rt.get_rubik();
 nz=r->rz_vec.size();
 areas=0.;
 for(is=0;is<nz;is++)
 {
  si=r->rz_vec[is];
  for(js=0;js<nz;js++)
  {
   if(is!=js)
   {
    sj=r->rz_vec[js];
    ga=rt.get_adsorbtion_graph(si,sj);
    if(!check_valid(ga))
    {
     continue;
    }
    lastvol=ga->last_vol;
    info=ga->get_adsorbtion_info(lastvol);
    switch(sj->my_type())
    {
     case(RZ_VOL):
      p=info->pin;
      break;
     case(RZ_SURF):
      p=info->pout;
      break;
    }
    ga->chk_infos();
    x0=rt.X0(si,sj);
    matr_S(si,sj)=p;//info->pout;
    matr_X(si,sj)=x0*p;//info->pout;
    nv=ga->size();
    x0=rt.X0(sj,si);
/*    if(si->my_type()==RZ_SURF && sj->my_type()==RZ_SURF)
    {
     for(iv=0;iv<nv;iv++)
     {
      info=ga->get_adsorbtion_info(iv);
      lastvol=info->vol_zone;
      vol_area(static_cast<Treg_zone_surf*>(sj),lastvol)+=x0*ga->s2s_ads(lastvol);
     }
    }*/
   }
  }
 }
/* nv=r->rzv_vec.size();
 nz=r->rzs_vec.size();
 for(iv=0;iv<nv;iv++)
 {
  lastvol=r->rzv_vec[iv];
  for(is=0;is<nz;is++)
  {
   si=r->rzs_vec[is];
   p=vol_area(static_cast<Treg_zone_surf*>(si),lastvol);
   vol_area(static_cast<Treg_zone_surf*>(si),lastvol)=si->get_vol_corr()*(1-p);
  }
 }*/
}

real Tradiation_coefs::vol_area(Treg_zone_surf* s, Treg_zone_vol* v)const
{
 int i,j;
 i=v->vol_index;
 j=s->surf_index;
 return areas[i][j];
}

real& Tradiation_coefs::vol_area(Treg_zone_surf* s, Treg_zone_vol* v)
{
 int i,j;
 i=v->vol_index;
 j=s->surf_index;
 return areas[i][j];
}

//#undef exp


real Tradiation_coefs::X(Treg_zone *i, Treg_zone *j)const
{
 return matr_X(i,j);
}
real Tradiation_coefs::S(Treg_zone *i, Treg_zone *j)const
{
 return matr_S(i,j);
}
#define TRC_PT(M) void Tradiation_coefs::pt##M(std::ostream &s) const {s<<"matr_"<<#M<<std::endl;s<<matr_##M;}
#define TRM_PT(M) void Tradiation_coefs::pt##M(std::ostream &s) const {s<<"map_"<<#M<<std::endl;s<<map_##M;}


TRC_PT(R)
TRC_PT(X)
TRC_PT(P)
TRC_PT(A)
TRC_PT(S)

TRM_PT(F)
TRM_PT(L)
TRM_PT(C)
