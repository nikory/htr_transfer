#ifndef Tzone_tracking_h
#define Tzone_tracking_h

#include <algorithm>
#include <list>
#include "line.h"
#include "Treg_zone.h"

class Tlink_coef
{
public:
 const Treg_zone *sink,*target;
 real value;
};

/*!
����� ������������ ������ � ���������. ������� - ��� ������������ 
���� � ������������ �� ��������������� ��� � ����������� � �����������. 
���� ������������� � ������� ���������� � ����������� ����.
*/
class Tzone_tracking
{
public:
 typedef std::vector<Treg_info> info_con_t; //!< ��� ��������� �����
protected:
 info_con_t info;
 Tline my_ray;
 real init_weight;
 bool finished;
public:
//! �������� ���������
 inline void flush()
 {
  info.clear();
  info.reserve(10);
  finished=false;
 }
 Tzone_tracking(){flush();}
 Tzone_tracking(const Tline& l, real genw=1.):
  my_ray(l),
  init_weight(genw)
 {
  flush();
 }
//! ���������� � �������� � ������ ��������� ����������� � ����� 
 bool process(const Treg_zone& rz)
 {
  if(finished)
  {
   return false;
  }
  else
  {
   Treg_info ri;
   ri=rz.reg_info(my_ray);
   if(ri.reg) 
   {
    info.push_back(ri);
   }
   return true;
  }
 }
//! ��������� ������� ������ ����� ���
 void finish_process()
 {
  std::sort(info.begin(),info.end());
  finished=true;
 }
//! �������� �� ������ ���������� � �����������
 info_con_t::const_iterator begin()const{return info.begin();}
//! �������� �� ����� ����������
 info_con_t::const_iterator end()const{return info.end();}
 Tlink_coef weight(const info_con_t::const_iterator& sink, const info_con_t::const_iterator& target)const
 {
  Tlink_coef res;
  info_con_t::const_iterator s,t;
  int inc;
  real w;
  if(finished)
  {
   res.sink=sink->rz;
   res.target=target->rz;
   info_con_t::const_iterator it;
   inc=1;
   if(sink->x_line > target->x_line)inc=-1;
   w=abs(init_weight*sink->sink_factor);
   for(it=sink;(it!=target);it+=inc)
   {
    if(it!=sink)
    {
     w=w*it->weight_factor;
    }
   }                 
   res.value=fabs(w);
   return res;
  }
  else
  {
   res.sink=0;
   res.target=0;
   res.value=0;
   return res;
  }
 }
};



#endif