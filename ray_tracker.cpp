#include "ray_tracker.h"
#include <time.h>
#include <iostream>

void Tray_tracker::set_rubik(rubik &rub)
{
 fullWray=0;
 fullLray=0;
 my_rubik=&rub;
 matr_G.set_index(rub.zone_index_com);
 ray_count.set_index(my_rubik->zone_index_com);
 ray_length.set_index(my_rubik->zone_index_vol);
 my_rubik->index_zones();
}

rubik* Tray_tracker::get_rubik() const 
{
 return my_rubik;
}

//#include "cintintf/oarch_cint.h"
extern void BuildNameMap();

#include<conio.h>

enum {MC_BREAK};

int process_kbhit()
{
 std::cout<<"select from the follow:"<<std::endl;
 std::cout<<"1. break"<<std::endl;
 return MC_BREAK;
}

void Tray_tracker::generate_link_mtr(int m, int q)
{
 using namespace std;
 int i;
 static std::pair<const Tplanesurf*, Tline> ray;
 time_t ti,ti0;
 static TV3 pt;
 pt=0.;
 real x; 
 time(&ti0);
 BuildNameMap();
 cout<<"Starting MonteCarlo with "<<m<<" experiments..."<<endl;
 for(i=0;i<m;i+=1)
 {
  ray=my_rubik->get_outer_cube().random_ray_from_bound();
  ray.second.inc_id();
  ray.second.w=fabs(dot(ray.first->normv(pt),ray.second.direct));
  fix_ray(ray.second);
  if(i%q==0)
  {
//   init_rndgen_state();
   time(&ti);
   std::cout<<i<<" done... Time is "<<real(ti-ti0)/60<<" minutes... Please wait..."<<std::endl;
   ray.second.flush();
  }
  if(kbhit())
  {
   int ret=process_kbhit();
   if(ret==MC_BREAK) break;
  }
 }
 nexp=m;
 update_ads();
 correct_vols();
 X0();
 time(&ti);
 cout<<"MonteCarlo with "<<m<<" experiments finished. Total time is "<<real(ti-ti0)<<" sec.\n";
}

void Tray_tracker::fix_ray(const Tline &ray)
{
 // ���������� ����, � �������� ������������ ��� � ����������� ����� �����-������ ���� �� ��������� � ������ ���� �� ����� ������, ����� ����� ������������� ���� � ������� ����������� �� ������.
 // ��� ����, ����� ��������� ������� ���������, ������ ������������ ���� ������ �������� ������������� ������������, ������� ����� 1 ��� �������� ���� � 0 ��� �������������.
 // ��� ����, ����� ��������� ���������� ��� ����, ����� �� ������������ ��� ������� ������������ ����, ������� ��� "���������".
 // ��� ����, ����� ������� ��� ���� ����� �����������: ������� � ������� ����� ����������� ����, ��������������� ���, ��� ���������� ���� � ������������ ������������ ���� ���, ������� ��������� ����� "�����������" ����� � �������, ������� ������� ������ ���� �� ������������ ������.
 real icos=1,x,xtmp,rctmp;
 Treg_info ri;
 Tzone_tracker zt(ray,icos);
 Treg_info reg_outer_cube;
 Tzone_tracker::info_con_t::const_iterator rb,rb1,re,sink,target;
 std::set<Treg_zone*>::const_iterator zb,ze,zi,zj;
 Treg_zone *ztarg,*zsink;
 static Treg_zone *rrz;
 zb=my_rubik->rz_set.begin();
 ze=my_rubik->rz_set.end();
 reg_outer_cube=my_rubik->get_outer_cube_zone().reg_info(ray);
 if(!(reg_outer_cube.reg))
 {
  cout<<"error here, line dir=("<<ray.direct<<")"<<endl;
  return;
 }
 icos=ray.w;
 for(zi=zb;zi!=ze;zi++)
 {
  rrz=*zi;
  zt.process(**zi);
 }
 zt.finish_process();
 if(zt.size()>0)
 {
  if(zt.begin()->rz->my_type()!=RZ_SURF || zt.rbegin()->rz->my_type()!=RZ_SURF)
  {
    std::cout<<"[";
    for(sink=zt.begin();sink!=zt.end();sink++)
    {
     std::cout<<(*sink).rz->name<<" ";
    }
    std::cout<<"]"<<std::endl;
    std::cout<<"ray.p0="<<ray.p0<<std::endl;
    std::cout<<"ray.Omega="<<ray.Omega<<std::endl;
    std::cout<<"ray.direct="<<ray.direct<<std::endl;
   zt.flush();
   for(zi=zb;zi!=ze;zi++)
   {
    rrz=*zi;
    ri=rrz->reg_info(ray);
    zt.process(*rrz);
    if(ri.reg)
    {
     std::cout<<rrz->name<<'\t';
     std::cout<<ri.x_line<<'\t'<<ri.len<<std::endl;
    }
   } 
   return;
  }
 }
 fullWray+=icos;
 if(reg_outer_cube.len>1000.||reg_outer_cube.len<0.)
 {
  cout<<"error here, reg_outer_cube.len="<<reg_outer_cube.len<<endl;
  reg_outer_cube=my_rubik->get_outer_cube_zone().reg_info(ray);
  return;
 }
 fullLray+=reg_outer_cube.len;
 adsorb(zt);
// ads_array.update();
 rb=zt.begin();
 re=zt.end();
 for(sink=rb;sink!=re;sink++)
 {
  zsink=sink->rz;
  switch(sink->rz->my_type())
  {
   case(RZ_VOL):
    rb1=sink; 
    ray_length(static_cast<Treg_zone_vol*>(zsink))+=sink->len;
    break;
   case(RZ_SURF):
    rb1=rb; break;
  }
  ray_count(zsink)+=sink->sink_factor*icos;
  for(target=rb1;target!=re;target++)
  {
   ztarg=target->rz;                         
   x=icos*(zt.weight(sink,target)).value;
//   x=(zt.weight(sink,target)).value*exp(-(zsink,ztarg));
   matr_G(zsink,ztarg)+=x;
  }
 }
/* if(zt.size()>0)
 {
  if(zt.begin()->rz->my_type()!=RZ_SURF || zt.rbegin()->rz->my_type()!=RZ_SURF)
  {
   zt.flush();
   for(zi=zb;zi!=ze;zi++)
   {
    rrz=*zi;
    zt.process(**zi);
   }
  }
 }*/
/* if(zt.size()>0)
 {
 for(zi=zb;zi!=ze;zi++)
 {
  if((*zi)->my_type()==RZ_SURF)
  {
   xtmp=0;
   for(zj=zb;zj!=ze;zj++)
   {
    if((*zj)->my_type()==RZ_SURF)
    {
     xtmp+=matr_G(*zi,*zj);
    }
   }
   rctmp=ray_count(*zi);
   if(abs(xtmp-rctmp)>1e-10)
   {
    std::cout<<"zone "<<(*zi)->name<<"\t"<<"ray# "<<ray.id<<std::endl;
    std::cout<<"ray.p0="<<ray.p0<<std::endl;
    std::cout<<"ray.Omega="<<ray.Omega<<std::endl;
    std::cout<<"ray.direct="<<ray.direct<<std::endl;
    std::cout<<"xtmp="<<xtmp<<std::endl;
    std::cout<<"rctmp="<<rctmp<<std::endl;
    std::cout<<"xtmp-rctmp="<<xtmp-rctmp<<std::endl;
    rb=zt.begin();
    re=zt.end();
    std::cout<<"[";
    for(sink=rb;sink!=re;sink++)
    {
     std::cout<<(*sink).rz->name<<" ";
    }
    std::cout<<"]"<<std::endl;
   }
  }
 }
 }*/
}
real Tray_tracker::operator()(Treg_zone *i, Treg_zone *j)
{
 return matr_G(i,j);
}

real Tray_tracker::operator()(Treg_zone *i)
{
 return ray_count(i);
}

void Tray_tracker::update_vols()
{
/* int vct,sct;
 real Vc,Sc;
 rubik::rzs_set_t::const_iterator s,sb,se;
 rubik::rzv_set_t::const_iterator v,vb,ve;
 sb=my_rubik->rzs_set.begin();
 se=my_rubik->rzs_set.end();
 vb=my_rubik->rzv_set.begin();
 ve=my_rubik->rzv_set.end();
 vct=0;
 sct=0;
 for(s=sb;s!=se;s++)
 {
  sct+=ray_count(*s);
 }
 for(v=vb;v!=ve;v++)
 {
  vct+=ray_count(*v);
 }
 Vc=my_rubik->get_outer_cube().volume();
 Sc=my_rubik->get_outer_cube().area();
 my_rubik->region_volume(real(vct)/nexp*Vc);
 my_rubik->region_surface(real(sct)/nexp*Sc);
 for(s=sb;s!=se;s++)
 {
  (*s)->set_vol(ray_count(*s)/my_rubik->region_surface());
 }
 for(v=vb;v!=ve;v++)
 {
  (*v)->set_vol(ray_count(*v)/my_rubik->region_volume());
 }   */
}

/*void Tray_tracker::adsorb(Tzone_tracker &zt)
{
 trz_iter_t i,j,k;
 trz_iter_t b,e; 
 b=zt.begin();
 e=zt.end();
 real x0,x;
 if(b!=e)x0=b->x_line;
 for(i=b;i!=e;i++)
 {
  for(j=i;j!=e;j++)
  {
   for(k=i;k!=j+1;k++)
   {
    if(k->rz->my_type()!=RZ_VOL) continue;
    x=k->x_line;
    ads_array.insert(i->rz,j->rz,static_cast<Treg_zone_vol*>(k->rz),k->len,x-x0);
    ads_array.insert(j->rz,i->rz,static_cast<Treg_zone_vol*>(k->rz),k->len,x0-x);
   }
  }
 }
}*/

void Tray_tracker::adsorb(Tzone_tracker &zt)
{
 trz_iter_t i,j,k;
 trz_iter_t b,e; 
 b=zt.begin();
 e=zt.end();
 real x0,x;
 if(b!=e)x0=b->x_line;
 for(i=b;i!=e;i++)
 {
  if(i->rz->my_type()!=RZ_SURF)
  {
   continue;
  }
  for(j=i+1;j!=e;j++)
  {
   if(j->rz->my_type()!=RZ_SURF)
   {
    continue;
   }
   ads_array.insert_fw(i,j);
   ads_array.insert_bw(j,i);
   i=j;
  }
 }
}

/*real Tray_tracker::adsl(Treg_zone *i, Treg_zone *j)
{
 real res;
 Tray_adsorbtion* ra;
 ra=ads_array(i,j);
 if(ra==0)res=0;
 else res=(*ra)();
 return res;
}*/

/*Tray_adsorbtion* Tray_tracker::get_ray_adsorbtion(Treg_zone *i, Treg_zone *j)
{
 return ads_array(i,j);
}*/

Tadsorbtion_graph* Tray_tracker::get_adsorbtion_graph(Treg_zone *i, Treg_zone *j)
{
 return ads_array(i,j);
}

void Tray_tracker::update_ads()
{
 ads_array.update();
}

void Tray_tracker::prepare_ads()
{
 ads_array.prepare();
}


void Tray_tracker::ptstat(std::ostream& s)
{
 s<<"Matrix of interzonal exchange:\n"; 
 s<<matr_G<<endl;
 s<<"Zonal sink power:\n";
 s<<ray_count;
}

void Tray_tracker::correct_vols()
{
 using namespace std;
 real fS,fV,nS,nV,A;
 rubik::rz_set_t::iterator zi,zb,ze;
 rubik::rzs_set_t::iterator si,sb,se;
 rubik::rzv_set_t::iterator vi,vb,ve;
// fS=my_rubik->estimate_full_Sq();
// fV=my_rubik->estimate_full_Vol();
 fS=my_rubik->get_outer_cube().area();
 fV=my_rubik->get_outer_cube().volume();
 zb=my_rubik->rz_set.begin();
 ze=my_rubik->rz_set.end();
 sb=my_rubik->rzs_set.begin();
 se=my_rubik->rzs_set.end();
 vb=my_rubik->rzv_set.begin();
 ve=my_rubik->rzv_set.end();
 nS=fullWray*2.;
 nV=fullLray;
/* cout<<"cv1"<<endl;
 for(si=sb;si!=se;si++)
 {
  nS+=(*this)(*si);
 }
 cout<<"cv2"<<endl;
 for(vi=vb;vi!=ve;vi++)
 {
  nV+=(*this)(*vi);
 }*/
// cout<<"cv3"<<endl;
 for(si=sb;si!=se;si++)
 {
  A=fS*(*this)(*si)/nS;
  (*si)->set_vol_corr(A);
 }
// cout<<"cv4"<<endl;
/* for(vi=vb;vi!=ve;vi++)
 {
  A=fV*(*this)(*vi)/nV;
  (*vi)->set_vol_corr(A);
 }*/
 for(vi=vb;vi!=ve;vi++)
 {
  A=fV*ray_length(*vi)/nV;
  (*vi)->set_vol_corr(A);
 }
// cout<<"cv5"<<endl;
}

void Tray_tracker::X0()
{
 matr_X0=matr_G;
 rubik::rz_set_t::iterator zi,zj,zb,ze;
 zb=my_rubik->rz_set.begin();
 ze=my_rubik->rz_set.end();
 for(zi=zb;zi!=ze;zi++)
 {
  for(zj=zb;zj!=ze;zj++) 
  {
   matr_X0(*zi,*zj)=matr_G(*zi,*zj)/ray_count(*zi);
  }
 }
}

real Tray_tracker::X0(Treg_zone *i, Treg_zone *j)
{
 return matr_X0(i,j);
}