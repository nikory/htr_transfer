#ifndef MC_PHYS_CONST_H
#define MC_PHYS_CONST_H

const double BoltsmanK = 1.386504e-23; //!< ��/(�� �) -- ���������� ���������
const double PlankH = 6.626068960e-34; //!< ��-� -- ���������� ������ (��� �����)
const double DirakH = 1.054571628e-34; //!< ��-� -- ���������� ������ (h c ������)
const double StephanBoltsmanSigma = 5.6704e-8; //!< ��/(�2*�4) -- ���������� �������-���������
const double LigthVelocity = 3e8;       //!< �/� -- �������� ����� � �������
const double KelvinMinusCelsium = 273.15;  //!< ������� �� ������� � ��������

inline double Celsium2Kelvin(double C)
{
 return C+KelvinMinusCelsium;
}
inline double Kelvin2Celsium(double K)
{
 return K-KelvinMinusCelsium;
}


#endif
