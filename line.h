#ifndef LINE_H
#define LINE_H

#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>
#include "tv3.h"
#include "rand_f.h"
#include "decart.h"


//!������ ������ � ������������
class Tline
{
public:
 static int id;
 TV3 p0;//!�����, ����� ������� �������� ������
 TV2 Omega;//!����������� ������ Omega[[1]]=fi(az) Omega[[2]]=theta(tan)
 TV3 direct;//!{cos[fi]sin[teta],sin[fi]sin[teta],cos[teta]}
 real w;
 inline Tline(){p0=0.;Omega=0.;update();w=1;}
 inline Tline(const Tline& l)
 {
  p0=l.p0;Omega=l.Omega;direct=l.direct;w=l.w;
 }
 inline Tline& operator=(const Tline& l)
 {
	p0=l.p0;
	Omega=l.Omega;
	direct=l.direct;
	w=l.w;
	return *this;
 }
// int is_intersect(const typename Tsvol& v)const{return v.is_intersect(*this);}
 inline void update()
 {
  real c3=sin(Omega(1));
  direct(0)=cos(Omega(0))*c3;
  direct(1)=sin(Omega(0))*c3;
  direct(2)=cos(Omega(1));
 }
 inline void update_ang()
 {
  real c3;
  Omega(1)=acos(direct(2));
  c3=direct(0);
  if(fabs(c3)>1e-20)
  {
   if(c3>0)
    Omega(0)=atan(direct(1)/c3);
   else
    Omega(0)=M_PI-atan(direct(1)/c3);
  }
  else
   Omega(0)=M_PI_2*sign(1.,direct(1));
 }
 inline TV3 get_pt(real d)const 
 {
  return p0+d*direct;
 }
 void flush(){id=0;}
 void inc_id(){id++;}
 void dec_id(){id--;}
};

inline Tline rnd_line(const TV3& pt)
{
 Tline res;
 double x,y;
 res.p0=pt;
 x=random(0.,2.*M_PI);
 y=random(0.,1.);
 res.Omega(0)=x;
 res.Omega(1)=acos(1.-2.*y);
 res.update();
/* res.direct(2)/=1.;
 res.direct/=norm(res.direct);
 res.update_ang();*/
 return res;
}

inline Tline line2p(const TV3& pt1, const TV3& pt2)
{
 Tline l;
 TV3 r;
 real d;
 r=pt2-pt1;
 d=sqrt(dot(r,r));
 r/=d;
 l.direct=r;
 l.p0=(pt1+pt2)*0.5;
 l.update_ang();
 return l;
};

/*!������� ��������� �����, ���������� ����� ����� l.p0, 
�������������� �� �������� ���� ������� ����� � l.dir*/
inline Tline rnd_line(const Tline& l)
{
 Tline res;
// Decart_system ds; //! ��� �������������� ����������� ������� ��������� �������
// ds.rotate_z_to(l.Omega(0)); //! ����������� ��� z ������� ��������� � ������������ ������ l
// ds.rotate_x_to(l.Omega(1)); //! ����������� ��������� ������� ������ ��� z, ����� ������ x
res=rnd_line(l.p0);         //! �������� ����� ����� l.p0 ������, �������������� �� �������� ���� � ��� z ������� ds
// res.p0=l.p0;
// res.Omega(0)=random(0,2*M_PI);
// res.Omega(1)=asin(sqrt(random_e()));
// res.update();
// res.direct=ds.convert_from(res.direct);//! ����������� ����������� ��������� ������ � ���������� ������� ���������
// res.direct-=ds.inip();
// res.update_ang();           //! ����� ���� ��������� ���� ����������� ������ � ���������� �������
// res.Omega+=l.Omega;
// res.update();           //! ����� ���� ��������� ���� ����������� ������ � ���������� �������
 return res;                 //! ��� � ���� ������� ���������.
}

#endif