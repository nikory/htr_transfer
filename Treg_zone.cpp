#include "Treg_zone.h"

Treg_info REG_INFO_TMP;

Treg_zone::Treg_zone(void)
{
}

Treg_zone::~Treg_zone(void)
{
}

bool Treg_zone_surf::operator ()(const Tline &l)
{
 bool result;
 result=vol->lints(l);
 Tline_stp* ls=&vol->ipoint();
 return ls->find(sv[0]) && result;
}


Treg_info Treg_zone_surf::reg_info(const Tline& l)
{
 Treg_info res;
 res.reg=(*this)(l);
 if(res.reg)
 {
  Tsurf::iptcon_t& ipi(sv[0]->ipoint(l));
  TV3 pt=l.get_pt(ipi[0]);
  res.norm=sv[0]->normv(pt);
  real c=fabs(dot(res.norm,l.direct));
  res.cosfi=c;
  res.len=0;
  res.pt=pt;
  res.rz=this;
  res.sink_factor=1;
  res.weight_factor=0;
  res.x_line=ipi[0];
 }
 return res;
}

