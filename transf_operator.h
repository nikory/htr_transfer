#ifndef TRANSF_OPERATOR_H
#define TRANSF_OPERATOR_H

/*!
� ����� ������� ������, ����������� ��� �������� ������ ������ �������� ���������
*/

#include <map>
#include <iostream>
#include <iomanip>
#include <matrix/matr_com/matr_com.h>
#include "zone_index.h"
#include "TV3.h"
#include "Treg_zone.h"
#include "rubik.h"

#include "serialize_inc.h"

template<class Tzone>
class transf_operator: public matr_com<real>
{
 friend class boost::serialization::access;
/* template<class Arch>
 void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR(Dim);
  a & ARC_ADAPTOR(Dimo);
  for(int i=0;i<Dim*Dimo;i++)
  {
  #ifdef XML_IO
   a & boost::serialization::make_nvp("arr",arr[i]);
  #else
   a & arr[i];
  #endif
  }
  a & ARC_ADAPTOR(index);
 } */
 template<class Arch>
 void save(Arch &a,int)const
 {
  a & ARC_ADAPTOR(Dim);
  a & ARC_ADAPTOR(Dimo);
  for(int i=0;i<Dim*Dimo;i++)
  {
  #ifdef XML_IO
   a & boost::serialization::make_nvp("arr",arr[i]);
  #else
   a & arr[i];
  #endif
  }
  a & ARC_ADAPTOR(index);
 }
 template<class Arch>
 void load(Arch &a,int)
 {
  a & ARC_ADAPTOR(Dim);
  a & ARC_ADAPTOR(Dimo);
  matr_com<real>::reset_to_size(Dimo);
  for(int i=0;i<Dim*Dimo;i++)
  {
  #ifdef XML_IO
   a & boost::serialization::make_nvp("arr",arr[i]);
  #else
   a & arr[i];
  #endif
  }
  a & ARC_ADAPTOR(index);
 }
 BOOST_SERIALIZATION_SPLIT_MEMBER()
public:
 typedef typename zone_index<Tzone> zone_index_t;
 typedef typename matr_com<real> derived_t;
protected:
 zone_index_t* index;
 inline void resize();
public:
 transf_operator();
 transf_operator(rubik& s);
 real& operator()(Tzone* i, Tzone* j);
 real operator()(Tzone* i, Tzone* j)const;
 void set_index(zone_index_t& r);
 Tzone* get_zone(int i);
 inline transf_operator<Tzone>& operator=(const transf_operator<Tzone>& op)
 {
  index=op.index;
  resize();
  (derived_t&)(*this)=(const derived_t&)op;
  return *this;
 }
 inline void fill(real x)
 {
  int i,n;
  n=dim()*dim();
  for(i=0;i<n;i++)  {   (*this)[0][i]=x;  }
 }
};


template <class Tzone>
inline Tzone* transf_operator<Tzone>::get_zone(int i)
{
 return (*index)[i];
}

template<class Tzone> 
inline void transf_operator<Tzone>::resize()
{
 reset_to_size(index->size());
}
template<class Tzone> 
inline void transf_operator<Tzone>::set_index(typename transf_operator<Tzone>::zone_index_t& r)
{
 index=&r;
 resize();
 (*(matr_com<real>*)this)=0.;
}

template<class Tzone>
inline transf_operator<Tzone>::transf_operator():matr_com(1)
{
}

template<class Tzone>
inline transf_operator<Tzone>::transf_operator(rubik &s):matr_com(1)
{
 set_rubik(s);
}

template<class Tzone> 
inline real& transf_operator<Tzone>::operator ()(Tzone *i, Tzone *j)
{
 int ii,ij;
 ii=(*index)[i];
 ij=(*index)[j];
 return (*this)[ii][ij];
}

template<>
inline real& transf_operator<Treg_zone>::operator ()(Treg_zone *i, Treg_zone *j)
{
 int ii,jj;
 ii=i->zone_index;
 jj=j->zone_index;
 return (*this)[ii][jj];
}
template<>
inline real& transf_operator<Treg_zone_vol>::operator ()(Treg_zone_vol *i, Treg_zone_vol *j)
{
 int ii,jj;
 ii=i->vol_index;
 jj=j->vol_index;
 return (*this)[ii][jj];
}
template<>
inline real& transf_operator<Treg_zone_surf>::operator ()(Treg_zone_surf *i, Treg_zone_surf *j)
{
 int ii,jj;
 ii=i->surf_index;
 jj=j->surf_index;
 return (*this)[ii][jj];
}

template<class Tzone> 
inline real transf_operator<Tzone>::operator ()(Tzone *i, Tzone *j)const
{
 int ii,ij;
 ii=(*index)[i];
 ij=(*index)[j];
 return (*this)[ii][ij];
}

template<>
inline real transf_operator<Treg_zone>::operator ()(Treg_zone *i, Treg_zone *j)const
{
 int ii,jj;
 ii=i->zone_index;
 jj=j->zone_index;
 return (*this)[ii][jj];
}
template<>
inline real transf_operator<Treg_zone_vol>::operator ()(Treg_zone_vol *i, Treg_zone_vol *j)const
{
 int ii,jj;
 ii=i->vol_index;
 jj=j->vol_index;
 return (*this)[ii][jj];
}
template<>
inline real transf_operator<Treg_zone_surf>::operator ()(Treg_zone_surf *i, Treg_zone_surf *j)const
{
 int ii,jj;
 ii=i->surf_index;
 jj=j->surf_index;
 return (*this)[ii][jj];
}


template<class T>
inline std::ostream& operator<<(std::ostream& s, const transf_operator<T>& m)
{
 return s<<(matr_com<real>&)m;
}

#endif
