#ifndef RAY_TRACKER_H
#define RAY_TRACKER_H

#include "rubik.h"
#include "matr_t.h"
#include "adsorb.h"

#include "serialize_inc.h"
/*!
����� Tray_tracker ������������ ��� ������� ��������� ������� �������������.
*/

class Tray_tracker
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &ar, int)
 {
  try
  {
   ar & ARC_ADAPTOR(my_rubik);
   ar & ARC_ADAPTOR(matr_G);
   ar & ARC_ADAPTOR(matr_X0);
   ar & ARC_ADAPTOR(ray_count);
   ar & ARC_ADAPTOR(ray_length);
   ar & ARC_ADAPTOR(ads_array);
   ar & ARC_ADAPTOR(nexp);
   ar & ARC_ADAPTOR(fullWray);
   ar & ARC_ADAPTOR(fullLray);
  }
  catch(exception& e)
  {
   cout<<e.what()<<endl;
  }
  catch(...)
  {
   cout<<"some";
  }
}
protected:
 typedef Tzone_tracker::info_con_t::iterator trz_iter_t;
 typedef Tzone_tracker::info_con_t::const_iterator trz_iter_const_t;
 rubik* my_rubik;
 transf_operator<Treg_zone> matr_G;
 transf_operator<Treg_zone> matr_X0;
 zzReal_map_t ray_count;
 vvReal_map_t ray_length; 
 Tray_adsorbtion_array ads_array;
 int nexp;
 void adsorb(Tzone_tracker&);
 real fullWray; //��������� ��� �����
 real fullLray; //���������� ���������� ����� �����
 real fullLray_u;//���������� ������������ ����� �����
public:
 void set_rubik(rubik& rub);
 void update_vols();
 void update_ads();
 void prepare_ads();
 rubik* get_rubik()const;
 void generate_link_mtr(int n, int);
 void fix_ray(const Tline& ray);
// real adsl(Treg_zone* i, Treg_zone* j);
// Tray_adsorbtion* get_ray_adsorbtion(Treg_zone* i, Treg_zone* j);
 Tadsorbtion_graph* get_adsorbtion_graph(Treg_zone* i, Treg_zone* j);
 real operator()(Treg_zone* i, Treg_zone* j);
 real operator()(Treg_zone* i);
 void ptstat(std::ostream& s);
 void correct_vols();
 void X0();
 real X0(Treg_zone* i, Treg_zone* j);
};

#endif