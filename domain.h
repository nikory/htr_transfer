#ifndef DOMAIN_H
#define DOMAIN_H

#include "serialize_inc.h"
#include <map>
#include "surface.h"
#include "condition.h"


/*! ����� ��������� ������� ������������, ������������ �������������. 
    ������� �������� ������: ���� ��������. � ����� �������� ��������� �� �����������,
    � ������ ������ ������� �����������. �������������� ����� ������� ������������
    ���������������� c������ ������� ����� �� ��������� � ������ ����������� c 
    ������������� ���������� �������. ����� ����� �������� () ������� ���������� 1 ���� 
    ��� ���������� �������� ����� ���������. */
class Tdomain_surf
{
 friend class boost::serialization::access;
 template<class Arch>
 inline void serialize(Arch &a,int)
 {
  a.register_type<Tsurf_ge>();
  a.register_type<Tnegation>();
  a.register_type<Tand>();
  a.register_type<Tor>();
  a & ARC_ADAPTOR(formula);
 }
protected:
 typedef Tsurf::iptcon_t iptcon_t;
 iptcon_t vpt;
 mutable Tline_stp vpt_ref;;
 Tcondition *formula;
public:
   mutable int last_line_id;
   Tdomain_surf():formula(0){vpt.reserve(5);vpt_ref.v.reserve(5);}
   Tdomain_surf(Tcondition & dsc):formula(&dsc){vpt.reserve(5);vpt_ref.v.reserve(5);}
///�������� ���������� �������������� ����� �������
    inline bool operator()(const TV3 &p) const
    {
     return (*formula)(p);
    }
///���������� �� ������ �������?
    virtual bool lints(const Tline &l)const
    {
//     if(last_line!=&l)
//     {
//      last_line=&l;
      vpt_ref.clear();
      formula->extract_inters(vpt_ref,l);
//     }
     return vpt_ref.v.size()>0;
    }
    virtual Tline_stp& ipoint(const Tline& l)const
    {
     if(last_line_id!=l.id)
     {
      last_line_id=l.id;
      vpt_ref.v.clear();
      formula->extract_inters(vpt_ref,l);
     }
     return vpt_ref;
    }
    virtual Tline_stp& ipoint()const{return vpt_ref;}
    inline void clear()
    {
     formula=0;
    }
    inline bool containes(const Tsurf *s) const
    {
     return formula->contains(s);
    }
    inline virtual Tdomain_surf& operator=(Tcondition & dsc)
    {
     formula=&dsc; 
     return *this;
    }
    inline Tcondition* get_formula(){return formula;}
};




class Touter_cube: public Tdomain_surf 
{
 friend class boost::serialization::access;
 template<class Arch>
 inline void serialize(Arch &a,int)
 {
  a.register_type<Tplanesurf>();
  a & ARC_ADAPTOR(inp);
  a & ARC_ADAPTOR(corners);
  a & ARC_ADAPTOR(xy1);
  a & ARC_ADAPTOR(xy2);
  a & ARC_ADAPTOR(xz1);
  a & ARC_ADAPTOR(xz2);
  a & ARC_ADAPTOR(yz1);
  a & ARC_ADAPTOR(yz2);
  a & ARC_ADAPTOR(plane_gab);
  a & ARC_ADAPTOR(areas);
  a & ARC_ADAPTOR(plane_finder);
  a & ARC_ADAPTOR_STR("Tdomain_surf",boost::serialization::base_object<Tdomain_surf>(*this));
//  init();
 }
protected:
 std::map<Tplanesurf*, pair<TV3,TV3> > plane_gab;
 std::map<Tplanesurf*, real> areas;
 get_rnd_distr_hist<Tplanesurf*, real> plane_finder;
 std::pair<TV3,TV3> inp;
 std::pair<TV3,TV3> corners;
 Tplanesurf xy1,xy2,xz1,xz2,yz1,yz2;
 std::map<const Tplanesurf*,std::pair<int,real> > sfpt_templ;
 pair<TV3,TV3> get_pt_plane(Tplanesurf* const & rp)const
 {
  std::map<Tplanesurf*, pair<TV3,TV3> >::const_iterator Where;
  Where=plane_gab.lower_bound(rp);
  if(Where==plane_gab.end())throw "Touter_cube::get_pt_plane(): ��� ������ ��������!!!";
  return Where->second;
 }
 pair<TV3,TV3> plg(int i, const Tplanesurf& s1)
 {
  pair<TV3,TV3> vp;
  vp.first=proection(corners.first,i,yz1.local_crd.inip()(i));
  vp.second=proection(corners.second,i,yz1.local_crd.inip()(i));
  return vp;
 }
public:
 inline Touter_cube()//:os("points.dat")
 {
  TV3 dr,r0;
  dr=0.;
  r0=0.;
  init(dr,r0);
 }
 inline Touter_cube(const TV3& dr, const TV3& r0)
 {
  init(dr,r0);
 }
 inline std::pair<Tplanesurf*,TV3> radom_pt_on_bound()const
 {
  static Tplanesurf *rp;
  TV3 p0,res;
  std::pair<Tplanesurf*,TV3> r;
  int i;
  std::pair<TV3,TV3> pp;
  rp=plane_finder();
  std::pair<int,real> pid=sfpt_templ.find(rp)->second;
  p0=rp->local_crd.inip();
  pp=get_pt_plane(rp);
  for(i=0;i<SD;i++)
  {
   if(i==pid.first)
   {
    res(i)=pid.second;
   }
   else
   {
    res(i)=random(pp.first(i),pp.second(i));
   }
  }
  r.first=rp;
  r.second=res;
//  os<<res<<endl;
  return std::pair<Tplanesurf*,TV3>(rp,res);
 }
 inline std::pair<const Tplanesurf*,Tline> random_ray_from_bound()const
 {
  Tline res,l;
  std::pair<const Tplanesurf*,TV3>  pp;
  pp=radom_pt_on_bound();
  l.p0=pp.second;
  l.direct=pp.first->normv(pp.second);
  l.update_ang();
  res=rnd_line(l);
  return std::pair<const Tplanesurf*, Tline>(pp.first,res);
 }
 inline TV3 rand_point()const
 {
  TV3 res;
  int i;
  for(i=0;i<SD;i++)
  {
   res(i)=random(corners.first(i),corners.second(i));
  }
  return res;
 }
 inline void init(const TV3& dr, const TV3& r0)
 {
  inp.first=dr;
  inp.second=r0;
  flush();
  std::pair<TV3,TV3> vp;
  corners.first=r0-0.5*dr;
  corners.second=r0+0.5*dr;
  yz1=plane(ORTS[0],proection(r0,0,corners.first(0)));
  yz2=plane(ORTS[0],proection(r0,0,corners.second(0)));
  xz1=plane(ORTS[1],proection(r0,1,corners.first(1)));
  xz2=plane(ORTS[1],proection(r0,1,corners.second(1)));
  xy1=plane(ORTS[2],proection(r0,2,corners.first(2)));
  xy2=plane(ORTS[2],proection(r0,2,corners.second(2)));
  plane_gab[&yz1]=plg(0,yz1);
  plane_gab[&yz2]=plg(0,yz2);
  plane_gab[&xz1]=plg(1,xz1);
  plane_gab[&xz2]=plg(1,xz2);
  plane_gab[&xy1]=plg(2,xy1);
  plane_gab[&xy2]=plg(2,xy2);
  formula=&((xy1-xy2)*(xz1-xz2)*(yz1-yz2));
  areas[&xy1]=areas[&xy2]=dr(0)*dr(1);
  areas[&xz1]=areas[&xz2]=dr(0)*dr(2);
  areas[&yz1]=areas[&yz2]=dr(1)*dr(2);
  plane_finder(areas);
  sfpt_templ[&xy1].first=2;sfpt_templ[&xy1].second=corners.first(2);
  sfpt_templ[&xy2].first=2;sfpt_templ[&xy2].second=corners.second(2);
  sfpt_templ[&xz1].first=1;sfpt_templ[&xz1].second=corners.first(1);
  sfpt_templ[&xz2].first=1;sfpt_templ[&xz2].second=corners.second(1);
  sfpt_templ[&yz1].first=0;sfpt_templ[&yz1].second=corners.first(0);
  sfpt_templ[&yz2].first=0;sfpt_templ[&yz2].second=corners.second(0);
 }
 inline void init(){init(inp.first,inp.second);}
 inline void flush()
 {
  plane_gab.clear();
  areas.clear();
 }
 inline real volume()const
 {
  real v;
  int i;
  v=1.;
  for(i=0;i<SD;i++)
  {
   v*=corners.second(i)-corners.first(i);
  }
  return v;
 }
 inline real area()const
 {
  real res,a,b,c;
  a=corners.second(0)-corners.first(0);
  b=corners.second(1)-corners.first(1);
  c=corners.second(2)-corners.first(2);
  res=2*(a*b + b*c + a*c);
  return res;
 }
};

//! ����� ��������� ������������ ������� ������������
class Tlim_domain: public Tdomain_surf
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("Tdomain_surf",boost::serialization::base_object<Tdomain_surf>(*this));
  a & ARC_ADAPTOR(gplane);
  a & ARC_ADAPTOR(outer_cube);
 }
 vector<Tplanesurf> gplane;
 Touter_cube* outer_cube;
public:
 Tlim_domain(){}
 Tlim_domain(Touter_cube & cube):outer_cube(&cube){}
 Tlim_domain(Touter_cube& cube, Tcondition &dsc):Tdomain_surf(dsc),outer_cube(&cube){}
 TV3 get_rnd_pt()const
    {
        static TV3 rp;
        rp=0.;
        for(;;)
        {
            rp=outer_cube->rand_point();
            if((*this)(rp))break;
        }
        return rp;
 }
 inline real volume(int n=1000)const
 {
  int in(0),i;
  static TV3 p;
  for(i=0;i<n;i++)
  {
   p=outer_cube->rand_point();
   if((*this)(p))in++;
  }
  return real(in)*outer_cube->volume()/n;
 }
 const Touter_cube& gabar()const{return *outer_cube;}
 void setg(Touter_cube& c){outer_cube=&c;}
 inline Tdomain_surf& operator=(Tcondition & dsc){return Tdomain_surf::operator=(dsc);}
};

/*! ����� ��������� �������� ������� (�.�.����� �������, ��� ������������ ������������ 
��� ������ ����� �� ����� 2 ����� ����������� � �������� �������. ���������� ���������� 
������, ������������� �������� ������� ������� ����, �� ����������� ������ ���������� ��� 
����� ������� ������������� ����� ����� ����.)*/
/*class Tconvex_domain: public Tlim_domain
{
public:
 std::pair<real,real> ipoint_pair(const Tline& l)const
 {
  std::pair<real,real> res;
  Tdomain_surf::iptcon_t vr;
  real a;
  vr=ipoint(l);
  if(vr.size()>0)
  {
   res.first=vr[0];
   if(vr.size()>1)
   {
    res.second=vr[1];
   }
  }
  if(res.first>res.second) 
  {
   a=res.first;
   res.first=res.second;
   res.second=a;
  }
  return res;
 }
};*/

#endif           