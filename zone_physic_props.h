#ifndef ZONE_PHYSIC_PROPS
#define ZONE_PHYSIC_PROPS

#define _USE_MATH_DEFINES

#include <cmath>
#include "phys_const.h"
#include "serialize_inc.h"
#include "tv3.h"

/*!� ��������� ������� �������� ���������� �������� ��� (������� �����)*/
class Tzone_props
{
 friend class boost::serialization::access;
 template <class arch>
 void serialize(arch& a, int)
 {
  a & ARC_ADAPTOR(TMPR);
 }
public:
 real TMPR; //!< �����������, �
 Tzone_props(){TMPR=300.;}
 virtual void find_q(){}
};

//! �������� �����������
class Tzone_props_surf: public Tzone_props
{
 friend class boost::serialization::access;
 template <class arch>
 void serialize(arch& a, int)
 {
  a & ARC_ADAPTOR_STR("Tzone_props",boost::serialization::base_object<Tzone_props>(*this));
  a & ARC_ADAPTOR(epsilon) & ARC_ADAPTOR(alpha) & ARC_ADAPTOR(Qs);
 }
public:
 real epsilon; //!< ������� ������� �����������
 real alpha;   //!< ������������� �����������
 real Qsb;     //!< ��������� ��������� ������� ����
 real Qs;      //!< ��������� ������������ ��������� �������, ��/�^2
 real Qeff;    //!< �������������� ����� ����� �� �����������, ��/�^2
 Tzone_props_surf():Tzone_props()
 {
  epsilon=0.5e0;
  alpha=1.-epsilon;
  Qs=0.;
  Qeff=0.;
 }
 void find_q()
 {
  Qsb=StephanBoltsmanSigma*sq4(TMPR);
  Qs=epsilon*Qsb;
 }
};

//! �������� �������� ������
class Tzone_props_vol: public Tzone_props
{
 friend class boost::serialization::access;
 template <class arch>
 void serialize(arch& a, int)
 {
  a & ARC_ADAPTOR_STR("Tzone_props",boost::serialization::base_object<Tzone_props>(*this));
  a & ARC_ADAPTOR(ro) & ARC_ADAPTOR(sigma_a) & ARC_ADAPTOR(Qv);
 }
public:
 real ro;      //!< ���������, ��/�^3
 real sigma_a; //!< ������� ����������, 1/�
 real sigma_a_eff; //!< ����������� ������� ����������, ����������� ��� ���������� ��������� ��������, 1/�
 real Qv;      //!< ��������� ������������ ��������� �������, ��/�^3
 real Qadv;    //!< ��������� ������������ ��������� �������, ��/�^3
 real volradcor;
 Tzone_props_vol():Tzone_props()
 {
  ro=1.;
  sigma_a=1.0;
  sigma_a_eff=1.0;
  Qv=1.;
  Qadv=1.;
 }
 void find_q()
 {
//  Qv=4.*M_PI*sigma_a*StephanBoltsmanSigma*sqr(sqr(TMPR));
//  Qv=sigma_a_eff*StephanBoltsmanSigma*sq4(TMPR);
//  Qv=0;
 }
};


#endif