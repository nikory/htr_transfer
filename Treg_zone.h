#ifndef Treg_zone_h
#define Treg_zone_h

#include <string>
#include "surface.h"
#include "domain.h"
#include "line.h"
#include "TV3.h"
#include "zone_physic_props.h"
#include "serialize_inc.h"
#define _USE_MATH_DEFINES
#include <cmath>

enum {RZ_VOL,RZ_SURF};

class Treg_zone;
class Treg_zone_surf;
class Treg_zone_vol;

/*! 
����� ������������ �������� ���������� � ����������� ������ � ����.
*/
class Treg_info
{
public:
 Treg_zone* rz; //!< ��������� �� ��������������� ����
 real x_line; //!< ���������� ����� ����������� �� ������ pt=line.p0+line.dir*x_line
 real len; //!����� �����������
 TV3 pt; //!< ���������� ����� �����������. ��� �������� ��� - ������� ����� ����������� � ���������
 TV3 norm; //!< ������� � ����������� � ����� �����������. (� ������ ���� ���� �������� ��� ����� ������������ ������)
 real cosfi; //!< ��������� ������������ ����������� ������ � �������
 real weight_factor; //!< ������� ���������
 real sink_factor; //!< ���������, ����������� ���������� ����
 bool reg; //!< ������� ����������� ���� ������
 Treg_info():reg(false){}
 bool operator<(const Treg_info &ri)const{return x_line<ri.x_line;}
};


/*!
�����-���������� ��������������� ����.
*/
class Treg_zone
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a, int)
 {
  a & ARC_ADAPTOR(my_vol);
  a & ARC_ADAPTOR(name);
 }
protected:
 real my_vol;
 real my_vol_corr;
public:
    Treg_zone(void);
public:
    ~Treg_zone(void);
public:
    int zone_index;
    std::string name;
    virtual bool operator()(const TV3&)=0;//!< ����������� �� �����  ����
    virtual bool operator()(const Tline&)=0;//!< ���������� �� ������ ����
    virtual Treg_info reg_info(const Tline&)=0;//!< �������� ���������� � ����������� ������ � ����
    virtual int my_type()const=0;//!< ���������� ������������� ���� ���� (�������� ��� �������������)
    virtual void set_vol(real v){my_vol=v;}//!< ������ ������ ���� (������� ��� �����)
    virtual void set_vol_corr(real v){my_vol_corr=v;}//!< ������ ������ ���� (������� ��� �����)
    virtual real get_vol()const{return my_vol;}//!< ������ ������ ���� (������� ��� �����)
    virtual real get_vol_corr()const{return my_vol_corr;}//!< ������ ������ ���� (������� ��� �����)
};
BOOST_SERIALIZATION_ASSUME_ABSTRACT(Treg_zone)

/*!
����� ��� �������� �������� ����. �������� ���� 
��� ������� ������������, ��� ������� ���������� �������
�������������� ����� ���� ����, �������������� �� �������� ������ �
������� ������������ ����� � ������� ������ ����� �������� ������.
��� ������������ �������� ���� ���������� ����� ������������ �����, 
���� �� �� ��������� ��������� �������� �������������.
*/
extern std::vector<Tlim_domain*> ZONE_DOMAIN;
extern Touter_cube SGAB;
class Treg_zone_vol:public Treg_zone
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("Treg_zone",boost::serialization::base_object<Treg_zone>(*this));
  a & ARC_ADAPTOR(props_v);
  a & ARC_ADAPTOR(vol);
 }
    Tzone_props_vol props_v;
public:
    int vol_index;
    Tlim_domain* vol;
    Treg_zone_vol(){}
    Treg_zone_vol(Tlim_domain& d,real v=0):vol(&d){set_vol(v);}
    Treg_zone_vol(Tcondition& dsc, real v)
    {
     vol=new Tlim_domain(SGAB,dsc);
     set_vol(v);
     ZONE_DOMAIN.push_back(vol);
    }
    bool operator()(const Tline& l)
    {
     return vol->lints(l);
    }
    bool operator()(const TV3& p)
    {
     return (*vol)(p);
    }
    Treg_info reg_info(const Tline& l)
    {
     Treg_info res;
     Tline_stp i;
     res.reg=(*this)(l);
     if(res.reg)
     {
      i=vol->ipoint();
      res.norm=l.direct;
      res.x_line=(real(i.v[0].x)+real(i.v[1].x))*0.5;
      res.len=i.length()*l.w;
      res.pt=l.get_pt(res.x_line);
      res.rz=this;
      res.sink_factor=1.;//my_vol;
      res.weight_factor=1.;
     }
     return res;
    }
    int my_type()const{return RZ_VOL;}
    Tzone_props_vol& props(){return props_v;}
    const Tzone_props_vol& props()const{return props_v;}
};

/*!
����� ��� �������� ������������� ����. ������������� ���� 
��� ������� �����������, ��� ������� ���������� �������
�������������� �� �������� ������ �
������� ������������ ����� � ������� ������ ����� �������� ������.
��� ������������ ������������� ���� ���������� ����� ������������ �������, 
���� �� ������� ��������� ��������� �������� �������������.
*/
class Treg_zone_surf: public Treg_zone_vol
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("Treg_zone_vol",
      boost::serialization::base_object<Treg_zone_vol>(*this));
  a & ARC_ADAPTOR(props_s);
  a & ARC_ADAPTOR(sv);
 }
    Tzone_props_surf props_s;
public:
    int surf_index;
    std::vector<Tsurf*> sv;
    Treg_zone_surf():Treg_zone_vol(){}
    Treg_zone_surf(Tlim_domain& d, Tsurf& s, real su=0):Treg_zone_vol(d)
    {
     sv.push_back(&s);
     set_vol(su);
    }
    Treg_zone_surf(Treg_zone_vol& v,Tsurf& s, real su=0):Treg_zone_vol(*v.vol)
    {
     sv.push_back(&s);
     set_vol(su);
    }
    bool operator()(const TV3&){return false;}
    bool operator()(const Tline &l);
    Treg_info reg_info(const Tline& l);
    inline int my_type()const{return RZ_SURF;}
    Tzone_props_surf& props(){return props_s;}
};

#endif