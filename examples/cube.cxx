#include "user_topka.dll"

real x0=0.5,y0=0.5,z0=0.5;
real dx=1.,dy=1.,dz=1.;
real gx=1.2,gy=1.2,gz=1.2;

SetGabarites(x0,y0,z0,gx,gy,gz);

Direction ex(-1,0,0);
Direction ey(0,1,0);
Direction ez(0,0,1);

real poi[][3]={{0,0,0},{1,1,1}};

Plane le(ex,poi[0]);
Plane ri(ex,poi[1]);
Plane ft(ey,poi[0]);
Plane bk(ey,poi[1]);
Plane dn(ez,poi[0]);
Plane up(ez,poi[1]);

VolumeZone cube((-le)*(ri)*(ft-bk)*(dn-up),1.);

SurfaceZone zle(cube,le,1.),
            zri(cube,ri,1.),
            zft(cube,ft,1.),
            zbk(cube,bk,1.),
            zdn(cube,dn,1.),
            zup(cube,up,1.);
            
StartMC(1e5,1e4);

CheckX0("output/cube_chk");

