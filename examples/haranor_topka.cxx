//�������� ���������� ������ ����� ����������� ���, ���������� ���������
//#include <iostream.h>
#include "user_topka.dll"

real GX=12.48,GY=13.52,GZ=52.3; //�������� �����
real DX=20.,DY=25.,DZ=70.; //������� ���������������
real X0=0.5*GX,Y0=0.5*GY,Z0=0.5*GZ; //��������� ������ ���������������
SetGabarites(X0,Y0,Z0,DX,DY,DZ); //������� ������������� ���������������
//������� �����������
Direction Ex(1.,0.,0.);
Direction Ey(0.,1.,0.);
Direction Ez(0.,0.,1.);
Direction E1(0.83,0., 0.57); //������� � ����� ����������� �����
Direction E2(0.83,0.,-0.57); //������� � ������ ����������� �����
Direction E3(0.58,0., 0.81); //������� � ��������� ����������� "����"
//������� ������� �����
real OOO[]={0.,0.,0.};    //������ ���������
real BP1[][3]={{0.,0.,52.3},{6.,0.,52.3},{6.,0.,38.3},{8.98,0.,38.3},{12.480,0.,35.8},{0.,0.,7.65},{12.48,0.,7.65},{5.24,0.,0.},{7.24,0.,0.}};
real BP2[][3]={{0.,13.52,52.3},{6.,13.52,52.3},{6.,13.52,38.3},{8.98,13.52,38.3},{12.480,13.52,35.8},{0.,13.52,7.65},
               {12.48,13.52,7.65},{5.24,13.52,0.},{7.24,13.52,0.}};
real BP3[][3]={{0.,0.,0.},{0.,0.,4.3},{0.,0.,8.8},{0.,0.,13.3},{0.,0.,17.8},{0.,0.,22.3},{0.,0.,26.8},{0.,0.,31.3},
               {0.,0.,35.8},{0.,0.,38.3},{0.,0.,45.3},{0.,0.,52.3}};

//�������� ������������
PlaneArray Pz(Ez,BP3,12);
Plane Pfront(Ey,BP1[0]);
Plane Pback(Ey,BP2[0]);
Plane Pleft(Ex,BP1[0]);
Plane PrightUp(Ex,BP1[1]);
Plane PrightDn(Ex,BP1[4]);
Plane PslantUp(E3,BP1[4]);
Plane PslantDnL(E1,BP1[7]);
Plane PslantDnR(E2,BP1[8]);

//�������� �������� ���
VolumeZone Lay1((Pz[0]-Pz[1])*(Pfront-Pback)*(PslantDnL-PslantDnR),287.503),
           Lay2((Pz[1]-Pz[2])*(Pfront-Pback)*(PslantDnL-PslantDnR)*(Pleft-PrightDn),655.354),
           Lay3((Pz[2]-Pz[3])*(Pfront-Pback)*(Pleft-PrightDn),759.283),
           Lay4((Pz[3]-Pz[4])*(Pfront-Pback)*(Pleft-PrightDn),759.283),
           Lay5((Pz[4]-Pz[5])*(Pfront-Pback)*(Pleft-PrightDn),759.283),
           Lay6((Pz[5]-Pz[6])*(Pfront-Pback)*(Pleft-PrightDn),759.283),
           Lay7((Pz[6]-Pz[7])*(Pfront-Pback)*(Pleft-PrightDn),759.283),
           Lay8((Pz[7]-Pz[8])*(Pfront-Pback)*(Pleft-PrightDn),759.283),
          Lay9L((Pz[8]-Pz[9])*(Pfront-Pback)*(Pleft-PrightUp),202.8),
          Lay9R((Pz[8]-Pz[9])*(Pfront-Pback)*(PrightUp-PslantUp),159.874),
          Lay10((Pz[9]-Pz[10])*(Pfront-Pback)*(Pleft-PrightUp),567.84),
         Lay11((Pz[10]-Pz[11])*(Pfront-Pback)*(Pleft-PrightUp),567.84);
         
//�������� ������������� ���
SurfaceZone Lay1Bt(Lay1,Pz[0],27.04),
            Lay1Ft(Lay1,Pfront,21.26504575),
            Lay1Bk(Lay1,Pback ,21.26504575),
            Lay1Le(Lay1,PslantDnL,70.46649739),
            Lay1Ri(Lay1,PslantDnR,70.46649739);
SurfaceZone Lay2Ft(Lay2,Pfront,48.47295425),
            Lay2Bk(Lay2,Pback ,48.47295425),
            Lay2LD(Lay2,PslantDnL,54.89831773),
            Lay2RD(Lay2,PslantDnR,54.89831773),
            Lay2LU(Lay2,Pleft,15.548),
            Lay2RU(Lay2,PrightDn,15.548);
SurfaceZone Lay3Ft(Lay3,Pfront,56.16),
            Lay3Bk(Lay3,Pback ,56.16),
            Lay3Le(Lay3,Pleft ,60.84),
            Lay3Ri(Lay3,PrightDn,60.84);
SurfaceZone Lay4Ft(Lay4,Pfront,56.16),
            Lay4Bk(Lay4,Pback ,56.16),
            Lay4Le(Lay4,Pleft ,60.84),
            Lay4Ri(Lay4,PrightDn,60.84);
SurfaceZone Lay5Ft(Lay5,Pfront,56.16),
            Lay5Bk(Lay5,Pback ,56.16),
            Lay5Le(Lay5,Pleft ,60.84),
            Lay5Ri(Lay5,PrightDn,60.84);
SurfaceZone Lay6Ft(Lay6,Pfront,56.16),
            Lay6Bk(Lay6,Pback ,56.16),
            Lay6Le(Lay6,Pleft ,60.84),
            Lay6Ri(Lay6,PrightDn,60.84);
SurfaceZone Lay7Ft(Lay7,Pfront,56.16),
            Lay7Bk(Lay7,Pback ,56.16),
            Lay7Le(Lay7,Pleft ,60.84),
            Lay7Ri(Lay7,PrightDn,60.84);
SurfaceZone Lay8Ft(Lay8,Pfront,56.16),
            Lay8Bk(Lay8,Pback ,56.16),
            Lay8Le(Lay8,Pleft ,60.84),
            Lay8Ri(Lay8,PrightDn,60.84);
SurfaceZone Lay9LFt(Lay9L,Pfront,15.),
            Lay9LBk(Lay9L,Pback ,15.),
            Lay9LLe(Lay9L,Pleft ,33.8)
            Lay9RFt(Lay9R,Pfront,11.825),
            Lay9RBk(Lay9R,Pback ,11.825),
            Lay9RRi(Lay9R,PslantUp,58.15171881),
            Lay9RUp(Lay9R,Pz[9],40.2896);
SurfaceZone Lay10Ft(Lay10,Pfront,42.),
            Lay10Bk(Lay10,Pback ,42.),
            Lay10Le(Lay10,Pleft ,94.64),
            Lay10Ri(Lay10,PrightUp,94.64);
SurfaceZone Lay11Ft(Lay11,Pfront,42.),
            Lay11Bk(Lay11,Pback ,42.),
            Lay11Le(Lay11,Pleft ,94.64),
            Lay11Ri(Lay11,PrightUp,94.64),
            Lay11Up(Lay11,Pz[11],81.12);

StartMC(8e6,1e5);
SaveModel("output/hartopka8e6.model.binary");
PrintStat("output/hartopka.mcout");
PrintCoef("output/hartopka.coefs");
PrintCoef("output/matr_x.dat");
CheckX0("output/topka_chk");


#include "user_topka.dll"
LoadModel("output/hartopka8e5.model.binary");
CheckX0("output/topka_chk");
