#ifndef ZONE_INDEX_H
#define ZONE_INDEX_H

#include "serialize_inc.h"
#include <map>
#include <vector>

template <class Tzone>
class zone_index
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch & a, int)
 {
  a.register_type<Treg_zone_vol>();
  a.register_type<Treg_zone_surf>();
  a & ARC_ADAPTOR(m);
  a & ARC_ADAPTOR(invmap);
 }
 typedef Tzone* pzone_t;
 typedef const Tzone* pzone_const_t;
 typedef int index_t;
 typedef typename std::map<pzone_t, index_t> map_t;
 typedef typename std::vector<pzone_t> invmap_t;
 map_t m;
 invmap_t invmap;
public:
 zone_index(){invmap.reserve(30);}
 inline index_t& operator[](pzone_t z)
 {
  return m[z];
 }
 inline index_t  operator[](pzone_t z)const
 {
  map_t::const_iterator m_end,find;
  m_end=m.end();
  find=m.lower_bound(z);
  if(find!=m_end)
  {
   return find->second;
  }
  else
  {
//   throw "zone_index: �� �������";
   return 0xffffff;
  }
 }
 inline pzone_t  operator[](index_t i)const
 {
  return invmap[i];
 }
/* inline pzone_t  operator[](index_t i)const
 {
  return invmap[i];
 }*/
 inline void clr(){m.clear();invmap.clear();}
 inline zone_index<Tzone>& operator=(map_t& mc)
 {
  m=mc;
  map_t::iterator i,b,e;
  invmap.resize(mc.size());
  b=mc.begin();
  e=mc.end();
  for(i=b;i!=e;i++)
  {
   invmap[i->second]=i->first;
  }
  return *this;
 }
 inline int size()const{return m.size();}
 inline int push(pzone_t z)
 {
  int n;
  auto f=m.find(z);
  if(f == m.end())
  {
   n=m.size();
   m[z]=n;
   invmap.push_back(z);
   return n;
  }
  else
  {
   return f->second;
  }
 }
 inline index_t sfind_index(pzone_t z)const
 {
  int i,n;
  index_t res;
  const pzone_t* zb;
  zb=&invmap[0];
  n=invmap.size();
  res=0xffffff;
  for(i=0;i<n;zb++)
  {
   if(*zb==z)break;
   i++;
  }
  if(i<n)res=i;
  return res;
 }
};


#endif