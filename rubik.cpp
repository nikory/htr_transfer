#include "rubik.h"

extern Touter_cube SGAB;

rubik::rubik():outer_cube(SGAB),outer_cube_zone(*(SGAB.get_formula()),SGAB.volume())
{
/* create_surfaces();
 create_region();
 create_volz();
 create_regzones();
 set_my_volume();   */
}

rubik::~rubik()
{
/* std::set<Treg_zone*>::iterator zb,ze,zi;
 zb=rz_set.begin();
 ze=rz_set.end();
 for(zi=zb;zi!=ze;zi++)
 {
  delete *zi;
 }  */
}

void rubik::create_surfaces()
{
/* typedef MatNM<int,3,1> TV3_int;
 TV3_int n={3,3,3};
 TV3 gb={2,2,2};
 TV3 dx=gb/(n-1);
 TV3 p,p0={-1,-1,-1};
 int i,j;
 for(i=0;i<3;i++)
 {
  p=0.;
  p(i)=p0(i);
  for(j=0;j<n(i);j++,p(i)+=dx(i))
  {
   planes[i].push_back(plane(ORTS[i],p));
  }
 } */
}  

/*void rubik::create_surfaces()
{
 Tline norm;
 real sides[]={2,2,2};
 int i,j;
 int N=1;
 for(i=0;i<3;i++)
 {
  norm.p0=0.;
  norm.p0(i)=-sides[i]*0.5;
  norm.direct=0.;
  norm.direct(i)=1;
  norm.update_ang();
  for(j=0;j<=N;j++,norm.p0(i)+=sides[i]/N)
  {
   planes[i].push_back(Tplanesurf(norm));
  }
 }
}*/

void rubik::create_region()
{ 
/* TV3 gb={3,3,3};
 outer_cube.init(gb,OOO);
 topka.setg(outer_cube);
 topka=(planes[0].front()-planes[0].back())*
       (planes[1].front()-planes[1].back())*
       (planes[2].front()-planes[2].back());*/
}      

/*void rubik::create_region()
{
 int i;
 TV3 gab={3,3,3};
 TV3 centr={0,0,0};
 outer_cube.flush();
 outer_cube.init(gab,centr);
 for(i=0;i<3;i++)
 {
  topka.add_space(planes[i].front(),1);
  topka.add_space(planes[i].back(),-1);
 }
 topka.setg(outer_cube);
} */     

void rubik::create_volz()
{
/* int n=3;
 int i,j,k;
 Tlim_domain v;
 for(i=0;i<planes[0].size()-1;i++)
 {
  for(j=0;j<planes[1].size()-1;j++)
  {
   for(k=0;k<planes[2].size()-1;k++)
   {
    v=(planes[0][i]-planes[0][i+1])*
      (planes[1][j]-planes[1][j+1])*
      (planes[2][k]-planes[2][k+1]);
    volz.push_back(v);
   }
  }
 } */
} 

void rubik::create_regzones()
{
/* int nv=volz.size();
 int i,j;
 for(i=0;i<nv;i++)
 {
  vzarr.push_back(Treg_zone_vol(volz[i]));
 }
 std::vector<Tsurf*> tmp;
 for(i=0;i<3;i++)
 {
  tmp.push_back(&planes[i].front());
  tmp.push_back(&planes[i].back());
 }
 for(i=0;i<tmp.size();i++)
 {
  for(j=0;j<volz.size();j++)
  {
   if(volz[j].containes(tmp[i]))
   {
    szarr.push_back(Treg_zone_surf(volz[j],*tmp[i]));
   }
  }
 }
 for(i=0;i<vzarr.size();i++)
 {
  rz_set.insert(&vzarr[i]);
  rzv_set.insert(&vzarr[i]);
  zone_index_com.push(&vzarr[i]);
  zone_index_vol.push(&vzarr[i]);
 }
 for(i=0;i<szarr.size();i++)
 {
  rz_set.insert(&szarr[i]);
  rzs_set.insert(&szarr[i]);
  zone_index_com.push(&szarr[i]);
  zone_index_surf.push(&szarr[i]);
 } */
}


/*void rubik::create_regzones()
{
 Tplanesurf *right,*left,*front,*back,*top,*down;
 Treg_zone_surf* sz;
 Treg_zone_vol* vz;
 right=&(planes[1].front());
 left=&(planes[1].back());
 front=&(planes[0].front());
 back=&(planes[0].back());
 top=&(planes[2].front());
 down=&(planes[2].back());
 vz=new Treg_zone_vol(volz.front(),8);
 rz_set.insert(vz);
 rzv_set.insert(vz);
 nray_zone[vz]=0;
 sz=new Treg_zone_surf(volz.front(),*left,4);
 rz_set.insert(sz);
 rzs_set.insert(sz);
 nray_zone[sz]=0;
 sz=new Treg_zone_surf(volz.front(),*right,4);
 rz_set.insert(sz);
 rzs_set.insert(sz);
 nray_zone[sz]=0;
 sz=new Treg_zone_surf(volz.front(),*front,4);
 rz_set.insert(sz);
 rzs_set.insert(sz);
 nray_zone[sz]=0;
 sz=new Treg_zone_surf(volz.front(),*back,4);
 rz_set.insert(sz);
 rzs_set.insert(sz);
 nray_zone[sz]=0;
 sz=new Treg_zone_surf(volz.front(),*top,4);
 rz_set.insert(sz);
 rzs_set.insert(sz);
 nray_zone[sz]=0;
 sz=new Treg_zone_surf(volz.front(),*down,4);
 rz_set.insert(sz);
 rzs_set.insert(sz);
 nray_zone[sz]=0;  
 make_index_map();
} */

void rubik::set_my_volume()
{
/* rz_set_t::iterator i,b,e;
 b=rz_set.begin();
 e=rz_set.end();
 s_surfz=0.;
 v_volz=0.;
 for(i=b;i!=e;i++)
 {
  switch((*i)->my_type())
  {
   case(RZ_SURF):
    s_surfz+=(*i)->get_vol();
    break;
   case(RZ_VOL):
    v_volz+=(*i)->get_vol();
    break;
  }
 } */
}
const Treg_zone* rubik::find(const TV3& pt)const
{
 std::set<Treg_zone*>::const_iterator zi,zb,ze;
 bool in;
 const Treg_zone* res=NULL;
 zb=rz_set.begin();
 ze=rz_set.end();
 for(zi=zb;zi!=ze;zi++)
 {
  in=(**zi)(pt);
  if(in)res=*zi;
 }
 return res;
}

const Touter_cube& rubik::get_outer_cube() const
{
 return outer_cube;
}

const Treg_zone_vol& rubik::get_outer_cube_zone() const
{
 return outer_cube_zone;
}

Treg_zone_vol& rubik::get_outer_cube_zone() 
{
 return outer_cube_zone;
}


real rubik::estimate_full_Sq()
{
 rzs_set_t::iterator si,sb,se;
 sb=rzs_set.begin();
 se=rzs_set.end();
 real res;
 res=0;
 for(si=sb;si!=se;si++)
 {
  res+=(*si)->get_vol();
 }
 return res; 
}

real rubik::estimate_full_Vol()
{
 rzv_set_t::iterator si,sb,se;
 sb=rzv_set.begin();
 se=rzv_set.end();
 real res;
 res=0;
 for(si=sb;si!=se;si++)
 {
  res+=(*si)->get_vol();
 }
 return res; 
}

void rubik::index_zones()
{
 rz_set_t::iterator zi,ze;
 rzv_set_t::iterator vi,ve;
 rzs_set_t::iterator si,se;
 ze=rz_set.end();
 ve=rzv_set.end();
 se=rzs_set.end();
 for(zi=rz_set.begin();zi!=ze;zi++)
 {
  (*zi)->zone_index=zone_index_com[*zi];
 }
 for(vi=rzv_set.begin();vi!=ve;vi++)
 {
  (*vi)->vol_index=zone_index_vol[*vi];
 }
 for(si=rzs_set.begin();si!=se;si++)
 {
  (*si)->surf_index=zone_index_surf[*si];
 }
}