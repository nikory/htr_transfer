#ifndef DECART_H
#define DECART_H

#include "serialize_inc.h"
#include "tv3.h"

/*!��������� ������� ���������*/
class Decart_system
{
 friend class boost::serialization::access;
 template <class Arch>
 inline void serialize(Arch & a, int version)
 {
  using namespace boost::serialization;
  a & ARC_ADAPTOR(p0);
  a & ARC_ADAPTOR(fi);
  a & ARC_ADAPTOR(converce_matrix);
  a & ARC_ADAPTOR(inv_converce_matrix);
 }
 TV3 p0; ///������ ��������� ������� ���������
 TV3 fi; ///���������� ��������� ������� ��������� (���������������� �������� ������ ������������ ���� ������� �������)
 TM3 converce_matrix; ///������� �������������� ��������� �� ���������� ������� � ���������
 TM3 inv_converce_matrix; ///������� ��������� �������������� ���������
public:
 Decart_system()
 {
  p0=0.;
  fi=0.;
  converce_matrix=I_matr<real,SD>();
  inv_converce_matrix=I_matr<real,SD>();
 }
 TV3 convert_to(const TV3& p) const
 {
  return converce_matrix*(p-p0);
 }
 TV3 convert_from(const TV3& p) const
 {
  return inv_converce_matrix*p+p0;
 }
 inline void move_to(const TV3& p) {  p0=p; }///����������� ������ ������� � ����� p
 inline void move_on(const TV3& d) { p0+=d; }///����������� ������ ������� �� ������-������ d
 void rotate_x_to(real al)///�������� �� ������� ������� ������ ��� x ������� �������(���� �������� ������ ����������� ���) �� ���� fi
 {
  static TM3 rot;
  static real a;
  static real b;
  a=cos(al);
  b=sin(al);
  rot=I_matr<real,3>();
  fi(0)=al;
  (rot[1][1])=a;
  (rot[2][2])=a;
  (rot[2][1])=b;
  (rot[1][2])=-b;
  converce_matrix=converce_matrix*rot;
 }
 void rotate_y_to(real al)///�������� �� ������� ������� ������ ��� y ������� �������(���� �������� ������ ����������� ���) �� ���� fi 
 {
  static TM3 rot;
  static real a;
  static real b;
  a=cos(al);
  b=sin(al);                
  rot=I_matr<real,3>();
  fi(1)=al;
  rot[0][0]=a;
  rot[2][2]=a;
  rot[2][0]=b;
  rot[0][2]=-b;
  converce_matrix=converce_matrix*rot;
 }
 void rotate_z_to(real al)///�������� �� ������� ������� ������ ��� z ������� �������(���� �������� ������ ����������� ���) �� ���� fi
 {
  static TM3 rot;
  static real a;
  static real b;
  a=cos(al);
  b=sin(al);     
  rot=I_matr<real,3>();
  fi(2)=al;           
  rot[0][0]=a;
  rot[1][1]=a;
  rot[1][0]=b;
  rot[0][1]=-b;
  converce_matrix=converce_matrix*rot;
 }
 void rotate_x_on(real al)///�������� �� ������� ������� ������ ��� x ������� �������(���� �������� ������ ����������� ���) �� ���� fi 
 {
  rotate_x_to(fi(0)+=al);
 }
 void rotate_y_on(real al)///�������� �� ������� ������� ������ ��� y ������� �������(���� �������� ������ ����������� ���) �� ���� fi  
 {
  rotate_x_to(fi(1)+=al);
 }
 void rotate_z_on(real al)///�������� �� ������� ������� ������ ��� z ������� �������(���� �������� ������ ����������� ���) �� ���� fi 
 {
  rotate_x_to(fi(2)+=al);
 }
 void flush()///
 {
  converce_matrix=I_matr<real,SD>();
  inv_converce_matrix=converce_matrix;
  p0.set_all(0.);
 }
 void update()
 {
  inv_converce_matrix=inv(converce_matrix);
 }
 const TV3& inip()const{return p0;}
};

#endif