#ifndef DOMAIN_H
#define DOMAIN_H

#include <map>
#include "surface.h"

/*! ����� ��������� ������� ������������, ������������ �������������. 
    ������� �������� ������: ���� ��������. � ����� �������� ��������� �� �����������,
    � ������ ������ ������� �����������. �������������� ����� ������� ������������
    ���������������� c������ ������� ����� �� ��������� � ������ ����������� c 
    ������������� ���������� �������. ����� ����� �������� () ������� ���������� 1 ���� 
    ��� ���������� �������� ����� ���������. */
class Tdomain_surf
{
public:
 typedef std::map<const Tsurf*,int> logmap_t;
protected:
 typedef Tsurf::iptcon_t iptcon_t;
/// � ���� �������� ������������ ����������� � ������� ����� �� ��������� � �����������
 logmap_t logmap;
public:
///�������� ���������� �������������� ����� �������
    inline bool operator()(const TV3 &p) const
    {
        bool result = true;
        logmap_t::const_iterator i;
        logmap_t::const_iterator b;
        logmap_t::const_iterator e;
        logmap_t::key_type s;
        int ip;
        int is;
        b = logmap.begin();
        e = logmap.end();
        for (i = b; i != e; i++)
        {
            s = i->first;
            is = i->second;
            ip = s->side(p);
            if (ip != is)
            {
                result = false;
                break;
            }
        }
        return result;
    }
///�������� "�����" ����� ����������� (������=-1)
    inline Tdomain_surf& operator<<(const Tsurf& s)
    {
     logmap[&s]=-1;
     return *this;
    }
///�������� "������" ����� ����������� (������=1)
    inline Tdomain_surf& operator>>(const Tsurf& s)
    {
     logmap[&s]=1;
     return *this;
    }
    inline Tdomain_surf& add_space(const Tsurf& s, int ind)
    {
     logmap[&s]=ind; 
     return *this;
    }
///���������� �� ������ �������?
    virtual bool lints(Tline const&l) const
    {
        bool result = false;
        const Tsurf::iptcon_t *ipts;
        std::vector<real> pts;
        pts.reserve(10);
        int i;
        int n;
        TV3 p;
        logmap_t::const_iterator ptb;
        logmap_t::const_iterator pte;
        logmap_t::const_iterator pt;
        ptb = logmap.begin();
        pte = logmap.end();
        for (pt = ptb; pt != pte && !result; pt++)
            {
                ipts = &(pt->first->ipoint(l));
                n = ipts->size();
                for (i = 0; i < n; i++)
                {
                 pts.push_back((*ipts)[i]);
                }
            }
         std::sort(pts.begin(),pts.end());
         n=pts.size();
         for (i = 0; i < n - 1; i++)
             {
                p = l.get_pt(0.5 * (pts[i] + pts[i + 1]));
                if ((*this)(p))
                {
                    result = true;
                    break;
                }
             }
            return result;
    }
    virtual iptcon_t& ipoint(const Tline& l)const
    {
     logmap_t::const_iterator s,sb,se;
     int n,i;
     static iptcon_t res;
     const iptcon_t* tmp;
     res.resize(0);
     sb=logmap.begin();
     se=logmap.end();
     for(s=sb;s!=se;s++)
     {
       tmp=&(s->first->ipoint(l));
       n=tmp->size();
       for(i=0;i<n;i++)
       {
         if(point_on_border(l.get_pt(tmp[0][i]),s->first))
         {
          res.push_back((*tmp)[i]);
         }
       }
     }
     std::sort(res.begin(),res.end());
     return res;
    }
///��������� �� ������ ����� pt �� ������� ������� sf?
    inline bool point_on_border(const TV3& pt, const Tsurf* sf)const
    {
     logmap_t::const_iterator s,sb,se;
     bool res;
     const Tsurf* cs;
     int i;
     res=true;
     sb=logmap.begin();
     se=logmap.end();
     for(s=sb;s!=se;s++)
     {
      cs=s->first;
      if(cs!=sf)
      {
       i=cs->side(pt);
       if(i!=s->second)
       {
        res=false;
        break;
       }
      }
     }
     return res;
    }
    inline void clear()
    {
     logmap.clear();
    }
    inline void remove_sf(const Tsurf* s)
    {
     logmap.erase(s);
    }
    inline bool containes(const Tsurf *s) const
    {
        bool result = false;
        logmap_t::const_iterator b;
        logmap_t::const_iterator e;
        logmap_t::const_iterator i;
        b = logmap.begin();
        e = logmap.end();
        for (i = b; i != e; i++)
            if (i->first == s)
            {
                result = true;
                break;
            }
        return result;
    }
};




//#include<fstream>
class Touter_cube: public Tdomain_surf 
{
protected:
 std::map<const Tplanesurf*, pair<TV3,TV3> > plane_gab;
 std::map<const Tplanesurf*, real> areas;
 get_rnd_distr_hist<const Tplanesurf*, real> plane_finder;
 std::pair<TV3,TV3> corners;
 Tplanesurf sides[6];
 pair<TV3,TV3> get_pt_plane(const Tplanesurf* rp)const
 {
  std::map<const Tplanesurf*, pair<TV3,TV3> >::const_iterator Where;
  Where=plane_gab.lower_bound(rp);
  if(Where==plane_gab.end())throw "Touter_cube::get_pt_plane(): ��� ������ ��������!!!";
  return Where->second;
 }
// std::ofstream os;
public:
 inline Touter_cube()//:os("points.dat")
 {
  TV3 dr,r0;
  dr=0.;
  r0=0.;
  init(dr,r0);
 }
 inline Touter_cube(const TV3& dr, const TV3& r0)
 {
  init(dr,r0);
 }
 inline std::pair<const Tplanesurf*,TV3> radom_pt_on_bound()const
 {
  const Tplanesurf *rp;
  TV3 p0,res;
  std::pair<const Tplanesurf*,TV3> r;
  int i;
  std::pair<TV3,TV3> pp;
  rp=plane_finder();
  p0=rp->local_crd.inip();
  pp=get_pt_plane(rp);
  for(i=0;i<SD;i++)
  {
   res(i)=random(pp.first(i),pp.second(i));
  }
  r.first=rp;
  r.second=res;
//  os<<res<<endl;
  return std::pair<const Tplanesurf*,TV3>(rp,res);
 }
 inline std::pair<const Tplanesurf*, Tline> random_ray_from_bound()const
 {
  Tline res,l;
  std::pair<const Tplanesurf*,TV3>  pp;
  pp=radom_pt_on_bound();
  l.p0=pp.second;
  l.direct=pp.first->normv(pp.second);
  l.update_ang();
  res=rnd_line(l);
  return std::pair<const Tplanesurf*, Tline>(pp.first,res);
 }
 inline TV3 rand_point()const
 {
  TV3 res;
  int i;
  for(i=0;i<SD;i++)
  {
   res(i)=random(corners.first(i),corners.second(i));
  }
  return res;
 }
 inline void init(const TV3& dr, const TV3& r0)
 {
  int i,j,k;
  flush();
  Tplanesurf *s1,*s2;
  real a;
  std::pair<TV3,TV3> vp;
  corners.first=r0-0.5*dr;
  corners.second=r0+0.5*dr;
  for(i=0,j=0;i<SD;i++,j+=2)
  {
   s1=sides+j;
   s2=s1+1;
   *s1=plane(ORTS[i],proection(r0,i,corners.first(i)));
   *s2=plane(ORTS[i],proection(r0,i,corners.second(i)));
   vp.first=proection(corners.first,i,s1->local_crd.inip()(i));
   vp.second=proection(corners.second,i,s1->local_crd.inip()(i));
   plane_gab[s1]=vp;
   vp.first=proection(corners.first,i,s2->local_crd.inip()(i));
   vp.second=proection(corners.second,i,s2->local_crd.inip()(i));
   plane_gab[s2]=vp;
   add_space(*s1,1);
   add_space(*s2,-1);
   a=1;
   for(k=0;k<SD;k++)
   {
    if(k!=i)
    {
     a*=dr(i);
    }
   }
   areas[s1]=a;
   areas[s2]=a;
  }
  plane_finder(areas);
 }
 inline void flush()
 {
  plane_gab.clear();
  areas.clear();
 }
 inline real volume()const
 {
  real v;
  int i;
  v=1.;
  for(i=0;i<SD;i++)
  {
   v*=corners.second(i)-corners.first(i);
  }
  return v;
 }
 inline real area()const
 {
  real res,a,b,c;
  a=corners.second(0)-corners.first(0);
  b=corners.second(1)-corners.first(1);
  c=corners.second(2)-corners.first(2);
  res=2*(a*b + b*c + a*c);
  return res;
 }
};




//! ����� ��������� ������������ ������� ������������
class Tlim_domain: public Tdomain_surf
{
    vector<Tplanesurf> gplane;
    const Touter_cube* outer_cube;
public:
  Tlim_domain(){}
  Tlim_domain(const Touter_cube& cube):outer_cube(&cube){}
  TV3 get_rnd_pt()const
    {
        static TV3 rp;
        rp=0.;
        for(;;)
        {
            rp=outer_cube->rand_point();
            if((*this)(rp))break;
        }
        return rp;
 }
  inline real volume(int n=1000)const
 {
  int in(0),i;
  static TV3 p;
  for(i=0;i<n;i++)
  {
   p=outer_cube->rand_point();
   if((*this)(p))in++;
  }
  return real(in)*outer_cube->volume()/n;
 }
  const Touter_cube& gabar()const{return *outer_cube;}
  void setg(const Touter_cube& c){outer_cube=&c;}
};

/*! ����� ��������� �������� ������� (�.�.����� �������, ��� ������������ ������������ 
��� ������ ����� �� ����� 2 ����� ����������� � �������� �������. ���������� ���������� 
������, ������������� �������� ������� ������� ����, �� ����������� ������ ���������� ��� 
����� ������� ������������� ����� ����� ����.)*/
class Tconvex_domain: public Tlim_domain
{
public:
 std::pair<real,real> ipoint_pair(const Tline& l)const
 {
  std::pair<real,real> res;
  Tdomain_surf::iptcon_t vr;
  real a;
  vr=ipoint(l);
  if(vr.size()>0)
  {
   res.first=vr[0];
   if(vr.size()>1)
   {
    res.second=vr[1];
   }
  }
  if(res.first>res.second) 
  {
   a=res.first;
   res.first=res.second;
   res.second=a;
  }
  return res;
 }
};

#endif           