#ifndef MC_INTERPOLATION_H
#define MC_INTERPOLATION_H

#include <vector>
#include <algorithm>

template<class real>
class linear_interpolation_1d_v
{
 typedef std::pair<real,real> tab_el_t;
 typedef std::vector<tab_el_t> tab_t;
 bool less(const tab_el_t& el1, const tab_el_t& el2)
 {
  return el1.first<el2.first;
 }
 int n;
 tab_t data;
public:
 linear_interpolation_1d_v():n(0){}
 int size()const{return n;}
 void set_data(real* _x, real* _y,int s)
 {
  n=s;
  data.resize(n);
  int i;
  for(i=0;i<n;i++)
  {
   data[i].first=_x[i];
   data[i].second=_y[i];
  }
  std::sort(data.begin(),data.end());
 }
 real operator()(real x)
 {
  tab_el_t xx;
  xx.first=x;
  tab_t::iterator d1,d2;
  real x1,x2,y1,y2,dx,dy;
  d1=std::lower_bound(data.begin(),data.end(),xx);
  d2=std::upper_bound(data.begin(),data.end(),xx);
  if(d1==d2)d1--;
  if(d2==data.begin())
  {
   return data[0].second;
  }
  else if(d1==data.end())
  {
   return data.back().second;
  }
  else
  {
   x1=d1->first;
   x2=d2->first;
   y1=d1->second;
   y2=d2->second;
   dx=x2-x1;
   dy=y2-y1;
   return y1+(x-x1)*dy/dx;
  }
 }
};

#include <include/int_bsearch.h>

template <class real>
class linear_interpolation_1d
{
 real *X, *Y;
 int Size;
public:
 linear_interpolation_1d(real* x=0, real* y=0, int s=0):X(x),Y(y),Size(s){}
 void set_data(real *x, real *y, int s){X=x;Y=y;Size=s;}
 int size()const{return Size;}
 real operator()(real x)
 {
  if(x<X[0])
  {
   return X[0];
  }
  else if(x>X[Size-1])
  {
   return X[Size-1];
  }
  else
  {
   int i=iint_bsearch<real>(X,X+Size-1,x);
   int j=i+1;
   return Y[i]+(Y[j]-Y[i])*(x-X[i])/(X[j]-X[i]);
  }
 }
};

#endif