#ifndef ADSORB_H
#define ADSORB_H

#define _USE_MATH_DEFINES

#include <utility>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <cmath>
#include "Treg_zone.h"
#include "transf_vector.h"
#include "serialize_inc.h"
#include "Tzone_tracker.h"

class __pless
{
 typedef std::pair<Treg_zone_vol*,real> _Ty;
public:
 bool operator()(const _Ty& x1, const _Ty& x2)
 {
  return x1.second<x2.second;
 }
};

class Tvol_adsorbtion_info
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a, int)
 {
  a & ARC_ADAPTOR(vol_zone);
//  a & ARC_ADAPTOR(sink);
//  a & ARC_ADAPTOR(goal);
  a & ARC_ADAPTOR(raylen_sum);
  a & ARC_ADAPTOR(count);
  a & ARC_ADAPTOR(raylen);
 }
public:
 inline Tvol_adsorbtion_info(Treg_zone_vol* vz=0):
  vol_zone(vz),
  raylen_sum(0),
  raylen(0),
  count(1),
  lsa(0),
  pads(0),
  pin(0),
  pout(0),
  Sg(0),
  Seff(0),
  Qerad(0),
  Qvol_in(0),
  Qvol_out(0),
  Qads(0),
  Qvol_res(0)
  { }
 Treg_zone_vol *vol_zone;
// Treg_zone *sink, *goal;
 real raylen_sum; //! ����� ���� �������� ���� �����, ���������� vol_zone
 real raylen; //! ������� ����� ���� ����
 real count; //! ����� �����
 real lsa; //! ������������ ������� ����� �� ������� ����������
 real weak; //! ����������� ���������� � ����
 real kads; //! ����������� ���������� (������� ������� ����, kads=1-exp(-lsa))
 real pads; //! ���� ������� ���������, ����������� � ���� vol_zone (kads*pin)
 real pin; //! ���� ������� ���������, �������� �� ���� vol_zone
 real pout; //! ���� ������� ���������, ��������� �� ���� vol_zone
 real Sg; //!�������������� ������� �����
 real Seff; //! ����������� ������� ����� ��� ������� ���������
 real Qerad; //! ��������� �� ������ � �����������, �������� ����������� ������
 real Qvol_in; //! ��������� �������, �������� � ���� vol_zone
 real Qvol_out; //! ��������� �������, ��������� �� ���� vol_zone
 real Qads; //! ���������, ����������� � ������
 real Qvol_res; //! ���������, ������ �� ������ ������
 inline void fin()
 {
  raylen=raylen_sum/count;
 }
 inline void update()
 {
  lsa=raylen*vol_zone->props().sigma_a;
  weak=exp(-lsa);
  kads=(1.-weak)*vol_zone->props().volradcor;
  weak=1-kads;
  if(weak<0)
  {
   weak=0;
  }
  pads=pin*kads;
  pout=pin*weak;
  Seff=Sg*kads;
  Qerad=StephanBoltsmanSigma*sq4(vol_zone->props().TMPR)*Seff;
//  Qerad=StephanBoltsmanSigma*sq4(vol_zone->props().TMPR)*Seff*vol_zone->props().volradcor;
  Qvol_out=Qvol_in*weak+Qerad;
  Qads=Qvol_in*kads;
  Qvol_res=Qvol_in-Qvol_out;
 }
};

class Tadsorbtion_link_info
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a, int)
 {
  a & ARC_ADAPTOR(node);
  a & ARC_ADAPTOR(koef);
 }
public:
 Tadsorbtion_link_info(int n=0,int k=1):node(n),koef(k){}
 int node;
 mutable real koef;
 bool operator<(const Tadsorbtion_link_info& i)const{return node<i.node;}
 void grow(real x)const{koef+=x;}
};

class Tadsorbtion_graph
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a, int)
 {
  a & ARC_ADAPTOR(zfrom);
  a & ARC_ADAPTOR(zto);
  a & ARC_ADAPTOR(index_begin);
  a & ARC_ADAPTOR(zind);
  a & ARC_ADAPTOR(links);
  a & ARC_ADAPTOR(infos);
  a & ARC_ADAPTOR(Valid);
/*  a & ARC_ADAPTOR(first_vol);
  a & ARC_ADAPTOR(last_vol);*/
 }
public:
 typedef Tzone_tracker::info_con_t::iterator zone_iter;
protected:
 int index_begin;
 zone_index<Treg_zone_vol> zind;
 std::vector<std::set<Tadsorbtion_link_info> > links;
 std::vector<std::set<Tadsorbtion_link_info> > links_from;
 std::vector<bool> used;
 std::vector<Tvol_adsorbtion_info> infos;
 void cook_a(int i,real,real Qin=0);//! ����� ����� ���������� (�������� BFS) � ������ ����� �������, �����������, ������������ �����, �� ������� ���������. ������� ��������� ����� ���������, ��������� � ����������� ���� ��������� ������� ���� � ����� ���������-������.
 void bfs(int i);//! ����� ����� ���������� (�������� BFS).
 void dfs(int i);//! ����� ����� ���������� (�������� DFS) ��� �������������� ����������.
 void topo_sort();//! �������������� ���������� ������� � ������
 Treg_zone* zfrom;
 Treg_zone* zto;
 bool Valid;
 inline void set_invalid(){Valid=false;}
 inline void set_valid(){Valid=true;}
 std::vector<int> bfs_order;
 std::vector<int> bfs_place;
public:
 Tadsorbtion_graph(){set_valid();}
 Tadsorbtion_graph(Treg_zone*f,Treg_zone*t):zfrom(f),zto(t){set_valid();}
 void insert_fw(zone_iter ii, zone_iter jj);
 void insert_bw(zone_iter ii, zone_iter jj);
 void update();
 void fin();
 void cook(real x, real Qin=0);
 void cook_bfs_order();
 Tvol_adsorbtion_info* get_adsorbtion_info(int v);
 Tvol_adsorbtion_info* get_adsorbtion_info(Treg_zone_vol* v);
 inline int size()const{return infos.size();}
 Treg_zone_vol *first_vol;
 Treg_zone_vol *last_vol;
 inline bool valid()const{return Valid;}
 void chk_infos();
 real s2s_ads(Treg_zone_vol* v);
};

bool check_valid(const Tadsorbtion_graph* g);

/*!
����� ��� ������� ���������� ������� ����� ������
*/
class Tray_adsorbtion
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a, int)
 {
  a & ARC_ADAPTOR(zones);
  a & ARC_ADAPTOR(zind);
  a & ARC_ADAPTOR(length_map);
  a & ARC_ADAPTOR(count_map);
  a & ARC_ADAPTOR(length);
  a & ARC_ADAPTOR(crd_map);
  a & ARC_ADAPTOR(crdv);
 }
public:
 typedef std::pair<Treg_zone*, Treg_zone*> zone_pair_t;
protected:
 zone_pair_t zones;
 zone_index<Treg_zone_vol> zind;
 std::map<Treg_zone_vol*,real> crd_map;
 std::map<Treg_zone_vol*,real> length_map;
 std::map<Treg_zone_vol*,real> count_map;
 transf_vector<Treg_zone_vol> length;
 std::vector<std::pair<Treg_zone_vol*,real> > crdv;
public:
 typedef std::map<Treg_zone_vol*,real>::iterator len_iter;
 inline Tray_adsorbtion(){}
 inline Tray_adsorbtion(Treg_zone* from, Treg_zone* to):zones(from,to){}
 inline void add_ray(Treg_zone_vol*, real l,real x);
 inline void update();
 inline real operator ()()const;
 inline real lsa(Treg_zone_vol* v)const;
 inline real lsa_full()const;
 inline real lsa_sum_to(Treg_zone_vol* v)const;
 inline real weak_last();
 inline real transit_part(Treg_zone_vol* v);
 inline std::map<Treg_zone_vol*,real>::iterator begin();
 inline std::map<Treg_zone_vol*,real>::iterator end();
 inline Treg_zone_vol* operator[](int i);
 inline int size()const;
};

inline int Tray_adsorbtion::size() const
{
 return zind.size();
}

inline Treg_zone_vol* Tray_adsorbtion::operator [](int i)
{
 return zind[i];
}

inline void Tray_adsorbtion::add_ray(Treg_zone_vol *z, real l, real x)
{
 zind.push(z);
 length_map[z]+=l;
 crd_map[z]+=x;
 count_map[z]+=1.;
}

inline void Tray_adsorbtion::update()
{
 int i,n;
 Treg_zone_vol* z;
 length.set_index(zind);
 n=length.size();
 for(i=0;i<n;i++)
 {
  z=zind[i];
  length[i]=length_map[z]/count_map[z];
 }
 std::map<Treg_zone_vol*,real>::iterator it;
 for(it=crd_map.begin();it!=crd_map.end();it++)
 {
  crdv.push_back(*it);
  crdv.back().second=it->second/count_map[it->first];
 }
 std::sort(crdv.begin(),crdv.end(),__pless());
 n=crdv.size();
 zind.clr();
 for(i=0;i<n;i++)
 {
  zind.push(crdv[i].first);
 }
}

inline real Tray_adsorbtion::operator ()() const
{
 Treg_zone_vol* z;
 real res,a,l,x;
 int n,i;
 n=zind.size();
 res=0;
 for(i=0;i<n;i++)
 {
  z=zind[i];
/*  if(z==zones.first)x=.;
  else x=1.;*/
  a=z->props().sigma_a;
  res+=length[i]*a;
 }
 return res;
}

inline real Tray_adsorbtion::lsa(Treg_zone_vol *v) const
{
 real res;
 if(length_map.find(v)!=length_map.end())
 {
  res=length(v);
  res*=v->props().sigma_a;
 }
 else
 {
  res=0;
 }
 return res;
}

inline real Tray_adsorbtion::lsa_sum_to(Treg_zone_vol *v) const
{
 int s=zind.sfind_index(v)/*[v]*/,i,ib;
 Treg_zone_vol *vi;
 real res;
 if(zones.first==zind[(int)0])
 {
  ib=1;
 }
 else 
 {
  ib=0;
 }
 res=0;
 for(i=ib;i<s;i++)
 {
  vi=zind[i];
  res+=length(vi)*vi->props().sigma_a;
 }
 return res;
}
inline real Tray_adsorbtion::lsa_full() const
{
 int s=zind.size()/*[v]*/,i,ib;
 Treg_zone_vol *vi;
 real res;
 if(zones.first==zind[(int)0])
 {
  ib=1;
 }
 else 
 {
  ib=0;
 }
 res=0;
 for(i=ib;i<s;i++)
 {
  vi=zind[i];
  res+=length(vi)*vi->props().sigma_a;
 }
 return res;
}
inline Tray_adsorbtion::len_iter Tray_adsorbtion::begin()
{
 return length_map.begin();
}

inline Tray_adsorbtion::len_iter Tray_adsorbtion::end()
{
 return length_map.end();
}

inline real Tray_adsorbtion::transit_part(Treg_zone_vol* v)
{
 real res;
 int nil=0;
 Treg_zone_vol *v0=zind[nil];
 real n0=count_map[v0];
 real nv=count_map[v];
 res=nv/n0;
 return res;
}


#include <exception>

/*!������� ���������� ����������� ���������� ��������� � ��������� ���� ����� �����*/
inline real Tray_adsorbtion::weak_last()
{
 real sa,ex,len,aa;
 Treg_zone* j=zones.second;
 if(j->my_type()==RZ_VOL)
 {
  if(j==zones.first)aa=0.;
  else aa=1.;
  len=length(static_cast<Treg_zone_vol*>(j));
  sa=static_cast<Treg_zone_vol*>(j)->props().sigma_a;
  ex=sa*len*aa;
 }
 else
 {
  throw std::exception("Debug Error: ������� ������� real Tray_adsorbtion::weak_last() ��� ������������ ��� (��������� ���� ������ ���� ��������). ��������� ���������!");
 }
 return ex;
}

class Tray_adsorbtion_array
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a, int)
 {
  a & ARC_ADAPTOR(ads_map);
 }
 typedef Tray_adsorbtion::zone_pair_t zone_pair_t;
// std::map<zone_pair_t,Tray_adsorbtion*> ads_map;
 std::map<zone_pair_t,Tadsorbtion_graph> ads_map;
 typedef Tzone_tracker::info_con_t::iterator zone_iter;
public:
//! �������� ����������� ���������� ����� ������ zfrom � zto
// Tray_adsorbtion* operator()(Treg_zone* zfrom,Treg_zone* zto);
 inline Tadsorbtion_graph* operator()(Treg_zone* zfrom,Treg_zone* zto);
//! �������� ����� ������� ����� ������ zfrom � zto � �������� ���� zm
// void insert(Treg_zone* zfrom, Treg_zone* zto, Treg_zone_vol*, real l, real x);
 inline void insert(zone_iter zfrom, zone_iter zto,int);
 inline void insert_bw(zone_iter zfrom, zone_iter zto);
 inline void insert_fw(zone_iter zfrom, zone_iter zto);
//! �������� ��� ���������
 inline void clear();
//! ����������� ��� ������� ����� ��������
 inline void update();
 inline void prepare();
// ~Tray_adsorbtion_array();
};


Tadsorbtion_graph* Tray_adsorbtion_array::operator()(Treg_zone* zfrom,Treg_zone* zto)
{
 std::map<zone_pair_t,Tadsorbtion_graph>::iterator f;
 f=ads_map.find(tie(zfrom,zto));
 if(f!=ads_map.end())
 {
  return &(f->second);
 }
 else 
 {
  return 0;
 }
}

void Tray_adsorbtion_array::insert(Tray_adsorbtion_array::zone_iter from, Tray_adsorbtion_array::zone_iter to,int g)
{
 Tadsorbtion_graph* f;
 Treg_zone *zfrom=from->rz;
 Treg_zone *zto=to->rz;
 f=(*this)(zfrom,zto);
 auto it=ads_map.begin();
 if(f==NULL)
 {
  it=ads_map.insert(it,tie(tie(zfrom,zto),Tadsorbtion_graph(zfrom,zto)));
  f=&(it->second);
 }
 if(g==1)
 {
  f->insert_fw(from,to);
 }
 if(g==-1)
 {
  f->insert_bw(from,to);
 }
}

void Tray_adsorbtion_array::insert_fw(Tray_adsorbtion_array::zone_iter from, Tray_adsorbtion_array::zone_iter to)
{
 insert(from,to,1);
}
void Tray_adsorbtion_array::insert_bw(Tray_adsorbtion_array::zone_iter from, Tray_adsorbtion_array::zone_iter to)
{
 insert(from,to,-1);
}

inline void Tray_adsorbtion_array::clear()
{
 ads_map.clear();
}

inline void Tray_adsorbtion_array::update()
{
 std::map<zone_pair_t,Tadsorbtion_graph>::iterator i,b,e;
 b=ads_map.begin();
 e=ads_map.end();
 for(i=b;i!=e;i++)
 {
  i->second.fin();
 }
}

inline void Tray_adsorbtion_array::prepare()
{
 std::map<zone_pair_t,Tadsorbtion_graph>::iterator i,b,e;
 b=ads_map.begin();
 e=ads_map.end();
 for(i=b;i!=e;i++)
 {
  i->second.update();
 }
}


/*
inline Tray_adsorbtion* Tray_adsorbtion_array::operator ()(Treg_zone *zfrom, Treg_zone *zto)
{
 std::map<zone_pair_t,Tray_adsorbtion*>::iterator f;
 f=ads_map.find(tie(zfrom,zto));
 if(f!=ads_map.end())
 {
  return f->second;
 }
 else 
 {
  return 0;
 }
}

inline void Tray_adsorbtion_array::insert(Treg_zone *zfrom, Treg_zone *zto, Treg_zone_vol *zm, real l,real x)
{
 Tray_adsorbtion* f;
 f=(*this)(zfrom,zto);
 if(f==NULL)
 {
  f=new Tray_adsorbtion(zfrom,zto);
  ads_map[tie(zfrom,zto)]=f;
 }
 f->add_ray(zm,l,x);
}

inline void Tray_adsorbtion_array::clear()
{
 std::map<zone_pair_t,Tray_adsorbtion*>::iterator b,e,i;
 b=ads_map.begin();
 e=ads_map.end();
 for(i=b;i!=e;i++)
 {
  delete i->second;
 }
 ads_map.clear();
}

inline Tray_adsorbtion_array::~Tray_adsorbtion_array()
{
 clear();
}

inline void Tray_adsorbtion_array::update()
{
 std::map<zone_pair_t,Tray_adsorbtion*>::iterator i,b,e;
 b=ads_map.begin();
 e=ads_map.end();
 for(i=b;i!=e;i++)
 {
  i->second->update();
 }
}
*/



#endif