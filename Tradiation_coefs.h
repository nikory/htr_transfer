#ifndef TRADIATION_COEFS_H
#define TRADIATION_COEFS_H

#include <iostream>
#include "matr_t.h"
#include "ray_tracker.h"
#include "serialize_inc.h"

/*!
������ ����� ������������ ��� �������� ������ � �������������� ��������� �������� ���������
*/

class Tradiation_coefs
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR(matr_X);
  a & ARC_ADAPTOR(matr_S);
  a & ARC_ADAPTOR(matr_R);
  a & ARC_ADAPTOR(matr_P);
  a & ARC_ADAPTOR(matr_A);
  a & ARC_ADAPTOR(map_L);
  a & ARC_ADAPTOR(map_F);
  a & ARC_ADAPTOR(map_C);
 }
protected:
 zzReal_matr_t matr_X0;
 zzReal_matr_t matr_X;
 zzReal_matr_t matr_S;
 ssReal_matr_t matr_R;
 ssReal_matr_t matr_P;
 ssReal_matr_t matr_A;
 ssReal_map_t map_L;
 ssReal_map_t map_F;
 ssReal_map_t map_C;
 matr_comns<real> areas;
 matr_com_lux<real> solver;
 matr_com_lux<real> solver_V;
 void build_matrices_msq(Tray_tracker& rt);
 void build_matrices_Glr(Tray_tracker& rt);
 void build_freecf_msq(Tray_tracker& rt);
 void build_freecf_Glr(Tray_tracker& rt);
public:
 vvReal_matr_t matr_V;
 vvReal_map_t map_V;
 Tradiation_coefs();
 void check_x0(const char* fn, Tray_tracker& rt);
 void alloc_matrices(Tray_tracker& rt);
 void build_matrices(Tray_tracker& rt);
 void build_freecf(Tray_tracker& rt);
 void volume_rad(Tray_tracker& rt);
 void account_ads(Tray_tracker& rt);
 void factor_system();
 void solve_system(ssReal_map_t&);
 void factor_system_V();
 void solve_system_V(vvReal_map_t&);
 void ptR(std::ostream &s)const;
 void ptP(std::ostream &s)const;
 void ptA(std::ostream &s)const;
 void ptX(std::ostream &s)const;
 void ptF(std::ostream &s)const;
 void ptL(std::ostream &s)const;
 void ptS(std::ostream &s)const;
 void ptC(std::ostream &s)const;
 real vol_area(Treg_zone_surf* s, Treg_zone_vol* v)const;
 real& vol_area(Treg_zone_surf* s, Treg_zone_vol* v);
 real X(Treg_zone* i, Treg_zone *j)const;
 real S(Treg_zone* i, Treg_zone *j)const;
};

#endif