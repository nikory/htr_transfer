#ifndef TRACKING_CINT_H
#define TRACKING_CINT_H

#ifdef __MAKECINT__
void StartMC(int,int);
void StartMC(int);
void PrintStat(const char*);
void PrintStat();
void PrintCoef(const char*);
void PrintCoef();
#endif

#ifndef __MAKECINT__

#include <iostream>
#include <fstream>
#include "../ray_tracker.h"
#include "../Tradiation_coefs.h"

extern Tray_tracker GTRACKER;
extern Tradiation_coefs GRCOEFS;

inline void StartMC(int n, int q=1000000)
{
 GTRACKER.set_rubik(GRUBIK);
 GTRACKER.generate_link_mtr(n,q);
 GTRACKER.correct_vols();
 GRCOEFS.alloc_matrices(GTRACKER);
 GRCOEFS.build_matrices(GTRACKER);
}

inline void PrintStat(const char* fnm)
{
 using namespace std;
 ofstream os(fnm);   
 GTRACKER.ptstat(os);
 os.flush();
 os.close();
}

inline void PrintStat()
{
 GTRACKER.ptstat(std::cout);
}

inline void PrintCoef()
{
 GRCOEFS.ptX(std::cout);  
}


inline void PrintCoef(const char* fnm)
{
 using namespace std;
 ofstream os(fnm);   
 GRCOEFS.ptX(os);
 os.flush();
 os.close();
}

#endif

#endif