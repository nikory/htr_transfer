#ifndef PLANE_ARRAY_H
#define PLANE_ARRAY_H

#ifndef __MAKECINT__
#include "../line.h"
#include "../surface.h"
#include <vector>
#endif

#ifndef __MAKECINT__
#include <include/tmatr.h>
#endif
#include "condition_cint.h"

template<class T,int N, int M> class MatNM;

typedef MatNM<double,3,1> TV3;


#ifdef __MAKECINT__
class Tcondition;
//class Tsurf;
class Tplanesurf;
#endif

class PlaneArray
{
#ifndef __MAKECINT__
 std::vector<Tplanesurf> planes;
 std::vector<Tsurf_ge> planes_ge;
#endif
public:
 PlaneArray(const Direction&, const real**, int);
 Tplanesurf& operator[](int i);
 const Tplanesurf& operator[](int i)const;
};

#ifndef __MAKECINT__

inline PlaneArray::PlaneArray(const Direction &dir, const real ** parr, int n)
{
 int i,j;
 Tline l;
 l.direct=dir;
 l.update_ang();
 for(i=0,j=0;i<n;i++,j+=3)
 {
  l.p0=((const real*)parr)+j;
  planes.push_back(Tplanesurf(l));
  planes_ge.push_back(Tsurf_ge(planes[i]));
 }
}

inline Tplanesurf& PlaneArray::operator [](int i)
{
 return planes[i];
}

inline const Tplanesurf& PlaneArray::operator [](int i) const
{
 return planes[i];
}

#endif
                                            
#endif