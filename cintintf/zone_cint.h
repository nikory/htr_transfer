#ifndef ZONE_CINT_H
#define ZONE_CINT_H

#ifndef __MAKECINT__
#include "../Treg_zone.h"
#include "../rubik.h"
#include "../serialize_inc.h"
#include "plane.h"
#endif

#include "condition_cint.h"

#ifdef __MAKECINT__
class Treg_zone_vol;
class Treg_zone_surf;
class rubik;
#endif

extern rubik GRUBIK;

class VolumeZone: public Treg_zone_vol
{
#ifndef __MAKECINT__
 friend class boost::serialization::access;
 template<class Arch>
 inline void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("Treg_zone_vol",boost::serialization::base_object<Treg_zone_vol>(*this));
 }
 VolumeZone():Treg_zone_vol(){}
#endif
public:
 VolumeZone(Tcondition&, real);
};

class SurfaceZone: public Treg_zone_surf
{
#ifndef __MAKECINT__
 friend class boost::serialization::access;
 template<class Arch>
 inline void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("Treg_zone_surf",boost::serialization::base_object<Treg_zone_surf>(*this));
 }
 SurfaceZone():Treg_zone_surf(){}
#endif
public:
 SurfaceZone(VolumeZone&,Tplanesurf&,real);
 SurfaceZone(VolumeZone&,Plane&,real);
};


#ifndef __MAKECINT__

VolumeZone::VolumeZone(Tcondition& d, real v):Treg_zone_vol(d,v)
{
 Treg_zone_vol* z=static_cast<Treg_zone_vol*>(this);
 GRUBIK.rz_set.insert(z);
 GRUBIK.rzv_set.insert(z);
 GRUBIK.zone_index_com.push(z);
 GRUBIK.zone_index_vol.push(z);
}

SurfaceZone::SurfaceZone(VolumeZone &v, Tplanesurf &s, real a):Treg_zone_surf(v,s,a)
{
 Treg_zone_surf* z=static_cast<Treg_zone_surf*>(this);
 GRUBIK.rz_set.insert(z);
 GRUBIK.rzs_set.insert(z);
 GRUBIK.zone_index_com.push(z);
 GRUBIK.zone_index_surf.push(z);
}

SurfaceZone::SurfaceZone(VolumeZone &v, Plane &s, real a):Treg_zone_surf(v,s,a)
{
 Treg_zone_surf* z=static_cast<Treg_zone_surf*>(this);
 GRUBIK.rz_set.insert(z);
 GRUBIK.rzs_set.insert(z);
 GRUBIK.zone_index_com.push(z);
 GRUBIK.zone_index_surf.push(z);
}

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Treg_zone_vol)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(Treg_zone_surf)

#endif

#endif
