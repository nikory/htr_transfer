//---------------------------------------------------------------------------

#ifndef paletteH
#define paletteH
//---------------------------------------------------------------------------
class Tpal
{
public:
 virtual int operator()(double val) const =0;
};

class Tvectpal: public Tpal
{
 const int *v;
 int n;
public:
 Tvectpal(int n_,const int* data):v(data),n(n_){}
 virtual int operator()(double val) const
 {
  int ind=val*(n-1);
//  int ind=int(val*(n-1));
  if(ind<0)ind=0;
  if(ind>=n){ind=n-1;}
  return v[ind];
 }
};
extern Tvectpal pal_cupper,pal_gbr,pal_bw,pal_hot;
#endif
