/********************************************************
* G__cpp_int.cpp
********************************************************/
#include "G__cpp_int.h"

#ifdef G__MEMTEST
#undef malloc
#undef free
#endif

extern "C" void G__cpp_reset_tagtable();

extern "C" void G__set_cpp_environment() {
  G__add_compiledheader("condition_cint.h");
  G__add_compiledheader("direction.h");
  G__add_compiledheader("gabarites.h");
  G__add_compiledheader("plane.h");
  G__add_compiledheader("plane_array.h");
  G__add_compiledheader("tv3_cint.h");
  G__add_compiledheader("zone_cint.h");
  G__add_compiledheader("tracking_cint.h");
  G__add_compiledheader("oarch_cint.h");
  G__cpp_reset_tagtable();
}
class G__intdOcpp_tag {};

void* operator new(size_t size,G__intdOcpp_tag* p) {
  if(p && G__PVOID!=G__getgvp()) return((void*)p);
#ifndef G__ROOT
  return(malloc(size));
#else
  return(::operator new(size));
#endif
}

/* dummy, for exception */
#ifdef G__EH_DUMMY_DELETE
void operator delete(void *p,G__intdOcpp_tag* x) {
  if((long)p==G__getgvp() && G__PVOID!=G__getgvp()) return;
#ifndef G__ROOT
  free(p);
#else
  ::operator delete(p);
#endif
}
#endif

static void G__operator_delete(void *p) {
  if((long)p==G__getgvp() && G__PVOID!=G__getgvp()) return;
#ifndef G__ROOT
  free(p);
#else
  ::operator delete(p);
#endif
}

void G__DELDMY_intdOcpp() { G__operator_delete(0); }

extern "C" int G__cpp_dllrev() { return(30051515); }

/*********************************************************
* Member function Interface Method
*********************************************************/

/* Direction */
static int G__intdef_0_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Direction *p=NULL;
   if(G__getaryconstruct()) p=new Direction[G__getaryconstruct()];
   else p=::new((G__intdOcpp_tag*)G__getgvp()) Direction;
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_Direction);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_0_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Direction *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) Direction(
(real)G__double(libp->para[0]),(real)G__double(libp->para[1])
,(real)G__double(libp->para[2]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_Direction);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_0_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Direction& obj=((Direction*)(G__getstructoffset()))->operator=((const real*)G__int(libp->para[0]));
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__intdef_0_3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash)
{
   Direction *p;
   void *xtmp = (void*)G__int(libp->para[0]);
   p=new Direction(*(Direction*)xtmp);
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_Direction);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef Direction G__TDirection;
static int G__intdef_0_4_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (Direction *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((Direction *)((G__getstructoffset())+sizeof(Direction)*i))->~G__TDirection();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((Direction *)(G__getstructoffset()))->~G__TDirection();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}


/* Tcondition */
static int G__intdef_1_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__letint(result7,103,(long)((Tcondition*)(G__getstructoffset()))->operator()((const real*)G__int(libp->para[0])));
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic default constructor
static int G__intdef_1_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Tcondition *p;
   if(G__getaryconstruct()) p=new Tcondition[G__getaryconstruct()];
   else p=::new((G__intdOcpp_tag*)G__getgvp()) Tcondition;
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_Tcondition);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__intdef_1_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash)
{
   Tcondition *p;
   void *xtmp = (void*)G__int(libp->para[0]);
   p=new Tcondition(*(Tcondition*)xtmp);
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_Tcondition);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef Tcondition G__TTcondition;
static int G__intdef_1_3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (Tcondition *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((Tcondition *)((G__getstructoffset())+sizeof(Tcondition)*i))->~G__TTcondition();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((Tcondition *)(G__getstructoffset()))->~G__TTcondition();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__intdef_1_4_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Tcondition *dest = (Tcondition*)(G__getstructoffset());
   const Tcondition& obj = *dest;
   result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* Gabarites */
static int G__intdef_6_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Gabarites *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) Gabarites(
(real)G__double(libp->para[0]),(real)G__double(libp->para[1])
,(real)G__double(libp->para[2]),(real)G__double(libp->para[3])
,(real)G__double(libp->para[4]),(real)G__double(libp->para[5]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_Gabarites);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__intdef_6_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash)
{
   Gabarites *p;
   void *xtmp = (void*)G__int(libp->para[0]);
   p=new Gabarites(*(Gabarites*)xtmp);
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_Gabarites);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef Gabarites G__TGabarites;
static int G__intdef_6_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (Gabarites *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((Gabarites *)((G__getstructoffset())+sizeof(Gabarites)*i))->~G__TGabarites();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((Gabarites *)(G__getstructoffset()))->~G__TGabarites();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__intdef_6_3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Gabarites *dest = (Gabarites*)(G__getstructoffset());
   const Gabarites& obj = *dest;
   result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* PlaneArray */
static int G__intdef_8_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   PlaneArray *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) PlaneArray(
*(Direction*)libp->para[0].ref,(const real**)G__int(libp->para[1])
,(int)G__int(libp->para[2]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_PlaneArray);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_8_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tplanesurf& obj=((PlaneArray*)(G__getstructoffset()))->operator[]((int)G__int(libp->para[0]));
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_8_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tplanesurf& obj=((const PlaneArray*)(G__getstructoffset()))->operator[]((int)G__int(libp->para[0]));
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__intdef_8_3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash)
{
   PlaneArray *p;
   void *xtmp = (void*)G__int(libp->para[0]);
   p=new PlaneArray(*(PlaneArray*)xtmp);
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_PlaneArray);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef PlaneArray G__TPlaneArray;
static int G__intdef_8_4_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (PlaneArray *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((PlaneArray *)((G__getstructoffset())+sizeof(PlaneArray)*i))->~G__TPlaneArray();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((PlaneArray *)(G__getstructoffset()))->~G__TPlaneArray();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__intdef_8_5_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   PlaneArray *dest = (PlaneArray*)(G__getstructoffset());
   const PlaneArray& obj = *dest;
   result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* Plane */
static int G__intdef_9_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Plane *p=NULL;
   if(G__getaryconstruct()) p=new Plane[G__getaryconstruct()];
   else p=::new((G__intdOcpp_tag*)G__getgvp()) Plane;
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_Plane);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_9_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Plane *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) Plane(*(Direction*)libp->para[0].ref,(const real*)G__int(libp->para[1]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_Plane);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_9_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Plane *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) Plane(
(const real*)G__int(libp->para[0]),(const real*)G__int(libp->para[1])
,(const real*)G__int(libp->para[2]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_Plane);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_9_3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Plane *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) Plane((const real**)G__int(libp->para[0]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_Plane);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__intdef_9_4_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash)
{
   Plane *p;
   void *xtmp = (void*)G__int(libp->para[0]);
   p=new Plane(*(Plane*)xtmp);
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_Plane);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef Plane G__TPlane;
static int G__intdef_9_5_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (Plane *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((Plane *)((G__getstructoffset())+sizeof(Plane)*i))->~G__TPlane();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((Plane *)(G__getstructoffset()))->~G__TPlane();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__intdef_9_6_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   Plane *dest = (Plane*)(G__getstructoffset());
   const Plane& obj = *dest;
   result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* VolumeZone */
static int G__intdef_13_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   VolumeZone *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) VolumeZone(*(Tcondition*)libp->para[0].ref,(real)G__double(libp->para[1]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_VolumeZone);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__intdef_13_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash)
{
   VolumeZone *p;
   void *xtmp = (void*)G__int(libp->para[0]);
   p=new VolumeZone(*(VolumeZone*)xtmp);
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_VolumeZone);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef VolumeZone G__TVolumeZone;
static int G__intdef_13_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (VolumeZone *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((VolumeZone *)((G__getstructoffset())+sizeof(VolumeZone)*i))->~G__TVolumeZone();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((VolumeZone *)(G__getstructoffset()))->~G__TVolumeZone();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__intdef_13_3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   VolumeZone *dest = (VolumeZone*)(G__getstructoffset());
   const VolumeZone& obj = *dest;
   result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* SurfaceZone */
static int G__intdef_14_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   SurfaceZone *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) SurfaceZone(
*(VolumeZone*)libp->para[0].ref,*(Tplanesurf*)libp->para[1].ref
,(real)G__double(libp->para[2]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_SurfaceZone);
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef_14_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   SurfaceZone *p=NULL;
      p=::new((G__intdOcpp_tag*)G__getgvp()) SurfaceZone(
*(VolumeZone*)libp->para[0].ref,*(Plane*)libp->para[1].ref
,(real)G__double(libp->para[2]));
      result7->obj.i = (long)p;
      result7->ref = (long)p;
      result7->type = 'u';
      result7->tagnum = G__get_linked_tagnum(&G__LN_SurfaceZone);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic copy constructor
static int G__intdef_14_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash)
{
   SurfaceZone *p;
   void *xtmp = (void*)G__int(libp->para[0]);
   p=new SurfaceZone(*(SurfaceZone*)xtmp);
   result7->obj.i = (long)p;
   result7->ref = (long)p;
   result7->type = 'u';
   result7->tagnum = G__get_linked_tagnum(&G__LN_SurfaceZone);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef SurfaceZone G__TSurfaceZone;
static int G__intdef_14_3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (SurfaceZone *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((SurfaceZone *)((G__getstructoffset())+sizeof(SurfaceZone)*i))->~G__TSurfaceZone();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((SurfaceZone *)(G__getstructoffset()))->~G__TSurfaceZone();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__intdef_14_4_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   SurfaceZone *dest = (SurfaceZone*)(G__getstructoffset());
   const SurfaceZone& obj = *dest;
   result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* Setting up global function */
static int G__intdef__0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator*(*(Tcondition*)libp->para[0].ref,*(Tcondition*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator+(*(Tcondition*)libp->para[0].ref,*(Tcondition*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator-(*(Tcondition*)libp->para[0].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__3_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator-(*(Tcondition*)libp->para[0].ref,*(Tcondition*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__4_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator*(*(Tplanesurf*)libp->para[0].ref,*(Tplanesurf*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__5_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator*(*(Tplanesurf*)libp->para[0].ref,*(Tcondition*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__6_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator*(*(Tcondition*)libp->para[0].ref,*(Tplanesurf*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__7_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator+(*(Tplanesurf*)libp->para[0].ref,*(Tplanesurf*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__8_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator+(*(Tplanesurf*)libp->para[0].ref,*(Tcondition*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__9_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator+(*(Tcondition*)libp->para[0].ref,*(Tplanesurf*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__0_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator-(*(Tplanesurf*)libp->para[0].ref,*(Tplanesurf*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__1_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator-(*(Tplanesurf*)libp->para[0].ref,*(Tcondition*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__2_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator-(*(Tcondition*)libp->para[0].ref,*(Tplanesurf*)libp->para[1].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__3_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      {
        const Tcondition& obj=operator-(*(Tplanesurf*)libp->para[0].ref);
         result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
      }
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__4_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      SetGabarites((real)G__double(libp->para[0]),(real)G__double(libp->para[1])
,(real)G__double(libp->para[2]),(real)G__double(libp->para[3])
,(real)G__double(libp->para[4]),(real)G__double(libp->para[5]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__5_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      StartMC((int)G__int(libp->para[0]),(int)G__int(libp->para[1]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__6_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      StartMC((int)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__7_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      PrintStat((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__8_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      PrintStat();
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__9_1(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      PrintCoef((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__0_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      PrintCoef();
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__1_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      SaveModel((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__2_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      LoadModel((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__3_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      MakeIDF((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__4_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      MakeDDF((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__5_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      MakeLinkSubr((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__6_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      CheckX0((const char*)G__int(libp->para[0]));
   return(1 || funcname || hash || result7 || libp) ;
}

static int G__intdef__7_2(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__setnull(result7);
      BuildNameMap();
   return(1 || funcname || hash || result7 || libp) ;
}


/*********************************************************
* Member function Stub
*********************************************************/

/* Direction */

/* Tcondition */

/* Gabarites */

/* PlaneArray */

/* Plane */

/* VolumeZone */

/* SurfaceZone */

/*********************************************************
* Global function Stub
*********************************************************/

/*********************************************************
* Get size of pointer to member function
*********************************************************/
class G__Sizep2memfunc {
 public:
  G__Sizep2memfunc() {p=&G__Sizep2memfunc::sizep2memfunc;}
    size_t sizep2memfunc() { return(sizeof(p)); }
  private:
    size_t (G__Sizep2memfunc::*p)();
};

size_t G__get_sizep2memfunc()
{
  G__Sizep2memfunc a;
  G__setsizep2memfunc((int)a.sizep2memfunc());
  return((size_t)a.sizep2memfunc());
}


/*********************************************************
* virtual base class offset calculation interface
*********************************************************/

   /* Setting up class inheritance */

/*********************************************************
* Inheritance information setup/
*********************************************************/
extern "C" void G__cpp_setup_inheritance() {

   /* Setting up class inheritance */
   if(0==G__getnumbaseclass(G__get_linked_tagnum(&G__LN_Gabarites))) {
     Gabarites *G__Lderived;
     G__Lderived=(Gabarites*)0x1000;
     {
       Touter_cube *G__Lpbase=(Touter_cube*)G__Lderived;
       G__inheritance_setup(G__get_linked_tagnum(&G__LN_Gabarites),G__get_linked_tagnum(&G__LN_Touter_cube),(long)G__Lpbase-(long)G__Lderived,1,1);
     }
   }
   if(0==G__getnumbaseclass(G__get_linked_tagnum(&G__LN_Plane))) {
     Plane *G__Lderived;
     G__Lderived=(Plane*)0x1000;
     {
       Tplanesurf *G__Lpbase=(Tplanesurf*)G__Lderived;
       G__inheritance_setup(G__get_linked_tagnum(&G__LN_Plane),G__get_linked_tagnum(&G__LN_Tplanesurf),(long)G__Lpbase-(long)G__Lderived,1,1);
     }
   }
   if(0==G__getnumbaseclass(G__get_linked_tagnum(&G__LN_VolumeZone))) {
     VolumeZone *G__Lderived;
     G__Lderived=(VolumeZone*)0x1000;
     {
       Treg_zone_vol *G__Lpbase=(Treg_zone_vol*)G__Lderived;
       G__inheritance_setup(G__get_linked_tagnum(&G__LN_VolumeZone),G__get_linked_tagnum(&G__LN_Treg_zone_vol),(long)G__Lpbase-(long)G__Lderived,1,1);
     }
   }
   if(0==G__getnumbaseclass(G__get_linked_tagnum(&G__LN_SurfaceZone))) {
     SurfaceZone *G__Lderived;
     G__Lderived=(SurfaceZone*)0x1000;
     {
       Treg_zone_surf *G__Lpbase=(Treg_zone_surf*)G__Lderived;
       G__inheritance_setup(G__get_linked_tagnum(&G__LN_SurfaceZone),G__get_linked_tagnum(&G__LN_Treg_zone_surf),(long)G__Lpbase-(long)G__Lderived,1,1);
     }
   }
}

/*********************************************************
* typedef information setup/
*********************************************************/
extern "C" void G__cpp_setup_typetable() {

   /* Setting up typedef entry */
   G__search_typename2("real",100,-1,0,
-1);
   G__setnewtype(-1,NULL,0);
   G__search_typename2("TV3",117,G__get_linked_tagnum(&G__LN_MatNMlEdoublecO3cO1gR),0,-1);
   G__setnewtype(-1,NULL,0);
}

/*********************************************************
* Data Member information setup/
*********************************************************/

   /* Setting up class,struct,union tag member variable */

   /* Direction */
static void G__setup_memvarDirection(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__LN_Direction));
   { Direction *p; p=(Direction*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}


   /* Tcondition */
static void G__setup_memvarTcondition(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__LN_Tcondition));
   { Tcondition *p; p=(Tcondition*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}


   /* Gabarites */
static void G__setup_memvarGabarites(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__LN_Gabarites));
   { Gabarites *p; p=(Gabarites*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}


   /* PlaneArray */
static void G__setup_memvarPlaneArray(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__LN_PlaneArray));
   { PlaneArray *p; p=(PlaneArray*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}


   /* Plane */
static void G__setup_memvarPlane(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__LN_Plane));
   { Plane *p; p=(Plane*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}


   /* VolumeZone */
static void G__setup_memvarVolumeZone(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__LN_VolumeZone));
   { VolumeZone *p; p=(VolumeZone*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}


   /* SurfaceZone */
static void G__setup_memvarSurfaceZone(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__LN_SurfaceZone));
   { SurfaceZone *p; p=(SurfaceZone*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}

extern "C" void G__cpp_setup_memvar() {
}
/***********************************************************
************************************************************
************************************************************
************************************************************
************************************************************
************************************************************
************************************************************
***********************************************************/

/*********************************************************
* Member function information setup for each class
*********************************************************/
static void G__setup_memfuncDirection(void) {
   /* Direction */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__LN_Direction));
   G__memfunc_setup("Direction",929,G__intdef_0_0_0,105,G__get_linked_tagnum(&G__LN_Direction),-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("Direction",929,G__intdef_0_1_0,105,G__get_linked_tagnum(&G__LN_Direction),-1,0,3,1,1,0,
"d - 'real' 0 - - d - 'real' 0 - - "
"d - 'real' 0 - -",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("operator=",937,G__intdef_0_2_0,117,G__get_linked_tagnum(&G__LN_Direction),-1,1,1,1,1,0,"D - 'real' 10 - -",(char*)NULL,(void*)NULL,0);
   // automatic copy constructor
   G__memfunc_setup("Direction",929,G__intdef_0_3_0,(int)('i'),G__get_linked_tagnum(&G__LN_Direction),-1,0,1,1,1,0,"u 'Direction' - 11 - -",(char*)NULL,(void*)NULL,0);
   // automatic destructor
   G__memfunc_setup("~Direction",1055,G__intdef_0_4_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}

static void G__setup_memfuncTcondition(void) {
   /* Tcondition */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__LN_Tcondition));
   G__memfunc_setup("operator()",957,G__intdef_1_0_0,103,-1,-1,0,1,1,1,0,"D - 'real' 10 - -",(char*)NULL,(void*)NULL,0);
   // automatic default constructor
   G__memfunc_setup("Tcondition",1051,G__intdef_1_1_0,(int)('i'),G__get_linked_tagnum(&G__LN_Tcondition),-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic copy constructor
   G__memfunc_setup("Tcondition",1051,G__intdef_1_2_0,(int)('i'),G__get_linked_tagnum(&G__LN_Tcondition),-1,0,1,1,1,0,"u 'Tcondition' - 11 - -",(char*)NULL,(void*)NULL,0);
   // automatic destructor
   G__memfunc_setup("~Tcondition",1177,G__intdef_1_3_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic assignment operator
   G__memfunc_setup("operator=",937,G__intdef_1_4_0,(int)('u'),G__get_linked_tagnum(&G__LN_Tcondition),-1,1,1,1,1,0,"u 'Tcondition' - 11 - -",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}

static void G__setup_memfuncGabarites(void) {
   /* Gabarites */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__LN_Gabarites));
   G__memfunc_setup("Gabarites",914,G__intdef_6_0_0,105,G__get_linked_tagnum(&G__LN_Gabarites),-1,0,6,1,1,0,
"d - 'real' 0 - - d - 'real' 0 - - "
"d - 'real' 0 - - d - 'real' 0 - - "
"d - 'real' 0 - - d - 'real' 0 - -",(char*)NULL,(void*)NULL,0);
   // automatic copy constructor
   G__memfunc_setup("Gabarites",914,G__intdef_6_1_0,(int)('i'),G__get_linked_tagnum(&G__LN_Gabarites),-1,0,1,1,1,0,"u 'Gabarites' - 11 - -",(char*)NULL,(void*)NULL,0);
   // automatic destructor
   G__memfunc_setup("~Gabarites",1040,G__intdef_6_2_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic assignment operator
   G__memfunc_setup("operator=",937,G__intdef_6_3_0,(int)('u'),G__get_linked_tagnum(&G__LN_Gabarites),-1,1,1,1,1,0,"u 'Gabarites' - 11 - -",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}

static void G__setup_memfuncPlaneArray(void) {
   /* PlaneArray */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__LN_PlaneArray));
   G__memfunc_setup("PlaneArray",1007,G__intdef_8_0_0,105,G__get_linked_tagnum(&G__LN_PlaneArray),-1,0,3,1,1,0,
"u 'Direction' - 11 - - D - 'real' 12 - - "
"i - - 0 - -",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("operator[]",1060,G__intdef_8_1_0,117,G__get_linked_tagnum(&G__LN_Tplanesurf),-1,1,1,1,1,0,"i - - 0 - i",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("operator[]",1060,G__intdef_8_2_0,117,G__get_linked_tagnum(&G__LN_Tplanesurf),-1,1,1,1,1,9,"i - - 0 - i",(char*)NULL,(void*)NULL,0);
   // automatic copy constructor
   G__memfunc_setup("PlaneArray",1007,G__intdef_8_3_0,(int)('i'),G__get_linked_tagnum(&G__LN_PlaneArray),-1,0,1,1,1,0,"u 'PlaneArray' - 11 - -",(char*)NULL,(void*)NULL,0);
   // automatic destructor
   G__memfunc_setup("~PlaneArray",1133,G__intdef_8_4_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic assignment operator
   G__memfunc_setup("operator=",937,G__intdef_8_5_0,(int)('u'),G__get_linked_tagnum(&G__LN_PlaneArray),-1,1,1,1,1,0,"u 'PlaneArray' - 11 - -",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}

static void G__setup_memfuncPlane(void) {
   /* Plane */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__LN_Plane));
   G__memfunc_setup("Plane",496,G__intdef_9_0_0,105,G__get_linked_tagnum(&G__LN_Plane),-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("Plane",496,G__intdef_9_1_0,105,G__get_linked_tagnum(&G__LN_Plane),-1,0,2,1,1,0,
"u 'Direction' - 11 - - D - 'real' 10 - -",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("Plane",496,G__intdef_9_2_0,105,G__get_linked_tagnum(&G__LN_Plane),-1,0,3,1,1,0,
"D - 'real' 10 - - D - 'real' 10 - - "
"D - 'real' 10 - -",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("Plane",496,G__intdef_9_3_0,105,G__get_linked_tagnum(&G__LN_Plane),-1,0,1,1,1,0,"D - 'real' 12 - -",(char*)NULL,(void*)NULL,0);
   // automatic copy constructor
   G__memfunc_setup("Plane",496,G__intdef_9_4_0,(int)('i'),G__get_linked_tagnum(&G__LN_Plane),-1,0,1,1,1,0,"u 'Plane' - 11 - -",(char*)NULL,(void*)NULL,0);
   // automatic destructor
   G__memfunc_setup("~Plane",622,G__intdef_9_5_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic assignment operator
   G__memfunc_setup("operator=",937,G__intdef_9_6_0,(int)('u'),G__get_linked_tagnum(&G__LN_Plane),-1,1,1,1,1,0,"u 'Plane' - 11 - -",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}

static void G__setup_memfuncVolumeZone(void) {
   /* VolumeZone */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__LN_VolumeZone));
   G__memfunc_setup("VolumeZone",1044,G__intdef_13_0_0,105,G__get_linked_tagnum(&G__LN_VolumeZone),-1,0,2,1,1,0,
"u 'Tcondition' - 1 - - d - 'real' 0 - -",(char*)NULL,(void*)NULL,0);
   // automatic copy constructor
   G__memfunc_setup("VolumeZone",1044,G__intdef_13_1_0,(int)('i'),G__get_linked_tagnum(&G__LN_VolumeZone),-1,0,1,1,1,0,"u 'VolumeZone' - 11 - -",(char*)NULL,(void*)NULL,0);
   // automatic destructor
   G__memfunc_setup("~VolumeZone",1170,G__intdef_13_2_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic assignment operator
   G__memfunc_setup("operator=",937,G__intdef_13_3_0,(int)('u'),G__get_linked_tagnum(&G__LN_VolumeZone),-1,1,1,1,1,0,"u 'VolumeZone' - 11 - -",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}

static void G__setup_memfuncSurfaceZone(void) {
   /* SurfaceZone */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__LN_SurfaceZone));
   G__memfunc_setup("SurfaceZone",1125,G__intdef_14_0_0,105,G__get_linked_tagnum(&G__LN_SurfaceZone),-1,0,3,1,1,0,
"u 'VolumeZone' - 1 - - u 'Tplanesurf' - 1 - - "
"d - 'real' 0 - -",(char*)NULL,(void*)NULL,0);
   G__memfunc_setup("SurfaceZone",1125,G__intdef_14_1_0,105,G__get_linked_tagnum(&G__LN_SurfaceZone),-1,0,3,1,1,0,
"u 'VolumeZone' - 1 - - u 'Plane' - 1 - - "
"d - 'real' 0 - -",(char*)NULL,(void*)NULL,0);
   // automatic copy constructor
   G__memfunc_setup("SurfaceZone",1125,G__intdef_14_2_0,(int)('i'),G__get_linked_tagnum(&G__LN_SurfaceZone),-1,0,1,1,1,0,"u 'SurfaceZone' - 11 - -",(char*)NULL,(void*)NULL,0);
   // automatic destructor
   G__memfunc_setup("~SurfaceZone",1251,G__intdef_14_3_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic assignment operator
   G__memfunc_setup("operator=",937,G__intdef_14_4_0,(int)('u'),G__get_linked_tagnum(&G__LN_SurfaceZone),-1,1,1,1,1,0,"u 'SurfaceZone' - 11 - -",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}


/*********************************************************
* Member function information setup
*********************************************************/
extern "C" void G__cpp_setup_memfunc() {
}

/*********************************************************
* Global variable information setup for each class
*********************************************************/
static void G__cpp_setup_global0() {

   /* Setting up global variables */
   G__resetplocal();

   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"CONDITION_CINT_H=0",1,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"DIRECTION_H=0",1,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"GABARITES_H=0",1,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"PLANE_H=0",1,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"PLANE_ARRAY_H=0",1,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"TV3_CINT_H=0",1,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"ZONE_CINT_H=0",1,(char*)NULL);
   G__memvar_setup((void*)(&GRUBIK),117,0,0,G__get_linked_tagnum(&G__LN_rubik),-1,-1,1,"GRUBIK=",0,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"TRACKING_CINT_H=0",1,(char*)NULL);
   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"OARCH_CINT_H=0",1,(char*)NULL);

   G__resetglobalenv();
}
extern "C" void G__cpp_setup_global() {
  G__cpp_setup_global0();
}

/*********************************************************
* Global function information setup for each class
*********************************************************/
static void G__cpp_setup_func0() {
   G__lastifuncposition();

   G__memfunc_setup("operator*",918,G__intdef__0_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tcondition' - 1 - arg1 u 'Tcondition' - 1 - arg2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator+",919,G__intdef__1_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tcondition' - 1 - arg1 u 'Tcondition' - 1 - arg2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator-",921,G__intdef__2_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,1,1,1,0,"u 'Tcondition' - 1 - arg",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator-",921,G__intdef__3_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tcondition' - 1 - arg1 u 'Tcondition' - 1 - arg2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator*",918,G__intdef__4_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tplanesurf' - 1 - s1 u 'Tplanesurf' - 1 - s2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator*",918,G__intdef__5_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tplanesurf' - 1 - s1 u 'Tcondition' - 1 - c2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator*",918,G__intdef__6_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tcondition' - 1 - c2 u 'Tplanesurf' - 1 - s1",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator+",919,G__intdef__7_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tplanesurf' - 1 - s1 u 'Tplanesurf' - 1 - s2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator+",919,G__intdef__8_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tplanesurf' - 1 - s1 u 'Tcondition' - 1 - c2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator+",919,G__intdef__9_0,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tcondition' - 1 - c2 u 'Tplanesurf' - 1 - s1",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator-",921,G__intdef__0_1,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tplanesurf' - 1 - s1 u 'Tplanesurf' - 1 - s2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator-",921,G__intdef__1_1,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tplanesurf' - 1 - s1 u 'Tcondition' - 1 - c2",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator-",921,G__intdef__2_1,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,2,1,1,0,
"u 'Tcondition' - 1 - c2 u 'Tplanesurf' - 1 - s1",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("operator-",921,G__intdef__3_1,117,G__get_linked_tagnum(&G__LN_Tcondition),-1,1,1,1,1,0,"u 'Tplanesurf' - 1 - s",(char*)NULL
,(void*)NULL,0);
   G__memfunc_setup("SetGabarites",1214,G__intdef__4_1,121,-1,-1,0,6,1,1,0,
"d - 'real' 0 - - d - 'real' 0 - - "
"d - 'real' 0 - - d - 'real' 0 - - "
"d - 'real' 0 - - d - 'real' 0 - -",(char*)NULL
#ifndef SetGabarites
,(void*)(void (*)(real,real,real,real,real,real))SetGabarites,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("StartMC",670,G__intdef__5_1,121,-1,-1,0,2,1,1,0,
"i - - 0 - - i - - 0 - -",(char*)NULL
#ifndef StartMC
,(void*)(void (*)(int,int))StartMC,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("StartMC",670,G__intdef__6_1,121,-1,-1,0,1,1,1,0,"i - - 0 - -",(char*)NULL
#ifndef StartMC
,(void*)(void (*)(int))StartMC,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("PrintStat",937,G__intdef__7_1,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef PrintStat
,(void*)(void (*)(const char*))PrintStat,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("PrintStat",937,G__intdef__8_1,121,-1,-1,0,0,1,1,0,"",(char*)NULL
#ifndef PrintStat
,(void*)(void (*)())PrintStat,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("PrintCoef",906,G__intdef__9_1,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef PrintCoef
,(void*)(void (*)(const char*))PrintCoef,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("PrintCoef",906,G__intdef__0_2,121,-1,-1,0,0,1,1,0,"",(char*)NULL
#ifndef PrintCoef
,(void*)(void (*)())PrintCoef,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("SaveModel",896,G__intdef__1_2,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef SaveModel
,(void*)(void (*)(const char*))SaveModel,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("LoadModel",881,G__intdef__2_2,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef LoadModel
,(void*)(void (*)(const char*))LoadModel,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("MakeIDF",593,G__intdef__3_2,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef MakeIDF
,(void*)(void (*)(const char*))MakeIDF,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("MakeDDF",588,G__intdef__4_2,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef MakeDDF
,(void*)(void (*)(const char*))MakeDDF,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("MakeLinkSubr",1192,G__intdef__5_2,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef MakeLinkSubr
,(void*)(void (*)(const char*))MakeLinkSubr,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("CheckX0",614,G__intdef__6_2,121,-1,-1,0,1,1,1,0,"C - - 10 - -",(char*)NULL
#ifndef CheckX0
,(void*)(void (*)(const char*))CheckX0,0);
#else
,(void*)NULL,0);
#endif
   G__memfunc_setup("BuildNameMap",1167,G__intdef__7_2,121,-1,-1,0,0,1,1,0,"",(char*)NULL
#ifndef BuildNameMap
,(void*)(void (*)())BuildNameMap,0);
#else
,(void*)NULL,0);
#endif

   G__resetifuncposition();
}

extern "C" void G__cpp_setup_func() {
  G__cpp_setup_func0();
}

/*********************************************************
* Class,struct,union,enum tag information setup
*********************************************************/
/* Setup class/struct taginfo */
G__linked_taginfo G__LN_Direction = { "Direction" , 99 , -1 };
G__linked_taginfo G__LN_Tcondition = { "Tcondition" , 99 , -1 };
G__linked_taginfo G__LN_Tplanesurf = { "Tplanesurf" , 99 , -1 };
G__linked_taginfo G__LN_Tdomain_surf = { "Tdomain_surf" , 99 , -1 };
G__linked_taginfo G__LN_Touter_cube = { "Touter_cube" , 99 , -1 };
G__linked_taginfo G__LN_Tlim_domain = { "Tlim_domain" , 99 , -1 };
G__linked_taginfo G__LN_Gabarites = { "Gabarites" , 99 , -1 };
G__linked_taginfo G__LN_MatNMlEdoublecO3cO1gR = { "MatNM<double,3,1>" , 99 , -1 };
G__linked_taginfo G__LN_PlaneArray = { "PlaneArray" , 99 , -1 };
G__linked_taginfo G__LN_Plane = { "Plane" , 99 , -1 };
G__linked_taginfo G__LN_Treg_zone_vol = { "Treg_zone_vol" , 99 , -1 };
G__linked_taginfo G__LN_Treg_zone_surf = { "Treg_zone_surf" , 99 , -1 };
G__linked_taginfo G__LN_rubik = { "rubik" , 99 , -1 };
G__linked_taginfo G__LN_VolumeZone = { "VolumeZone" , 99 , -1 };
G__linked_taginfo G__LN_SurfaceZone = { "SurfaceZone" , 99 , -1 };

/* Reset class/struct taginfo */
extern "C" void G__cpp_reset_tagtable() {
  G__LN_Direction.tagnum = -1 ;
  G__LN_Tcondition.tagnum = -1 ;
  G__LN_Tplanesurf.tagnum = -1 ;
  G__LN_Tdomain_surf.tagnum = -1 ;
  G__LN_Touter_cube.tagnum = -1 ;
  G__LN_Tlim_domain.tagnum = -1 ;
  G__LN_Gabarites.tagnum = -1 ;
  G__LN_MatNMlEdoublecO3cO1gR.tagnum = -1 ;
  G__LN_PlaneArray.tagnum = -1 ;
  G__LN_Plane.tagnum = -1 ;
  G__LN_Treg_zone_vol.tagnum = -1 ;
  G__LN_Treg_zone_surf.tagnum = -1 ;
  G__LN_rubik.tagnum = -1 ;
  G__LN_VolumeZone.tagnum = -1 ;
  G__LN_SurfaceZone.tagnum = -1 ;
}


extern "C" void G__cpp_setup_tagtable() {

   /* Setting up class,struct,union tag entry */
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Direction),sizeof(Direction),-1,35072,(char*)NULL,G__setup_memvarDirection,G__setup_memfuncDirection);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Tcondition),sizeof(Tcondition),-1,0,(char*)NULL,G__setup_memvarTcondition,G__setup_memfuncTcondition);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Tplanesurf),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Tdomain_surf),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Touter_cube),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Tlim_domain),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Gabarites),sizeof(Gabarites),-1,32768,(char*)NULL,G__setup_memvarGabarites,G__setup_memfuncGabarites);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_MatNMlEdoublecO3cO1gR),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_PlaneArray),sizeof(PlaneArray),-1,32768,(char*)NULL,G__setup_memvarPlaneArray,G__setup_memfuncPlaneArray);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Plane),sizeof(Plane),-1,33024,(char*)NULL,G__setup_memvarPlane,G__setup_memfuncPlane);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Treg_zone_vol),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_Treg_zone_surf),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_rubik),0,-1,0,(char*)NULL,NULL,NULL);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_VolumeZone),sizeof(VolumeZone),-1,32768,(char*)NULL,G__setup_memvarVolumeZone,G__setup_memfuncVolumeZone);
   G__tagtable_setup(G__get_linked_tagnum(&G__LN_SurfaceZone),sizeof(SurfaceZone),-1,32768,(char*)NULL,G__setup_memvarSurfaceZone,G__setup_memfuncSurfaceZone);
}
extern "C" void G__cpp_setup(void) {
  G__check_setup_version(30051515,"G__cpp_setup()");
  G__set_cpp_environment();
  G__cpp_setup_tagtable();

  G__cpp_setup_inheritance();

  G__cpp_setup_typetable();

  G__cpp_setup_memvar();

  G__cpp_setup_memfunc();
  G__cpp_setup_global();
  G__cpp_setup_func();

   if(0==G__getsizep2memfunc()) G__get_sizep2memfunc();
  return;
}
