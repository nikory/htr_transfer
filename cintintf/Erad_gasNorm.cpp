/* Erad_gasNorm.f -- translated by f2//(version 20020208).
   You must link the resulting object file with the libraries:
-lf2//-lm   (in that order)
*/

#define _USE_MATH_DEFINES

#include <math.h>
#include "Erad_gasNorm.h"

/* ================================================= */
//������ ������� ���������� ��������� ��������� ��� �������� ����� ��� ����
double sigma_ads(double *p, double *t, double *rh2o, double *rco2, double *leff, double *eradcfcorr)
//double *p, *t, *rh2o, *rco2, *leff, *eradcfcorr;
{
    /* System generated locals */
    double ret_val;

    /* Local variables */
    static double kabs, pmpa, tkel, serv1, serv2, r3sum;


//������� ���������� ������������ ������� ������� ���� ����� �����
//P - ������ ��������, ��
//T - �����������, ����.�
//Rh2o - �������� ���� ������� ����� � ����� �����
//Rco2 - �������� ���� ����������� � ����� �����
//Leff - ������� ����������� ����, �
//Eradcfcorr - ����������� �����������
/* ----- */
    tkel = *t + 273.15;
    pmpa = *p * 1e-6;
    r3sum = *rh2o + *rco2 + 1e-10;
    serv1 = (*rco2 * 16. + 7.8) / sqrt(pmpa * 10. * r3sum * *leff) - 1.;
    serv2 = 1. - tkel * 3.7e-4;
    kabs = serv1 * serv2 * r3sum;
    ret_val = kabs * pmpa;
    return ret_val;
} /* erad_gasnorm__ */


/* ================================================= */
//������ ������� ���������� ��������� ��������� ��� ����� � ������ ����
double sigma_ads(double *p, double *t, double *rh2o, double *rco2, double *leff, double *eradcfcorr,
                 int *radindsa, double *parsoot, double *dashes, double *rash)
{
    /* System generated locals */
    static double c_b2 = .3333;
    double ret_val;

    /* Local variables */
    static double kabs, pmpa, tkel, serv1, serv2, r3sum , kash, kr, ks, ss;
    double d__1;


//������� ���������� ������������ ������� ������� ���� ����� �����
//P - ������ ��������, ��
//T - �����������, ����.�
//Rh2o - �������� ���� ������� ����� � ����� �����
//Rco2 - �������� ���� ����������� � ����� �����
//Leff - ������� ����������� ����, �
//Eradcfcorr - ����������� �����������
/* ----- */
    tkel = *t + 273.15;
    pmpa = *p * 1e-6;
    r3sum = *rh2o + *rco2 + 1e-10;
    serv1 = (*rco2 * 16. + 7.8) / sqrt(pmpa * 10. * r3sum * *leff) - 1.;
    serv2 = 1. - tkel * 3.7e-4;
    kr = serv1 * serv2 * r3sum;
    if (*radindsa == 1) {

	if (tkel > 312.6) {
	    serv1 = tkel * .0016 - .5;
	} else {
	    serv1 = 0.;
	}
	ks = *parsoot * serv1;

	d__1 = tkel * *dashes;
	ss = d__1 * d__1 * 3.1416;
	ss = pow(ss,c_b2);
	kash = *rash * 5590. / ss * (float)10.19;
    } else {
	ks = 0.;
	kash = 0.;
    }
    kabs = kr + ks + kash;
    ret_val = kabs * pmpa;
    return ret_val;
} /* erad_gasnorm__ */
