#ifndef PLANE_H
#define PLANE_H

#ifndef __MAKECINT__
#include "../surface.h"
#endif

#include "plane_array.h"
#include "direction.h"
#include "condition_cint.h"

class Plane: public Tplanesurf
{
#ifndef __MAKECINT__
 friend class boost::serialization::access;
 template<class Arch>
 inline void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("Plane",boost::serialization::base_object<Tplanesurf>(*this));
 }
 bool x;
 Direction dir;
 bool init(const real*,const real*,const real*);
#endif
public:
 Plane();
 Plane(const Direction&, const real*);
 Plane(const real*,const real*,const real*);
 Plane(const real*[3]);
// operator Tcondition&();
};

#ifndef __MAKECINT__

TV3 MakeTV3(const real* v)
{
 TV3 res;
 memcpy(res[0],v,3*sizeof(real));
 return res;
}

Plane::Plane(const Direction &d, const real *v):
 Tplanesurf(MakeTV3(v),d)
 {
 }
Plane::Plane():Tplanesurf(){}
bool Plane::init(const real* p1,const real* p2,const real* p3)
{
 real x1=p1[0], y1=p1[1], z1=p1[2];
 real x2=p2[0], y2=p2[1], z2=p2[2];
 real x3=p3[0], y3=p3[1], z3=p3[2];
 dir(0)=y3* (-z1 + z2) + y2* (z1 - z3) + y1* (-z2 + z3);
 dir(1)=x3* (z1 - z2) + x1*(z2 - z3) + x2* (-z1 + z3);
 dir(2)=x3* (-y1 + y2) + x2* (y1 - y3) + x1* (-y2 + y3);
 real n=sqrt(dot(dir,dir));
 if(n<1e-4)
 {
  std::cout<<"Possible Error: All three points lays close to one line\n";
 }
 dir/=n;
 return true;
}
Plane::Plane(const real* p1,const real* p2,const real* p3):Tplanesurf()
{
 init(p1,p2,p3);
 (static_cast<Tplanesurf&>(*this))=plane(dir,MakeTV3(p1));
}
Plane::Plane(const real** p)
{
 init(p[0],p[1],p[2]);
 (static_cast<Tplanesurf&>(*this))=plane(dir,MakeTV3(p[0]));
}
/*Plane::operator Tcondition &()
{
 return Tplanesurf::operator Tcondition &();
} */            

#endif

#endif
