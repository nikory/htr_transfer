#ifndef DIRECTION_H
#define DIRECTION_H

#ifndef __MAKECINT__
#include "../tv3.h"
#endif

#ifdef __MAKECINT__
typedef double real;
#endif

#ifndef __MAKECINT__
class Direction: public TV3
#else
class Direction
#endif
{
public:
 Direction();
 Direction(real,real,real);
 Direction& operator=(const real*);
};

#ifndef __MAKECINT__

inline Direction::Direction():TV3(){}

inline Direction::Direction(real x, real y, real z)
{
 data[0]=x;
 data[1]=y;
 data[2]=z;
}

inline Direction& Direction::operator =(const real * el)
{
 *(TV3*)this=el;
 real nrm=norm(*(TV3*)this);
 (*this)/=nrm;
 return *this;
}

#endif

#endif