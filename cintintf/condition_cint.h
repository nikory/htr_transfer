#ifndef CONDITION_CINT_H
#define CONDITION_CINT_H

#ifndef __MAKECINT__
#include "../condition.h"
/*#include "plane.h"

inline Tcondition& operator*(Plane& s1, Plane& s2){return (Tplanesurf&)s1*(Tplanesurf&);}
inline Tcondition& operator*(Plane& s1, Tcondition& c2){return (Tplanesurf&)s1*c2;}
inline Tcondition& operator*(Tcondition& c2, Plane& s1){return c2*(Tplanesurf&)s1;}
inline Tcondition& operator+(Plane& s1, Plane& s2){return (Tplanesurf&)s1+(Tplanesurf&)s2;}
inline Tcondition& operator+(Plane& s1, Tcondition& c2){return (Tplanesurf&)s1+c2;}
inline Tcondition& operator+(Tcondition& c2, Plane& s1){return c2+(Tplanesurf&)s1;}
inline Tcondition& operator-(Plane& s1, Plane& s2){return (Tplanesurf&)s1-(Tplanesurf&)s2;}
inline Tcondition& operator-(Plane& s1, Tcondition& c2){return (Tplanesurf&)s1-c2;}
inline Tcondition& operator-(Tcondition& c2, Plane& s1){return c2-(Tplanesurf&)s1;}
inline Tcondition& operator-(Plane& s){return -(Tplanesurf&)s;}*/
#else
#include "direction.h"
//#include "plane.h"
class Tcondition
{
public:
 bool operator()(const real*);
};
class Tplanesurf;
Tcondition& operator*(Tcondition& arg1,Tcondition& arg2);
Tcondition& operator+(Tcondition& arg1,Tcondition& arg2);
Tcondition& operator-(Tcondition& arg);
Tcondition& operator-(Tcondition& arg1,Tcondition& arg2);
inline Tcondition& operator*(Tplanesurf& s1, Tplanesurf& s2);
inline Tcondition& operator*(Tplanesurf& s1, Tcondition& c2);
inline Tcondition& operator*(Tcondition& c2, Tplanesurf& s1);
inline Tcondition& operator+(Tplanesurf& s1, Tplanesurf& s2);
inline Tcondition& operator+(Tplanesurf& s1, Tcondition& c2);
inline Tcondition& operator+(Tcondition& c2, Tplanesurf& s1);
inline Tcondition& operator-(Tplanesurf& s1, Tplanesurf& s2);
inline Tcondition& operator-(Tplanesurf& s1, Tcondition& c2);
inline Tcondition& operator-(Tcondition& c2, Tplanesurf& s1);
inline Tcondition& operator-(Tplanesurf& s);

/*inline Tcondition& operator*(Plane& s1, Plane& s2);
inline Tcondition& operator*(Plane& s1, Tcondition& c2);
inline Tcondition& operator*(Tcondition& c2, Plane& s1);
inline Tcondition& operator+(Plane& s1, Plane& s2);
inline Tcondition& operator+(Plane& s1, Tcondition& c2);
inline Tcondition& operator+(Tcondition& c2, Plane& s1);
inline Tcondition& operator-(Plane& s1, Plane& s2);
inline Tcondition& operator-(Plane& s1, Tcondition& c2);
inline Tcondition& operator-(Tcondition& c2, Plane& s1);
inline Tcondition& operator-(Plane& s);*/
#endif

#endif