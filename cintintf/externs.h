#ifndef EXTERNS_H
#define EXTERNS_H

#include <map>
#include <vector>
#include <set>
#include <string>
#include "../condition.h"
#include "../domain.h"
#include "../rubik.h"
#include "../ray_tracker.h"
#include "../Tradiation_coefs.h"   
#include "../transf_vector.h"  
#include "giwlink.h"


extern DECL_FORMULASET;
extern std::vector<Tlim_domain*> ZONE_DOMAIN;
extern Touter_cube SGAB;
extern rubik GRUBIK;
extern Tray_tracker GTRACKER;
extern Tradiation_coefs GRCOEFS;   

extern std::vector<Tnode_link> GNODE_LINKS;
extern std::vector<Twall_link> GWALL_LINKS;
extern std::map<std::string,Treg_zone*> GNAME_ZONE_MAP;


extern transf_vector<Treg_zone_surf> GOUTPUT_QS_VEC;
extern transf_vector<Treg_zone_vol> GOUTPUT_QV_VEC;
extern transf_vector<Treg_zone_vol> GOUTPUT_KV_VEC;

extern TQ_color_scale QSURF_SCALE;
extern TQ_color_scale QVOL_SCALE;

#endif