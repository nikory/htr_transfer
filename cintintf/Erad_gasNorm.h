#ifndef Erad_gasNorm_H
#define Erad_gasNorm_H

double sigma_ads(double *p, double *t, double *rh2o, double *rco2, double *leff, double *eradcfcorr);
double sigma_ads(double *p, double *t, double *rh2o, double *rco2, double *leff, double *eradcfcorr,
                 int *radindsa, double *parsoot, double *dashes, double *rash);

#endif