#include <iostream>
#include <exception>
#include <set>
#include <map>
#include <vector>
#include <string>
#include "export_funcs.h"
#include "externs.h"
#include "../Treg_zone.h"
#include "giwlink.h"

int RTRANSF_COUNTER;
double* BLACKNESS_GL;
double* SIGMA_ADS_GL;
int* EPS_SEL_FLAG;
const int EPS_SEL_GLOB=1;


extern void LoadModel(const char*);

void cook_adsorbtion_graph();

extern "C" void __stdcall LOAD_MODEL(char* fnm,int n)
{
 try
 {
  std::cout<<"��������� ������ ����� �� �����: "<<fnm<<endl;
  LoadModel(fnm);
  GRUBIK.index_zones();
  GRUBIK.set2vec();
  GTRACKER.X0();
  std::cout<<"��������� ������ ����� �� ����� "<<fnm<<std::endl;
  GOUTPUT_QS_VEC.set_index(GRUBIK.zone_index_surf);
//  std::cout<<"������ �����: ������ �������� ������"<<std::endl;
  cook_adsorbtion_graph();
  std::cout<<"��������� ���������� ���"<<std::endl;
  RTRANSF_COUNTER=0;
 }
 catch(std::exception& e)
 {
  std::cout<<"������ �������� ������ �����: "<<e.what()<<std::endl;
 }
 GOUTPUT_QS_VEC.set_index(GRUBIK.zone_index_surf);
 GOUTPUT_QV_VEC.set_index(GRUBIK.zone_index_vol);
 GOUTPUT_KV_VEC.set_index(GRUBIK.zone_index_vol);
 GRCOEFS.matr_V.set_index(GRUBIK.zone_index_vol);
 GTRACKER.correct_vols();
 GRCOEFS.alloc_matrices(GTRACKER);
}

extern "C" void __stdcall MAKE_NAME_MAP()
{
 using namespace std;
 set<Treg_zone*>::iterator i,b,e;
 b=GRUBIK.rz_set.begin();
 e=GRUBIK.rz_set.end();
 for(i=b;i!=e;i++)
 {
  GNAME_ZONE_MAP[(**i).name]=*i;
 } 
}

extern "C" void __stdcall LINK_TEMPER(char* nm,int,real* T)
{
/* std::string name(nm);
 real* tar;
 Treg_zone* fi;
 fi=GNAME_ZONE_MAP[name];                                                                                  
 switch(fi->my_type())
 {
  case(RZ_SURF):
   tar=&(((Treg_zone_surf*)fi)->props().TMPR);
   break;
  case(RZ_VOL):
   tar=&(((Treg_zone_vol*)fi)->props().TMPR);
   break;
 }
 Tlink_interface<real,real> res;
 res.first=T;
 res.second=tar;
 GZONE_TEMPER_LINK.push_back(res);   */
}

extern "C" void __stdcall LINK_NODE(char*name,int,double*t,double*p,double*vh2o,double*vco2,double*rDash,double*dDash,double*Sa,double*rc,double*drc,double*Qva,double*Qva_cap,double*Qvmax,double*Qvmin,int*dashFlag,int*color)
{
 Tnode_link res;
 Treg_zone_vol* v=static_cast<Treg_zone_vol*>(GNAME_ZONE_MAP[std::string(name)]);
 res.linked_zone=v;
 res.node_temper=t;
 res.node_press=p;
 res.node_VCO2=vco2;
 res.node_VH2O=vh2o;
 res.node_Rash=rDash;
 res.node_Dash=dDash;
 res.node_Sads=Sa;
 res.node_VolRadCor=rc;
 res.node_ParSoot=drc;
 res.node_Qadv=Qva;
 res.node_Qadv_cap=Qva_cap;
 res.Qv_max=Qvmax;
 res.Qv_min=Qvmin;
 res.node_DashFlag=dashFlag;
 res.Qv_color=color;
 GNODE_LINKS.push_back(res);
}

extern "C" void __stdcall LINK_WALL(char*name,int,double*t,double*e,double*q,double*q_cap,double*Qsmax,double*Qsmin,int*color)
{
 Twall_link res;
 Treg_zone_surf* s=static_cast<Treg_zone_surf*>(GNAME_ZONE_MAP[std::string(name)]);
 res.linked_zone=s;
 res.wall_temper=t;
 res.wall_epsilon=e;
 res.wall_Qads=q;
 res.wall_Qads_cap=q_cap;
 res.Qs_max=Qsmax;
 res.Qs_min=Qsmin;
 res.Qs_color=color;
 GWALL_LINKS.push_back(res);
}

extern "C" void __stdcall GET_QWALL_SCALE_HANDLE(int* handle)
{
 *handle=(int)(&QSURF_SCALE);
}

extern "C" void __stdcall GET_QNODE_SCALE_HANDLE(int* handle)
{
 *handle=(int)(&QVOL_SCALE);
}

extern "C" void __stdcall SCALE_PUSH(int* handle, int* color)
{
 TQ_color_scale* sc=(TQ_color_scale*)(*handle);
 sc->push_color_handle(color);
}

extern "C" void __stdcall UPDATE_SCALES()
{
 QSURF_SCALE.update();
 QVOL_SCALE.update();
}

extern "C" void __stdcall RESET_SCALES()
{
 QSURF_SCALE.clear();
 QVOL_SCALE.clear();
}

void cook_adsorbtion()
{
 int is,ns;
 int jz,nz;
 real x0,Qj,Aj,ej;
 Treg_zone_surf *si;
 Treg_zone_surf *zj;
 Tadsorbtion_graph *ga;
 ns=GRUBIK.rzs_vec.size();
 nz=GRUBIK.rzs_vec.size();
/* omp_set_dynamic(0);
 omp_set_num_threads(4);
#pragma omp parallel for private(is,si,Qj,x0,ga) shared(ns,nz)*/
 for(is=0;is<ns;is++)
 {
  si=GRUBIK.rzs_vec[is];
  for(jz=0;jz<nz;jz++)
  {
   zj=GRUBIK.rzs_vec[jz];
   if(si==zj) continue;
   ga=GTRACKER.get_adsorbtion_graph(zj,si);
   if(!check_valid(ga))
   {
    continue;
   }
   x0=GTRACKER.X0(zj,si);
/*   Qj=zj->props().Qsb;
   Aj=zj->get_vol_corr();
   Qj=x0*Aj*Qj;*/
   Qj=zj->props().Qsb*x0*zj->get_vol_corr();
   ga->cook(x0,Qj);
  }
 }
}

void cook_adsorbtion_graph()
{
 int is,ns;
 int jz,nz;
 real x0;
 Treg_zone_surf *si;
 Treg_zone_surf *zj;
 Tadsorbtion_graph *ga;
 ns=GRUBIK.rzs_vec.size();
 nz=GRUBIK.rzs_vec.size();
 for(is=0;is<ns;is++)
 {
  si=GRUBIK.rzs_vec[is];
  for(jz=0;jz<nz;jz++)
  {
   zj=GRUBIK.rzs_vec[jz];
   ga=GTRACKER.get_adsorbtion_graph(zj,si);
   if(!check_valid(ga))
   {
    continue;
   }
   ga->cook_bfs_order();
  }
 }
}

void update_zones()
{
 using namespace std;
 vector<Tnode_link>::iterator ni,nb,ne;
 vector<Twall_link>::iterator wi,wb,we;
 nb=GNODE_LINKS.begin();
 ne=GNODE_LINKS.end();
 wb=GWALL_LINKS.begin();
 we=GWALL_LINKS.end();
// GRCOEFS.volume_rad(GTRACKER);
 for(ni=nb;ni!=ne;ni++)
 {
  ni->node_AdsCor=*SIGMA_ADS_GL* *ni->node_VolRadCor;
  ni->update_zone();
 }
 for(wi=wb;wi!=we;wi++)
 {
  if(*EPS_SEL_FLAG==EPS_SEL_GLOB)
  {
   wi->zone_epsilon=*BLACKNESS_GL;
  }
  else
  {
   wi->zone_epsilon=*(wi->wall_epsilon);
  }
  if(wi->zone_epsilon<1e-10)
  {
   wi->zone_epsilon=1e-10;
  }
  if(wi->zone_epsilon>1.)
  {
   wi->zone_epsilon=1.;
  }
  wi->update_zone();
 }
}

void vol_erad()//������ ������� ������������ ��������� � �������� ����� (�� ��� �������)
{
 int is,ns;
 int jz,nz;
 Treg_zone_surf *si;
 Treg_zone_vol *zj;
 Tadsorbtion_graph *ga;
 real Tj,st4,Qvj,x0,x1,A_i,dQ,kads;
 Tvol_adsorbtion_info *info_j;
 ns=GRUBIK.rzs_vec.size();
 nz=GRUBIK.rzv_vec.size();
 for(jz=0;jz<nz;jz++)
 {
  zj=GRUBIK.rzv_vec[jz];
  Tj=zj->props().TMPR;
  st4=sq4(Tj)*StephanBoltsmanSigma;
  Qvj=0;
  for(is=0;is<ns;is++)
  {
   si=GRUBIK.rzs_vec[is];
   ga=GTRACKER.get_adsorbtion_graph(zj,si); 
   if(!check_valid(ga))
   {
    continue;
   }
   info_j=ga->get_adsorbtion_info(zj);
   kads=info_j->kads;
   x0=GTRACKER.X0(si,zj);
   x1=GTRACKER.X0(zj,si);
   A_i=si->get_vol_corr();
//   dQ=A_i*x0*kads;
   dQ=GRCOEFS.vol_area(si,zj);
   Qvj+=dQ;
  }
  Qvj*=st4*zj->props().volradcor;
  zj->props().Qv=Qvj/zj->get_vol_corr();
 }
}

void adsorb_in_vol()//������ ������� ���������� � �������� �����
{
 int is,js,ns;
 int jv,nv,iv,nvk;
 real Qsj,Qresj,Jsj,Jsji,eps_j,alp_j,x0ij,x0ji,pads,A_i,A_j,Qvj,Tj4,x0;
 Treg_zone_surf *si,*sj;
 Treg_zone_vol *vi,*vj;
 Tadsorbtion_graph *ga;
 Tvol_adsorbtion_info *info_i;
 ns=GRUBIK.rzs_vec.size();
 nv=GRUBIK.rzv_vec.size();
 GOUTPUT_QV_VEC.v()=0.;
 for(is=0;is<ns;is++) //������� �������� ��������� ��������� - ������������� ����
 {
  si=GRUBIK.rzs_vec[is];
  A_i=si->get_vol_corr();
  for(js=0;js<ns;js++)
  {
   sj=GRUBIK.rzs_vec[js];
   ga=GTRACKER.get_adsorbtion_graph(sj,si);
   if(!check_valid(ga))
   {
    continue;
   }
   A_j=sj->get_vol_corr();
   Qresj=GOUTPUT_QS_VEC(sj);
   Qsj=sj->props().Qs;
   eps_j=sj->props().epsilon;
   alp_j=sj->props().alpha;
   Qresj=(Qsj+alp_j*Qresj)*A_j/eps_j;
   x0=GTRACKER.X0(sj,si);
   ga->cook(x0,x0*Qresj);
   nv=ga->size();
   for(iv=0;iv<nv;iv++)
   {
    info_i=ga->get_adsorbtion_info(iv);
    vi=info_i->vol_zone;
    GOUTPUT_QV_VEC(vi)+=info_i->Qvol_res;
   }
  }
 }
 nv=GOUTPUT_QV_VEC.size();
 for(iv=0;iv<nv;iv++)
 {
  vi=GOUTPUT_QV_VEC(iv);
  GOUTPUT_QV_VEC[iv]/=vi->get_vol_corr();
 }
}

/*
void build_SA_system()
{
 int iv,jv,kv,nkv,nv,js,ns;
 real sumlsa,lsa,e1,e2,vij_el,x0,vol_i,vol_j,vol_ji;
 Treg_zone_surf *sj;
 Treg_zone_vol *vi,*vj,*vk;
 Tray_adsorbtion *ra;
 nv=GRUBIK.rzv_set.size();
 ns=GRUBIK.rzs_vec.size();
 for(iv=0;iv<nv;iv++)
 {
  vi=GRUBIK.rzv_vec[iv];
  vol_i=vi->get_vol_corr();
  GRCOEFS.map_V(vi)=0.;
  for(js=0;js<ns;js++)
  {
   sj=GRUBIK.rzs_vec[js];
   ra=GTRACKER.get_ray_adsorbtion(sj,vi);
   if(ra!=0)
   {
    x0=GTRACKER.X0(sj,vi);
    vol_j=sj->get_vol_corr();
    vol_ji=vol_j/vol_i;
    nkv=ra->size();
    lsa=ra->lsa(vi);
    e2=exp(-lsa);
    sumlsa=0;
    for(kv=0;kv<nkv-1;kv++)
    {
     vk=(*ra)[kv];
     sumlsa+=ra->lsa(vk);
    }
    e1=exp(-sumlsa);
    GRCOEFS.map_V(vi)+=e1*(1.-e2)*x0*vol_ji;
   }
  }
  for(jv=0;jv<nv;jv++)
  {
   vj=GRUBIK.rzv_vec[jv];
   if(vi==vj)
   {
    GRCOEFS.matr_V(vi,vj)=1.;
    break;
   }
   ra=GTRACKER.get_ray_adsorbtion(vj,vi);
   if(ra!=0)
   {
    x0=GTRACKER.X0(vj,vi);
    vol_j=vj->get_vol_corr();
    vol_ji=vol_j/vol_i;
    nkv=ra->size();
    lsa=ra->lsa(vi);
    e2=exp(-lsa);
    sumlsa=0;
    for(kv=0;kv<nkv-1;kv++)
    {
     vk=(*ra)[kv];
     sumlsa+=ra->lsa(vk);
    }
    e1=exp(-sumlsa);
    GRCOEFS.matr_V(vi,vj)=-e1*(1.-e2)*x0*vol_ji;
   }
  }
 }
}

*/

/*���������� ���������� ������� ������������ ��������� � ������������� ����*/
void put_Qads()
{
 int i,n;
 real *qmax,*qmin,q;
 real Qres,Qads,Qself,alpha,eps;
 Treg_zone_surf* sz;
 Treg_zone_vol* vz;
 n=GOUTPUT_QS_VEC.size();
 for(i=0;i<n;i++)
 {
  sz=GOUTPUT_QS_VEC(i);
  Qres=GOUTPUT_QS_VEC[i];
//  Qres/=sz->get_vol();
  alpha=sz->props().alpha;
  if(alpha<1e-20)alpha=1e-20;
  eps=sz->props().epsilon;
  Qself=sz->props().Qs;
  Qads=eps/alpha*(Qres-Qself);
  sz->props().Qeff=Qres;//Qads-Qself;
 }
 n=GOUTPUT_QV_VEC.size();
 for(i=0;i<n;i++)
 {
  vz=GOUTPUT_QV_VEC(i);
  Qads=GOUTPUT_QV_VEC[i];
//  Qself=vz->props().Qv;
//  vz->props().Qadv=Qads-Qself;
  vz->props().Qadv=Qads;
 }
 n=GWALL_LINKS.size();
 qmin=GWALL_LINKS[0].Qs_min;
 qmax=GWALL_LINKS[0].Qs_max;
 *qmax=*GWALL_LINKS[0].wall_Qads_cap;
 *qmin=*GWALL_LINKS[0].wall_Qads_cap;
 for(i=0;i<n;i++)
 {
  GWALL_LINKS[i].qminmax();
 }
 for(i=0;i<n;i++)
 {
  GWALL_LINKS[i].update_wall();
 }
 n=GNODE_LINKS.size();
 qmin=GNODE_LINKS[0].Qv_min;
 qmax=GNODE_LINKS[0].Qv_max;
 *qmax=*GNODE_LINKS[0].node_Qadv_cap;
 *qmin=*GNODE_LINKS[0].node_Qadv_cap;
 for(i=0;i<n;i++)
 {
  GNODE_LINKS[i].qminmax();
 }
 for(i=0;i<n;i++)
 {
//  q=*(GNODE_LINKS[i].node_Qadv_cap);
  GNODE_LINKS[i].update_node();
 }
}

extern "C" void __stdcall RTRANSF()
{
// UPDATE_SCALES();
 update_zones();
 cook_adsorbtion();
 GRCOEFS.build_matrices(GTRACKER);
 GRCOEFS.build_freecf(GTRACKER);
 GRCOEFS.factor_system();
 GRCOEFS.solve_system(GOUTPUT_QS_VEC);
 adsorb_in_vol();
 put_Qads();
}   
extern "C" void __stdcall BLACKNESS_GLOB(double* x, int* y)
{
 BLACKNESS_GL=x;
 EPS_SEL_FLAG=y;
}
extern "C" void __stdcall SIGMA_ADS_GLOB(double* x)
{
 SIGMA_ADS_GL=x;
}

