#include <map>
#include <vector>
#include <set>
#include <string>
#include "../condition.h"
#include "../domain.h"
#include "../rubik.h"
#include "../ray_tracker.h"
#include "../Tradiation_coefs.h"  
#include "giwlink.h"   


DECL_FORMULASET;
std::vector<Tlim_domain*> ZONE_DOMAIN;
Touter_cube SGAB; 
rubik GRUBIK;
Tray_tracker GTRACKER;
Tradiation_coefs GRCOEFS;  

std::vector<Tnode_link> GNODE_LINKS;
std::vector<Twall_link> GWALL_LINKS;
std::map<std::string,Treg_zone*> GNAME_ZONE_MAP;

transf_vector<Treg_zone_surf> GOUTPUT_QS_VEC;
transf_vector<Treg_zone_vol> GOUTPUT_QV_VEC;
transf_vector<Treg_zone_vol> GOUTPUT_KV_VEC;

TQ_color_scale QSURF_SCALE(static_cast<Tpal*>(&pal_cupper));
TQ_color_scale QVOL_SCALE(static_cast<Tpal*>(&pal_cupper));
