#ifndef GABARITES_H
#define GABARITES_H

#ifndef __MAKECINT__
#include "../domain.h"
#endif

#ifdef __MAKECINT__
class Tdomain_surf;
class Touter_cube;
class Tlim_domain;
#endif

class Gabarites: public Touter_cube
{
public:
 Gabarites(real,real,real,real,real,real);
};

void SetGabarites(real,real,real,real,real,real);

#ifndef __MAKECINT__

Gabarites::Gabarites(real xc, real yc, real zc, real dx, real dy, real dz)
{
 TV3 r0={xc,yc,zc};
 TV3 dr={dx,dy,dz};
 init(dr,r0);
}

extern Touter_cube SGAB;

void SetGabarites(real xc, real yc, real zc, real dx, real dy, real dz)
{
 TV3 r0={xc,yc,zc};
 TV3 dr={dx,dy,dz};
 SGAB.init(dr,r0);
}

#endif

#endif