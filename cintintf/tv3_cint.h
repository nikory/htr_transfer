#ifndef TV3_CINT_H
#define TV3_CINT_H

#ifndef __MAKECINT__
#include <include/tmatr.h>
#endif

template<class T,int N, int M> class MatNM;

typedef MatNM<double,3,1> TV3;

#endif