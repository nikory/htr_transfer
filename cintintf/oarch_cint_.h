#ifndef OARCH_CINT_H
#define OARCH_CINT_H

#ifdef __MAKECINT__
void SaveModel(const char*);
inline void LoadModel(const char*);
#endif

#ifndef __MAKECINT__

#include "../serialize_inc.h"
#include "../model.h"

inline void SaveModel(const char* filename)
{
#ifdef BIN_IO
 std::ofstream ostr(filename,ios::binary);
#else
 std::ofstream ostr(filename);
#endif
 OARC oa(ostr);
 topka_model tsave;
 oa<<tsave;
}

inline void LoadModel(const char* filename)
{
#ifdef BIN_IO
 std::ifstream istr(filename,ios::binary);
#else
 std::ifstream istr(filename);
#endif
 IARC ia(istr);
 topka_model tload;
 ia>>tload;
}

#endif

#endif