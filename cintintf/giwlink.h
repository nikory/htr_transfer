#ifndef GIWLINK_H
#define GIWLINK_H

#include <utility>
#include <vector>
#include "../tv3.h"
#include "../Treg_zone.h"
#include "../phys_const.h"

template<class T1, class T2>
class Tlink_interface: public std::pair<T1*,T2*>
{
public:
 inline void send12(){*second=*first;} 
 inline void send21(){*first=*second;}
};

#include "Erad_gasNorm.h"
#include "palette.h"

/*!����� ��� ����� ���������� ������� ������� (����) � ��������� ������*/
class Tnode_link
{
public:
 Treg_zone_vol * linked_zone; //! ��������� � ����� ����
 double * node_temper; //! ����������� � ����, C
 double * node_press; //! �������� � ����, ��
 double * node_VH2O; //! �������� ������������ ����� ���� � ����
 double * node_VCO2; //! �������� ������������ ����������� ����
 double * node_Rash; //! �������� ������������ ������� ������ � ����
 double * node_Dash; //! ����������� ������� ������� ������
 double * node_ParSoot; //! �������� ������������ ������ ����
 int * node_DashFlag; //! ���� ����� ���� � ���� ��� ������� ����������
 double * node_VolRadCor; //! ��������� ������� ������� ����
 double node_AdsCor; //! ��������� ������� ������� ����
 double * node_Qadv; //! ������������ �����, ����������� � ������ (��� �������� � ���)
 double * node_Qadv_cap; //! ��������� �����, ������������ � ������
 double * node_Sads; //! ������� ���������� ����� � ����
 int * Qv_color;
 double * Qv_max;
 double * Qv_min;
 inline real estimate_sa()
 {
  double Leff=1;
  double Eradcorr=1;
  return sigma_ads(node_press,node_temper,node_VH2O,node_VCO2,&Leff,&Eradcorr,node_DashFlag,node_ParSoot,node_Dash,node_Rash);
 }
 inline void update_zone()
 {
  Tzone_props_vol* pv=&(linked_zone->props());
//  real sa=node_AdsCor*estimate_sa();
  real sa=estimate_sa();
//  real sa=log(node_AdsCor)+estimate_sa();
  pv->sigma_a=sa;
  pv->volradcor=node_AdsCor;
  pv->TMPR=Celsium2Kelvin(*node_temper);
  pv->find_q();
 }
 inline void qminmax()
 {
  if(*node_Qadv_cap<*Qv_min)*Qv_min=*node_Qadv_cap;
  if(*node_Qadv_cap>*Qv_max)*Qv_max=*node_Qadv_cap;
 }
 inline void update_node()
 {
  *node_Sads=linked_zone->props().sigma_a;
  *node_Qadv_cap=linked_zone->props().Qadv;
  *node_Qadv=(*node_Qadv_cap)*(linked_zone->get_vol_corr());
  if(*node_Qadv_cap<*Qv_min)*Qv_min=*node_Qadv_cap;
  if(*node_Qadv_cap>*Qv_max)*Qv_max=*node_Qadv_cap;
  real dQ=(*Qv_max-*Qv_min);
  if(abs(dQ)<1e-5)dQ=1e-5;
//  double color=(*node_Qadv_cap-*Qv_min)/dQ;
  double color=(*Qv_max-*node_Qadv_cap)/dQ;
  if(color<0)color=0;
  else if(color>1)color=1;
  *Qv_color=pal_cupper(color);
 }
};

/*!����� ��� ����� ���������� ������� ������� (����) � �������������� ������*/
class Twall_link
{
public:
 Treg_zone_surf * linked_zone; //! ��������� ����
 double * wall_temper; //! ����������� ����������� (�� �����)
 double * wall_epsilon; //! ������� ������� �����������
 double zone_epsilon;
 double * wall_Qads; //! �������� �����, ����������� ������������ (��� �������� � ���)
 double * wall_Qads_cap; //! ��������� �����, ������������ � ������
 int * Qs_color;
 double * Qs_max;
 double * Qs_min;
 inline void update_zone()
 {
  Tzone_props_surf* ps=&(linked_zone->props());
  ps->TMPR=Celsium2Kelvin(*wall_temper);
  ps->epsilon=zone_epsilon;
  ps->alpha=1.-zone_epsilon;
  ps->find_q();
 }
 inline void qminmax()
 {
  if(*wall_Qads_cap<*Qs_min)*Qs_min=*wall_Qads_cap;
  if(*wall_Qads_cap>*Qs_max)*Qs_max=*wall_Qads_cap;
 }
 inline void update_wall()
 {
  *wall_Qads_cap=linked_zone->props().Qeff;
  *wall_Qads=(*wall_Qads_cap)*(linked_zone->get_vol_corr());
  real dQ=(*Qs_max-*Qs_min);
  if(abs(dQ<1e-5))dQ=1e-5;
  double color=(*wall_Qads_cap-*Qs_min)/dQ;
//  double color=(*Qs_max-*wall_Qads_cap)/dQ;
  *Qs_color=pal_cupper(color);
/*  if(*Qs_color==0)
  {
   std::cout<<linked_zone->name<<std::endl;
   std::cout<<*wall_Qads_cap<<std::endl;
   std::cout<<*Qs_min<<std::endl;
   std::cout<<*Qs_max<<std::endl;
  }*/
 }
};

/*!�������� �������� ����� �����������*/
class TQ_color_scale
{
 Tpal* myPal;
 double* myQmin;
 double* myQmax;
 std::vector<int*> myColorHandles;
 int **pColors;
public:
 TQ_color_scale(Tpal* pal);
 void push_color_handle(int* color);
 void update();
 void clear();
};

void SendVarsFromGiw();

#endif