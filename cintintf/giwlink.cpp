#include "giwlink.h"
#include "externs.h"

void SendVarsFromGiw()
{
 int n,i;
 real *qmax,*qmin,q;
 n=GNODE_LINKS.size();
 qmin=GNODE_LINKS[i].Qv_min;
 qmax=GNODE_LINKS[i].Qv_max;
 for(i=0;i<n;i++)
 {
  GNODE_LINKS[i].update_zone();
  q=*(GNODE_LINKS[i].node_Qadv_cap);
  if(q>*qmax)
  {
   *qmax=q;
  }
  if(q<*qmin)
  {
   *qmin=q;
  }
 }
 n=GWALL_LINKS.size();
 qmin=GWALL_LINKS[0].Qs_min;
 qmax=GWALL_LINKS[0].Qs_max;
 for(i=0;i<n;i++)
 {
  GWALL_LINKS[i].update_zone();
  q=*(GWALL_LINKS[i].wall_Qads_cap);
  if(q>*qmax)
  {
   *qmax=q;
  }
  if(q<*qmin)
  {
   *qmin=q;
  }
 }
}

TQ_color_scale::TQ_color_scale(Tpal* pal):myPal(pal){}

void TQ_color_scale::push_color_handle(int *color_handle)
{
 myColorHandles.push_back(color_handle);
 pColors=&myColorHandles[0];
}

void TQ_color_scale::clear()
{
 myColorHandles.clear();
}

void TQ_color_scale::update()
{
 int n=myColorHandles.size();
 int i;
 for(i=0;i<n;i++)
 {
  (*pColors[i])=(*myPal)(real(i)/(n-1));
 }
}