#ifndef OARCH_CINT_H
#define OARCH_CINT_H

#ifdef __MAKECINT__
void SaveModel(const char*);
void LoadModel(const char*);
void MakeIDF(const char*);
void MakeDDF(const char*);
void MakeLinkSubr(const char*);
void CheckX0(const char*);
void BuildNameMap();
#endif

#ifndef __MAKECINT__

#include "../serialize_inc.h"
#include <fstream>
#include <set>
#include <string>
#include "externs.h"
#include "zone_cint.h"
#include <exception>

void BuildNameMap();


inline void SaveModel(const char* filename)
{
 try
 {
#ifdef BIN_IO
 std::ofstream ostr;
 try{
 ostr.exceptions(0xffffff);
 ostr.open(filename,ios::binary);
 }
 catch(...)
 {
  std::cout<<"catched when file opens\n";
 }
#else
 std::ofstream ostr(filename);
#endif
 BuildNameMap();
 OARC oa(ostr);
 oa.template register_type<Tlim_domain>();
 oa.template register_type<Treg_zone_surf>();
 oa.template register_type<Treg_zone_vol>();
 oa.template register_type<VolumeZone>();
 oa.template register_type<SurfaceZone>();
 oa.template register_type<Plane>();
 oa<<ARC_ADAPTOR(SGAB);
 oa<<ARC_ADAPTOR(FORMULASET);
 oa<<ARC_ADAPTOR(ZONE_DOMAIN);
 oa<<ARC_ADAPTOR(GRUBIK);
 oa<<ARC_ADAPTOR(GTRACKER);
 oa<<ARC_ADAPTOR(GRCOEFS);
 }
 catch(std::exception& e)
 {
  std::cout<<e.what();
 }
 catch(...)
 {
  std::cout<<"Catched in dll"<<endl;
 }
}

inline void LoadModel(const char* filename)
{
 extern void cook_adsorbtion_graph();
 std::cout<<"��������� �����"<<endl;
 std::ifstream istr;
 try
 {
#ifdef BIN_IO
 istr.open(filename,ios::binary);
#else
 std::ifstream istr(filename);
#endif
 }
 catch(std::exception& e)
 {
  std::cout<<e.what()<<endl;
 }
 catch(...)
 {
  std::cout<<"�� ������� ������� �����"<<endl;
 }
 IARC ia(istr);
 ia.template register_type<Tlim_domain>();
 ia.template register_type<Treg_zone_surf>();
 ia.template register_type<Treg_zone_vol>();
 ia.template register_type<VolumeZone>();
 ia.template register_type<SurfaceZone>();
 ia.template register_type<Plane>();
 std::cout<<"�������� ���������"<<endl;
 ia>>ARC_ADAPTOR(SGAB);
 std::cout<<"�������� �������� �������"<<endl;
 ia>>ARC_ADAPTOR(FORMULASET);
 std::cout<<"�������� ZONE_DOMAIN"<<endl;
 ia>>ARC_ADAPTOR(ZONE_DOMAIN);
 std::cout<<"�������� ���"<<endl;
 ia>>ARC_ADAPTOR(GRUBIK);
 std::cout<<"�������� ����������� �����-�����"<<endl;
 ia>>ARC_ADAPTOR(GTRACKER);
 std::cout<<"�������� ������������� ��������"<<endl;
 ia>>ARC_ADAPTOR(GRCOEFS);
 std::cout<<"��� ���������"<<endl;
 GRUBIK.index_zones();
 GTRACKER.prepare_ads();
 GTRACKER.X0();
}


#include <string>
#include <G__ci.h>
inline void BuildNameMap()
{
 G__var_array *va;
 va=G__varglobal();
 while(va!=NULL)
 {
  int ni=va->allvar,i;
  Treg_zone *obj;
  rubik::rz_set_t::iterator fi;
  std::string obj_name;
  for(i=0;i<ni;i++)
  {
   obj=static_cast<Treg_zone*>((void*)(va->p[i]));
   obj_name=va->varnamebuf[i];
   fi=GRUBIK.rz_set.find(obj);
   if(fi!=GRUBIK.rz_set.end())
   {
    obj->name=obj_name;
   }
  }
  va=va->next;
 }
 return;
}

inline void MakeIDF(const char* idfname)
{
 using namespace std;
 ofstream idf(idfname);
 set<Treg_zone_vol*>::iterator vol,volb,vole;
 set<Treg_zone_surf*>::iterator sur,surb,sure;
 string objnm;
 volb=GRUBIK.rzv_set.begin();
 vole=GRUBIK.rzv_set.end();
 surb=GRUBIK.rzs_set.begin();
 sure=GRUBIK.rzs_set.end();
 idf<<"* Topka_Rad_Look 0 0"<<endl;
 idf<<"* TOPKA_PROCESS 1 0"<<endl<<endl;
 idf<<"V TOPKA_RAD_BLGLOB Re8"<<endl;
 idf<<"I TOPKA_RAD_BLGLOB [XX] \'��������� ��������� ������� ������� ������\'"<<endl;
 idf<<"V TOPKA_RAD_BLGLOB_FLAG In4"<<endl;
 idf<<"I TOPKA_RAD_BLGLOB_FLAG [XX] \'���� ��������� ��������� ��������� ������� ������� ������: 1 - On, 0 - Off\'"<<endl;
 idf<<"V TOPKA_RAD_SAGLOB Re8"<<endl;
 idf<<"I TOPKA_RAD_SAGLOB [XX] \'��������� ��������� ������������ ���������� � ����\'"<<endl;
 idf<<"V TOPKA_RAD_QV_MAX Re8"<<endl;
 idf<<"I TOPKA_RAD_QV_MAX [XX] \'������������ �������� ���������������� � ������ (��� �����������)\'"<<endl;
 idf<<"V TOPKA_RAD_QV_MIN Re8"<<endl;
 idf<<"I TOPKA_RAD_QV_MIN [XX] \'����������� �������� ���������������� � ������ (��� �����������)\'"<<endl;
 idf<<"V TOPKA_RAD_QS_MAX Re8"<<endl;
 idf<<"I TOPKA_RAD_QS_MAX [XX] \'������������ �������� ���������������� � ������ (��� �����������)\'"<<endl;
 idf<<"V TOPKA_RAD_QS_MIN Re8"<<endl;
 idf<<"I TOPKA_RAD_QS_MIN [XX] \'����������� �������� ���������������� � ������ (��� �����������)\'"<<endl;
 idf<<"V TOPKA_RAD_DASH_FLAG In4"<<endl;
 idf<<"I TOPKA_RAD_DASH_FLAG [XX] \'���� ����� ���� ��� ������� ���������� � �����\'"<<endl;
 idf<<"; ������ ������� ���������� ��� �������� ���"<<endl;
 for(vol=volb;vol!=vole;vol++)
 {
  objnm=(**vol).name;
  idf<<"; ���������� ��� �������� ���� "<<objnm<<endl;
  idf<<"; ����� ���� "<<(**vol).get_vol()<<endl;
  idf<<"V "<<objnm<<"_Temp Re8"<<endl;
  idf<<"I "<<objnm<<"_T [C] \'����������� � ����"<<objnm<<" [����.�]\'"<<endl;
  idf<<"V "<<objnm<<"_Pr Re8"<<endl;
  idf<<"I "<<objnm<<"_Pr [C] \'�������� � ����"<<objnm<<" [����.�]\'"<<endl;
  idf<<"V "<<objnm<<"_SigmaAds Re8"<<endl;
  idf<<"O "<<objnm<<"_SigmaAds [1/�] \'����������� ���������� ��������� ��������� � �������� ����\'"<<endl;
  idf<<"V "<<objnm<<"_Qva Re8"<<endl;
  idf<<"O "<<objnm<<"_Q [��] \'������������ �������� ���������, ����������� � ���� "<<objnm<<"\'"<<endl;
  idf<<"V "<<objnm<<"_Qvacap Re8"<<endl;
  idf<<"O "<<objnm<<"_Qcap [��/�3] \'��������  �������� ���������, ����������� � ���� "<<objnm<<"\'"<<endl;
  idf<<"V "<<objnm<<"_Qvacol In4"<<endl;
  idf<<"O "<<objnm<<"_Qcol [XX] \'���� ��� ����������� �������� �������� ����������� � ���� "<<objnm<<"\'"<<endl;
  idf<<"V "<<objnm<<"_VH2O Re8"<<endl;
  idf<<"I "<<objnm<<"_VH2O \'�������� ���� ����� ���� � ���� "<<objnm<<"\'"<<endl;
  idf<<"V "<<objnm<<"_VCO2 Re8"<<endl;
  idf<<"I "<<objnm<<"_VCO2 \'�������� ���� ����������� ���� � ���� "<<objnm<<"\'"<<endl<<endl;
  idf<<"V "<<objnm<<"_RDash Re8"<<endl;
  idf<<"I "<<objnm<<"_RDash \'�������� ���� �������� ������ � ����"<<objnm<<"\'"<<endl<<endl;
  idf<<"V "<<objnm<<"_DDash Re8"<<endl;
  idf<<"I "<<objnm<<"_DDash \'����������� ������ �������� ������ � ����"<<objnm<<"\'"<<endl<<endl;
  idf<<"V "<<objnm<<"_VolRadCor Re8"<<endl;
  idf<<"I "<<objnm<<"_VolRadCor \'����������� ��������� ������� ������� �������� ���� "<<objnm<<"\'"<<endl<<endl;
  idf<<"V "<<objnm<<"_DashRadCor Re8"<<endl;
  idf<<"I "<<objnm<<"_DashRadCor \'����������� ��������� ���������� � �������� �������� "<<objnm<<"\'"<<endl<<endl;
 }
 idf<<"; ����� ������� ���������� ��� �������� ���"<<endl;
 idf<<endl;
 idf<<"; ������ ������� ���������� ��� ������������� ���"<<endl;
 for(sur=surb;sur!=sure;sur++)
 {
  objnm=(**sur).name;
  idf<<"; ���������� ��� ������������� ���� "<<objnm<<endl;
  idf<<"; ����� ���� "<<(**sur).get_vol()<<endl;
  idf<<"V "<<objnm<<"_Temp Re8"<<endl;
  idf<<"I "<<objnm<<"_T [�] \'����������� ������\'"<<endl;
  idf<<"V "<<objnm<<"_Blackness Re8"<<endl;
  idf<<"O "<<objnm<<"_Blackness [] \'������� �������\'"<<endl;
  idf<<"V "<<objnm<<"_Qsa Re8"<<endl;
  idf<<"O "<<objnm<<"_Q [��] \'������������ �������� ���������, ����������� � ������ "<<objnm<<"\'"<<endl<<endl;
  idf<<"V "<<objnm<<"_Qsacap Re8"<<endl;
  idf<<"O "<<objnm<<"_Qcap [��/�2] \'��������  �������� ���������, ����������� � ������ "<<objnm<<"\'"<<endl<<endl;
  idf<<"V "<<objnm<<"_Qsacol In4"<<endl;
  idf<<"O "<<objnm<<"_Qcol [XX] \'���� ��� ����������� �������� �������� ����������� � ������ "<<objnm<<"\'"<<endl;
 }
}

inline void MakeDDF(const char* ddfname)
{
 using namespace std;
 ofstream ddf(ddfname);
 set<Treg_zone_surf*>::iterator sur,surb,sure;
 set<Treg_zone_vol*>::iterator vol,volb,vole;
 string objnm;
 surb=GRUBIK.rzs_set.begin();
 sure=GRUBIK.rzs_set.end();
 volb=GRUBIK.rzv_set.begin();
 vole=GRUBIK.rzv_set.end();
 ddf<<"      data TOPKA_RAD_BLGLOB /"<<0.8<<"/"<<endl;
 ddf<<"      data TOPKA_RAD_SAGLOB /"<<1.0<<"/"<<endl;
 for(sur=surb;sur!=sure;sur++)
 {
  objnm=(**sur).name;
  ddf<<"c ���������� ��� ������������� ���� "<<objnm<<endl;
  ddf<<"c ����� ���� "<<(**sur).get_vol()<<endl;
  ddf<<"      data "<<objnm<<"_Blackness /"<<1.0<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_Temp /"<<300<<"/"<<endl;
 }
 for(vol=volb;vol!=vole;vol++)
 {
  objnm=(**vol).name;
  ddf<<"c ���������� ��� �������� ���� "<<objnm<<endl;
  ddf<<"c ����� ���� "<<(**vol).get_vol()<<endl;
  ddf<<"      data "<<objnm<<"_Temp /"<<300<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_Pr /"<<300000<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_VH2O /"<<0.1<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_VCO2 /"<<0.1<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_RDash /"<<0.1<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_DDash /"<<25.0<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_VolRadCor /"<<1.14<<"/"<<endl;
  ddf<<"      data "<<objnm<<"_DashRadCor /"<<1.<<"/"<<endl;
 }
}

inline void MakeLinkSubr(const char* fn)
{
 using namespace std;
 ofstream sub(fn);
 set<Treg_zone_vol*>::iterator vi,vb,ve;
 set<Treg_zone_surf*>::iterator si,sb,se;
 string objnm;
 vb=GRUBIK.rzv_set.begin();
 ve=GRUBIK.rzv_set.end();
 sb=GRUBIK.rzs_set.begin();
 se=GRUBIK.rzs_set.end();
 sub<<"      subroutine TOPKA_LINK"<<endl;
 sub<<"       implicit none"<<endl;
 sub<<"       include \'../fh/topka_funcs.fh\'"<<endl;
 sub<<"       include \'../fh/topka_rad.fh\'"<<endl;
 sub<<"c ����������� ����� ���"<<endl;
 sub<<"       call TOPKA_MAKE_NAME_MAP"<<endl<<endl;
 sub<<endl<<"c ������ ����� � ������"<<endl;
 for(vi=vb;vi!=ve;vi++)
 {
  objnm=(**vi).name;
  sub<<"       call TOPKA_LINK_NODE(\'"<<objnm<<"\'c,"<<endl
     <<"     & "<<objnm<<"_Temp,"<<endl
     <<"     & "<<objnm<<"_Pr,"<<endl
     <<"     & "<<objnm<<"_VH2O,"<<endl
     <<"     & "<<objnm<<"_VCO2,"<<endl
     <<"     & "<<objnm<<"_RDash,"<<endl
     <<"     & "<<objnm<<"_DDash,"<<endl
     <<"     & "<<objnm<<"_SigmaAds,"<<endl
     <<"     & "<<objnm<<"_VolRadCor,"<<endl
     <<"     & "<<objnm<<"_DashRadCor,"<<endl
     <<"     & "<<objnm<<"_Qva,"<<endl
     <<"     & "<<objnm<<"_Qvacap,"<<endl
     <<"     & TOPKA_RAD_QV_MAX,"<<endl
     <<"     & TOPKA_RAD_QV_MIN,"<<endl
     <<"     & TOPKA_RAD_DASH_FLAG,"<<endl
     <<"     & "<<objnm<<"_Qvacol)"<<endl;
 }
 sub<<endl<<"c ������ ����� �� ��������"<<endl;
 for(si=sb;si!=se;si++)
 {
  objnm=(**si).name;
  sub<<"       call TOPKA_LINK_WALL(\'"<<objnm<<"\'c,"<<endl
     <<"     & "<<objnm<<"_Temp,"<<endl
     <<"     & "<<objnm<<"_Blackness,"<<endl
     <<"     & "<<objnm<<"_Qsa,"<<endl
     <<"     & "<<objnm<<"_Qsacap,"<<endl
     <<"     & TOPKA_RAD_QS_MAX,"<<endl
     <<"     & TOPKA_RAD_QS_MIN,"<<endl
     <<"     & "<<objnm<<"_Qsacol)"<<endl;
 }
 sub<<"      end subroutine"<<endl;
}

inline void CheckX0(const char* fn)
{
 GTRACKER.correct_vols();
 GRCOEFS.check_x0(fn,GTRACKER);
}

#endif

#endif