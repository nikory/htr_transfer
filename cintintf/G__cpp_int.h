/********************************************************************
* G__cpp_int.h
********************************************************************/
#ifdef __CINT__
#error G__cpp_int.h/C is only for compilation. Abort cint.
#endif
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define G__ANSIHEADER
#define G__DICTIONARY
#include <G__ci.h>
extern "C" {
extern void G__cpp_setup_tagtable();
extern void G__cpp_setup_inheritance();
extern void G__cpp_setup_typetable();
extern void G__cpp_setup_memvar();
extern void G__cpp_setup_global();
extern void G__cpp_setup_memfunc();
extern void G__cpp_setup_func();
extern void G__set_cpp_environment();
}


#include "condition_cint.h"
#include "direction.h"
#include "gabarites.h"
#include "plane.h"
#include "plane_array.h"
#include "tv3_cint.h"
#include "zone_cint.h"
#include "tracking_cint.h"
#include "oarch_cint.h"

#ifndef G__MEMFUNCBODY
#endif

extern G__linked_taginfo G__LN_Direction;
extern G__linked_taginfo G__LN_Tcondition;
extern G__linked_taginfo G__LN_Tplanesurf;
extern G__linked_taginfo G__LN_Tdomain_surf;
extern G__linked_taginfo G__LN_Touter_cube;
extern G__linked_taginfo G__LN_Tlim_domain;
extern G__linked_taginfo G__LN_Gabarites;
extern G__linked_taginfo G__LN_MatNMlEdoublecO3cO1gR;
extern G__linked_taginfo G__LN_PlaneArray;
extern G__linked_taginfo G__LN_Plane;
extern G__linked_taginfo G__LN_Treg_zone_vol;
extern G__linked_taginfo G__LN_Treg_zone_surf;
extern G__linked_taginfo G__LN_rubik;
extern G__linked_taginfo G__LN_VolumeZone;
extern G__linked_taginfo G__LN_SurfaceZone;

/* STUB derived class for protected member access */
typedef MatNM<double,3,1> G__MatNMlEdoublecO3cO1gR;
