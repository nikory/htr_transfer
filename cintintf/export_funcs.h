#ifndef _EXPORT_FUNCS
#define _EXPORT_FUNCS

extern "C" void __stdcall LOAD_MODEL(char*,int);
extern "C" void __stdcall MAKE_NAME_MAP();
extern "C" void __stdcall LINK_TEMPER(char*,int,double*);
extern "C" void __stdcall LINK_NODE(char*,int,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*,double*,int*,int*);
extern "C" void __stdcall LINK_WALL(char*,int,double*,double*,double*,double*,double*,double*,int*);
extern "C" void __stdcall BLACKNESS_GLOB(double*,int*);

#endif