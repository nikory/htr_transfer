/*������ ������� ������������� ��������� ������ 
����� �������������� � ��������� ���������� �� �������
����, ��������� �� 27 �������� � 54 ������������� ���.
�������� ��������: RUBIK*/

#include <iostream>
#include "rubik.h"
#include "ray_tracker.h"
#include "Tradiation_coefs.h"

DECL_FORMULASET;

int main(int argc, char** argv)
{
 hqrndrandomize(STATIC_RND_STATE);
 int i=atoi(argv[1]);
 ssReal_map_t r;
 rubik examp;
 Tray_tracker tracker;
 Tradiation_coefs rcf;
 tracker.set_rubik(examp);
 tracker.generate_link_mtr(i);
 tracker.update_vols();
 std::cout<<"S="<<examp.region_surface()<<std::endl;
 std::cout<<"V="<<examp.region_volume()<<std::endl;
 rcf.build_matrices(tracker);
 rcf.build_freecf(tracker);
 rcf.ptX(std::cout);
 std::cout<<std::endl;
/* rcf.ptR(std::cout);
 std::cout<<std::endl;
 rcf.ptP(std::cout);
 std::cout<<std::endl;
 rcf.ptA(std::cout);
 std::cout<<std::endl;
 rcf.ptF(std::cout);
 std::cout<<std::endl;
 rcf.ptL(std::cout);
 std::cout<<std::endl;
 rcf.ptC(std::cout);
 std::cout<<std::endl;
 rcf.factor_system();
 rcf.solve_system(r);
 std::cout<<r<<std::endl;
 rcf.account_ads(tracker);
 rcf.ptX(std::cout);
 std::cout<<std::endl;
 rcf.ptS(std::cout);
 return 0;     */
}         

/*void main()
{
 TV3 dr = {2, 2, 2};
 TV3 r0 = {0, 0, 0};
 Touter_cube room(dr,r0);
 TV3 x1 = -ORTS[0];
 TV3 x2 = ORTS[0];
 TV3 y1 = { 0,-1, 0};
 TV3 y2 = { 0, 1, 0};
 TV3 z1 = { 0, 0,-1};
 TV3 z2 = { 0, 0, 1};
 Tplanesurf xy1(z1,ORTS[2]), 
            xy2(z2,ORTS[2]), 
            xz1(y1,ORTS[1]), 
            xz2(y2,ORTS[1]), 
            yz1(x1,ORTS[0]), 
            yz2(x2,ORTS[0]);
 Tlim_domain cube(room,(xy1-xy2)*(xz1-xz2)*(yz1-yz2));
 Treg_zone_vol rz(cube,8);
 Treg_zone_surf rs(cube,yz2,4);
 Tline l=line2p(dr,x2);
 bool i=rz(l);
 Treg_info ri=rz.reg_info(l);
 Treg_info rj=rs.reg_info(l);
}      */

/*void main()
{
 using namespace std; 
 TV3 dr = {0.5, 0.5, 0.5};
 TV3 r0 = {0, 0, 0};
 Touter_cube room(dr,OOO);
 TV3 x1 = -ORTS[0];
 TV3 x2 = ORTS[0];
 TV3 y1 = { 0,-1, 0};
 TV3 y2 = { 0, 1, 0};
 TV3 z1 = { 0, 0,-1};
 TV3 z2 = { 0, 0, 1};
 Tplanesurf xy1(z1,ORTS[2]), 
            xy2(z2,ORTS[2]), 
            xz1(y1,ORTS[1]), 
            xz2(y2,ORTS[1]), 
            yz1(x1,ORTS[0]), 
            yz2(x2,ORTS[0]);
 Tlim_domain cube(room,(xy1-xy2)*(xz1-xz2)*(yz1-yz2));
 rubik r;
 Tline l=line2p(dr,x2);
 for(int i=0;i<100;i++)
 cout<<cube.lints(room.random_ray_from_bound().second);
}  */