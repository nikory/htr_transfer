#ifndef Tzone_tracking_h
#define Tzone_tracking_h

#include <algorithm>
#include <list>
#include "line.h"
#include "Treg_zone.h"

class Tlink_coef
{
public:
 const Treg_zone *sink,*target;
 real value;
};

/*!
����� ������������ ������ � ���������. ������� - ��� ������������ 
���� � ������������ �� ��������������� ��� � ����������� � �����������. 
���� ������������� � ������� ���������� � ����������� ����.
*/
class Tzone_tracker
{
public:
 typedef std::vector<Treg_info> info_con_t; //!< ��� ��������� �����
protected:
 info_con_t info;
 Tline my_ray;
 real init_weight;
 bool finished;
public:
//! �������� ���������
 inline void flush()
 {
  info.clear();
  info.reserve(10);
  finished=false;
 }
 Tzone_tracker(){flush();}
 Tzone_tracker(const Tline& l, real genw=1.):
  my_ray(l),
  init_weight(genw)
 {
  flush();
 }
//! ���������� � �������� � ������ ��������� ����������� � ����� 
 inline bool process(Treg_zone& rz)
 {
  if(finished)
  {
   return false;
  }
  else
  {
   static Tline const *lll=&my_ray;
   Treg_info ri;
   ri=rz.reg_info(my_ray);
   if(ri.reg) 
   {
    info.push_back(ri);
   }
   return true;
  }
 }
//! ��������� ������� ������ ����� ���
 inline void finish_process()
 {
  std::sort(info.begin(),info.end());
  finished=true;
 }
//! �������� �� ������ ���������� � �����������
 inline info_con_t::const_iterator begin()const{return info.begin();}
 inline info_con_t::const_reverse_iterator rbegin()const{return info.rbegin();}
 inline info_con_t::iterator begin(){return info.begin();}
//! �������� �� ����� ����������
 inline info_con_t::const_iterator end()const{return info.end();}
 inline info_con_t::iterator end(){return info.end();}
 inline Tlink_coef weight(const info_con_t::const_iterator& sink, const info_con_t::const_iterator& target)const
 {
  Tlink_coef res;
  info_con_t::const_iterator next;
  int inc;
  real w,x1,x2;
  if(finished)
  {
   res.sink=sink->rz;
   res.target=target->rz;
   info_con_t::const_iterator it;
   inc=1;
   x1=sink->x_line;
   x2=target->x_line;
   if(x1>x2)
   {
    inc=-1;
   }
   else 
   {
    if(x1==x2)
    {
     if(sink->rz!=target->rz)
     {
      inc=0;
      std::cout<<"bla"<<std::endl;
     }
     else
     {
      inc=0;
     }
    }
   }
   w=abs(init_weight*sink->sink_factor);
   next=sink+inc;
   if(next==target && sink->rz->my_type()==RZ_SURF && target->rz->my_type()==RZ_SURF)
   {
    w=0;
   }
   else
   {
    if(inc!=0)
    {
     for(it=sink;(it!=target);it+=inc)
     {
      if(it!=sink)     
      {
       w*=(it->weight_factor);
      }
     }
    }                 
   }
   res.value=fabs(w);
   return res;
  }
  else
  {
   res.sink=0;
   res.target=0;
   res.value=0;
   return res;
  }
 }
 inline int size()const
 {
  return info.size();
 }
};



#endif