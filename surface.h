#ifndef SURFACE_H
#define SURFACE_H

#include <vector>
#define _USE_MATH_DEFINES
#include <string>
#include <math.h>
#include <include/sqr.h>
#include "tv3.h"
#include "line.h"
#include "decart.h"

#include "serialize_inc.h"

/*!
����� ��� �������� ���������� � ����� ����������� ����� � �����������
*/
class Tinters_info
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch & a, int version)
 {
  a & ARC_ADAPTOR(point);
  a & ARC_ADAPTOR(nlprod);
 }
public:
 real point;  /// ���������� ����� ����������� �� �����
 real nlprod; /// ��������� ������������ ����������� (�����������) ����� � ������� � ����������� � ����� �����������
};


#define INTSIGN(x) (x)<0?-1:(x)>0?1:0


/*!������� ����� ��� �������� ������������. 
����: ����������� -- ��� ������ � ������������, ������� ����� ������������ �� 2 �����*/
class Tcondition;
class Tsurf
{
public:
  typedef std::vector<real> iptcon_t;
  typedef std::vector<Tinters_info> iiptcon_t;
protected:
  mutable iptcon_t* vpoint;
  mutable iiptcon_t* vipoint;
public:
  Tsurf():vpoint(0),vipoint(0),local_crd(){}
  Decart_system local_crd;
  virtual int side(const TV3& p)const=0;//!���������� +1 (������� �������), -1(���������� �������), 0 (�� �����������) (���������� ����� ������������)
  ///������� ���������� ������ �� ���������� ������ ����� ����������� ������ � �����������
  virtual iptcon_t& ipoint(const Tline& l)const{static iptcon_t v; v.resize(0); return v;}
  virtual iiptcon_t& ipoint_info(const Tline& l)const{static iiptcon_t v; v.resize(0); return v;}
  virtual void update(){}
  virtual TV3 normv(const TV3& pt)const{throw "Tsurf::norm not implemented"; TV3 x; return x;}
  operator Tcondition&();
  virtual operator  iptcon_t&(){return *vpoint;}
  virtual operator  const iptcon_t&()const{return *vpoint;}
  virtual operator iiptcon_t&(){return *vipoint;}
  virtual operator const iiptcon_t&()const{return *vipoint;}
  virtual void flush()
  {
   vpoint=0;
   vipoint=0;
  }
 mutable int last_line_id;
private:
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch & a, int version)
 {
//  a & vpoint;
//  a & vipoint;
  a & ARC_ADAPTOR(local_crd);
 }
};

//BOOST_SERIALIZATION_ASSUME_ABSTRACT(Tsurf)

/*!���������. � ��������� ����������� ��������� � ���������� xy, ������� ������������� ����� ��� z*/
class Tplanesurf: public Tsurf
{
protected:
 mutable std::vector<real> result0;
 mutable std::vector<real> result1;
 mutable Tsurf::iiptcon_t iresult1;
 static TV3 cosv;
 TV3 norm;
public:
 Tplanesurf():Tsurf(),result0(0),result1(1),iresult1(1){iresult1.reserve(1);}
 Tplanesurf(const Tline& normal):Tsurf(),result0(0),result1(1),iresult1(1)
 {
  iresult1.reserve(1);
  local_crd.move_to(normal.p0);
  local_crd.rotate_y_to(normal.Omega(1));
  local_crd.rotate_z_to(-normal.Omega(0));
  local_crd.update();
  update();
 }
 Tplanesurf(const TV3& x0, const TV3& dir):Tsurf(),result0(0),result1(1),iresult1(1)
 {
  iresult1.reserve(1);
  Tline n;
  n.p0=x0;
  n.direct=dir;
  n.update_ang();
  local_crd.move_to(n.p0);
  local_crd.rotate_y_to(n.Omega(1));
  local_crd.rotate_z_to(-n.Omega(0));
  local_crd.update();
  update();
 }
 inline int side(const TV3& x)const
 {
     /*
     static TV3 x_loc;
     static real z;
     x_loc=local_crd.convert_to(x);
     z=x_loc(2);
     return INTSIGN(z);
     */
     TV3 dx=x-local_crd.inip();
     real c=dot(dx,norm);
//     return INTSIGN(c);
     return c<0.?-1:1;
 }
 inline std::vector<real>& ipoint(const Tline& l)const
 {
/*  if(last_line!=&l)
  {
   last_line=&l;*/
   static TV3 norm1;
   real s1,s2;
   norm1=norm;
   s1=dot(norm1,local_crd.inip()-l.p0);
   s2=dot(l.direct,norm1);
   if(fabs(s2)<1e-20)
   {
    vpoint=&result0;
    return result0;
   }
   else
   {
    s1/=s2;
    result1[0]=s1;
    vpoint=&result1;
    return result1;
   }
/*  }
  else
  {
   return *vpoint;
  }  */
 }
 inline Tsurf::iiptcon_t& ipoint_info(const Tline& l)const
 {
//  if(&l!=last_line)
  {
   const Tsurf::iptcon_t* pt;
   static real x,nl;
   static const Tsurf const* This;
   This=this;
   pt=&(this->ipoint(l));
   if(pt->size()>0)
   {
    iresult1.resize(1);
    x=(*pt)[0];
    iresult1[0].point=x;
    nl=dot(norm,l.direct);
    iresult1[0].nlprod=nl;
   }
   else
   {
    iresult1.resize(0);
   }
   vipoint=&iresult1;
  }
  return iresult1;
 }
 inline void update()
 {
//  TV3 x=cosv+local_crd.inip();
  norm=local_crd.convert_from(cosv)-local_crd.inip();
 }
 TV3 normv(const TV3& pt)const{return norm;}
private:
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch & a, int version)
 {
  a & ARC_ADAPTOR_STR("Tsurf",boost::serialization::base_object<Tsurf>(*this));
  a & ARC_ADAPTOR(norm);
 }
};



inline Tplanesurf plane(const TV3& dir, real x)
{
 Tline l;
 l.direct=dir;
 l.p0=x*dir;
 l.update_ang();
 return Tplanesurf(l);
}
inline Tplanesurf plane(const TV3& dir, const TV3& x)
{
 Tline l;
 l.direct=dir;
 l.p0=x;
 l.update_ang();
 return Tplanesurf(l);
}
/*!����� � �������� ������� � ��������. ������ �������� ��������� � ������ �����, ��� �� ���������.*/
class Tsphere: public Tsurf
{
 real R,R2;
public:
 Tsphere(){}
 Tsphere(real r, const TV3& center)
 {
  local_crd.move_to(center);
  local_crd.update();
  R=r;
  R2=r*r;
 }
 inline int side(TV3 const&x) const
 {
  static int result;
  static TV3 x_loc;
  static real d;
  static real dr;
  x_loc = local_crd.convert_to(x);
  x_loc -= local_crd.inip();
  d = dot(x_loc, x_loc);
  dr = d - R2;
  result=INTSIGN(dr);
  return result;
 }
 inline std::vector<real>& ipoint(const Tline& l)const
 {
  static std::vector<real> res0(0);
  static std::vector<real> res1(1);
  static std::vector<real> res2(2);
  static TV3 pr;
  static TV3 n;
  static real a;
  static real b;
  static real c;
  static real d;
  static real t1;
  static real t2;
  pr=l.p0-local_crd.inip();
  n=l.direct;
  a=dot(n,n);
  b=dot(n,pr)*2.;
  c=dot(pr,pr)-R2;
  d=b*b-4.*a*c;
  if(d<0)
  {
   return res0;
  }
  else if(d>0)
  {
   d=sqrt(d);
   a=0.5/a;
   t1=(-b+d)*a;
   t2=-(b+d)*a;
   res2[0]=t1;
   res2[1]=t2;
   return res2;
  }
  else
  {
   t1=-b*0.5/a;
   res1[0]=t1;
   return res1;
  }
 }
};

/*!�������������� ����������� � �������� ����. � ��������� ����������� ��� ��������� � ���� z, ������ ������� ��������� � ������� ������ ���*/
class Tcylsurf: public Tsurf
{
 real R,R2;
 static TV3 cosv;
 TV3 norm;
public:
 Tcylsurf(){}
 Tcylsurf(const Tline& axis, real r)
 {
  R=r;
  local_crd.move_to(axis.p0);
  local_crd.rotate_x_to(axis.Omega(0));
  local_crd.rotate_y_to(axis.Omega(1));
  local_crd.update();
  update();
 }
 inline int side(const TV3& x)const
 {
  static int result;
  static TV3 x_loc;
  static real d,dr;
  x_loc=local_crd.convert_to(x);
  d=sqr(x_loc(0))+sqr(x_loc(1));
  dr=d-R2;
  result=INTSIGN(dr);
  return result;
 }
 inline std::vector<real>& ipoint(const Tline& li)const
 {
     static std::vector<real> res0(0);
     static std::vector<real> res1(1);
     static std::vector<real> res2(2);
     static TV3 dr;
     static TV3 l;
     static real nl;
     static real ndr;
     static real ldr;
     static real dr2;
     static real ll;
     static real a;
     static real b;
     static real c;
     static real d;
     static real t1;
     static real t2;
     dr=li.p0-local_crd.inip();
     l=li.direct;
     nl=dot(l,norm);
     ldr=dot(l,dr);
     ndr=dot(norm,dr);
     dr2=dot(dr,dr);
     a=ll;
     b=-2.*(nl+ldr);
     c=dr2+ndr-R2;
     d=sqr(b)-4.*a*c;
     if(d<0)
     {
      return res0;
     }
     else if(d>0)
     {
      d=sqrt(d);
      a=0.5/a;
      t1=(-b+d)*a;
      t2=-(b+d)*a;
      res2[0]=t1;
      res2[1]=t2;
      return res2;
     }
     else
     {
      t1=-b*0.5/a;
      res1[0]=t1;
      return res1;
     }
 }
 inline void update()
 {
  R2=sqr(R);
  norm=local_crd.convert_to(cosv);
 }
};

#endif