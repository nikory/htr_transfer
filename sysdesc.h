#ifndef TOPKA_GEOM_H
#define TOPKA_GEOM_H


#include <vector>
#include <map>
#include <set>
#include "tv3.h"
#include "surface.h"
#include "Treg_zone.h"

/*class TV3;
class Tsurf;
class Treg_zone;
class Touter_cube;*/

struct sysbd
{
 std::set<TV3*> Points;
 std::set<TV3*> Directions;
 std::set<Tsurf*> Surfaces;
 std::set<Treg_zone*> Reg_zones;
 Touter_cube *Outer_cube;
 ~sysbd(){}
};

extern sysbd GLOBAL_SYS;

#endif

