#ifndef TOPKA_GEOM_H
#define TOPKA_GEOM_H


#include <vector>
#include <map>
#include <set>
#include "zone_index.h"
#include "surface.h" 
#include "domain.h"
#include "line.h"
#include "Treg_zone.h"
#include "Tzone_tracker.h"
#include <exception>
#include <iostream>


#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

class VolumeZone;
class SurfaceZone;

class rubik
{
 friend class boost::serialization::access;
 template<class Arch>
 inline void serialize(Arch &a,int)
 {
  using namespace std;
  try
  {
   a.register_type<Treg_zone_surf>();
   a.register_type<Treg_zone_vol>();
   a.register_type<VolumeZone>();
   a.register_type<SurfaceZone>();
   a & ARC_ADAPTOR(outer_cube);
   a & ARC_ADAPTOR(rzs_set);
   a & ARC_ADAPTOR(rzv_set);
   a & ARC_ADAPTOR(rz_set);
   a & ARC_ADAPTOR(rzs_vec);
   a & ARC_ADAPTOR(rzv_vec);
   a & ARC_ADAPTOR(rz_vec);
   a & ARC_ADAPTOR(zone_index_com);
   a & ARC_ADAPTOR(zone_index_surf);
   a & ARC_ADAPTOR(zone_index_vol);  
   a & ARC_ADAPTOR(outer_cube_zone);   
  }
  catch(exception& e)
  {
   cout<<e.what()<<endl;
  }
  catch(...)
  {
   cout<<"some";
  }
 }
protected:
 Touter_cube& outer_cube;
 Treg_zone_vol outer_cube_zone;
 void create_surfaces();
 void create_region();
 void create_volz();
 void create_regzones();
 void make_index_map();
 void set_my_volume();
public:
 typedef std::set<Treg_zone*> rz_set_t;
 typedef std::set<Treg_zone_surf*> rzs_set_t;
 typedef std::set<Treg_zone_vol*> rzv_set_t;
 typedef std::vector<Treg_zone*> rz_vec_t;
 typedef std::vector<Treg_zone_surf*> rzs_vec_t;
 typedef std::vector<Treg_zone_vol*> rzv_vec_t;
 rz_set_t rz_set;
 rzs_set_t rzs_set;
 rzv_set_t rzv_set;
 rz_vec_t rz_vec;
 rzs_vec_t rzs_vec;
 rzv_vec_t rzv_vec;
 zone_index<Treg_zone> zone_index_com;
 zone_index<Treg_zone_surf> zone_index_surf;
 zone_index<Treg_zone_vol> zone_index_vol;
 rubik();
 ~rubik();
 const Treg_zone* find(const TV3& pt)const;
 const Touter_cube& get_outer_cube()const;
 const Treg_zone_vol& get_outer_cube_zone()const;
 Treg_zone_vol& get_outer_cube_zone();
 real estimate_full_Sq();
 real estimate_full_Vol();
 void index_zones();
 inline void set2vec()
 {
  rzs_vec.clear();
  rz_vec.clear();
  rz_vec.clear();
  rzs_set_t::iterator s,sb,se;
  rzv_set_t::iterator v,vb,ve;
  rz_set_t::iterator z,zb,ze;
  sb=rzs_set.begin();
  se=rzs_set.end();
  vb=rzv_set.begin();
  ve=rzv_set.end();
  zb=rz_set.begin();
  ze=rz_set.end();
  for(s=sb;s!=se;s++)rzs_vec.push_back(*s);
  for(v=vb;v!=ve;v++)rzv_vec.push_back(*v);
  for(z=zb;z!=ze;z++)rz_vec.push_back(*z);
 }
};

#endif