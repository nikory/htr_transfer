#ifndef SERIALIZE_INC_H
#define SERIALIZE_INC_H

//serialization basics
#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/assume_abstract.hpp>
//serialization archives
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
//serialization stl containers
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/valarray.hpp>

#define BIN_IO

#ifdef XML_IO
 #define ARC_ADAPTOR(X) BOOST_SERIALIZATION_NVP(X)
 #define ARC_ADAPTOR_STR(str,X) boost::serialization::make_nvp(str,X)
 #define IARC boost::archive::xml_iarchive
 #define OARC boost::archive::xml_oarchive
#elif defined TXT_IO
 #define ARC_ADAPTOR(X) X
 #define ARC_ADAPTOR_STR(str,X) X
 #define IARC boost::archive::text_iarchive
 #define OARC boost::archive::text_oarchive
#elif defined BIN_IO
 #define ARC_ADAPTOR(X) X
 #define ARC_ADAPTOR_STR(str,X) X
 #define IARC boost::archive::binary_iarchive
 #define OARC boost::archive::binary_oarchive
#endif

#endif