#ifndef TRANSF_VECTOR_H
#define TRANSF_VECTOR_H

#include <iostream>
#include <iomanip>
#include <valarray>
#include "TV3.h"
#include "zone_index.h"

#include "serialize_inc.h"

template <class TZone>
class transf_vector: public std::valarray<real>
{
 friend class boost::serialization::access;
 template <class Arch>
 void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("valarray",boost::serialization::base_object<std::valarray<real> >(*this));
  a & ARC_ADAPTOR_STR("index_map",m);
 }
public:
 typedef typename zone_index<TZone> zone_index_t;
 typedef typename std::valarray<real> derived_t;
protected:
 zone_index_t* m;
 inline void resize()
 {
  std::valarray<real>::resize(m->size(),0.);
 }
public:
 inline void set_index(zone_index_t& i)
 {
  m=&i;
  resize();
 }
 inline std::valarray<real>& v()
 {
  return static_cast<std::valarray<real>&>(*this);
 }
 inline const std::valarray<real>& v()const
 {
  return static_cast<const std::valarray<real>&>(*this);
 }
 inline real operator()(TZone* i)const
 {
  int ii=(*m)[i];
  return (*this)[ii];
 }
 inline real& operator()(TZone* i)
 {
  int ii=(*m)[i];
  return (*this)[ii];
 }
 inline TZone* operator()(int i)const{return (*m)[i];}
 inline transf_vector<TZone>& operator=(const transf_vector<TZone>& x)
 {
  m=x.m;
  resize();
  (derived_t&)(*this)=(const derived_t&)x;
  return *this;
 }
 inline void put_to_object_props(int shift)
 {
  int i,n;
  n=m->size();
  char* pz;
  for(i=0;i<n;i++)
  {
   pz=(char*)&((*this)(i)->props());
   *(real*)(pz+shift)=(*this)[i];
  }
 }
 real sum()const
 {
  real res=0;
  for(int i=0;i<size();i++)
  {
   res+=this->v()[i];
  }
  return res;
 }
};

template<>
inline real& transf_vector<Treg_zone>::operator ()(Treg_zone* i)
{
 int ii;
// ii=i->zone_index;
 ii=m->sfind_index(i);
 return (*this)[ii];
}
template<>
inline real& transf_vector<Treg_zone_vol>::operator ()(Treg_zone_vol* i)
{
 int ii;
// ii=i->vol_index;
 ii=m->sfind_index(i);
 return (*this)[ii];
}
template<>
inline real& transf_vector<Treg_zone_surf>::operator ()(Treg_zone_surf* i)
{
 int ii;
// ii=i->surf_index;
 ii=m->sfind_index(i);
 return (*this)[ii];
}
template<>
inline real transf_vector<Treg_zone>::operator ()(Treg_zone* i)const
{
 int ii;
// ii=i->zone_index;
 ii=m->sfind_index(i);
 return (*this)[ii];
}
template<>
inline real transf_vector<Treg_zone_vol>::operator ()(Treg_zone_vol* i)const
{
 int ii;
// ii=i->vol_index;
 ii=m->sfind_index(i);
 return (*this)[ii];
}
template<>
inline real transf_vector<Treg_zone_surf>::operator ()(Treg_zone_surf* i)const
{
 int ii;
// ii=i->surf_index;
 ii=m->sfind_index(i);
 return (*this)[ii];
}

template<class T>
inline std::ostream& operator<<(std::ostream& s, const transf_vector<T>& m)
{
 int i;
 for(i=0;i<m.size();i++)
 {
  s<<m[i]<<endl;
 }
 return s;
}
#endif