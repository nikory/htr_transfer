#ifndef TV3_H
#define TV3_H

#include <include/tmatr.h>
#include <string>
#include <iostream>

const int SD=3;
typedef double real;
typedef MatNM<real,SD,SD> TM3;

typedef MatNM<real,SD,1> TV3;

#include "serialize_inc.h"

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive & a, TV3 & v, const unsigned int version)
{
#ifdef XML_IO
 for(int i=0;i<3;i++)
  a & boost::serialization::make_nvp("TV3.data",v.data[i]);
#else
 for(int i=0;i<3;i++)
  a & v.data[i];
#endif
}

template<class Archive>
void serialize(Archive & a, TM3 & v, const unsigned int version)
{
#ifdef XML_IO
 for(int i=0;i<9;i++)
  a & boost::serialization::make_nvp("TM3.data",v.data[i]);
#else
 for(int i=0;i<9;i++)
  a & v.data[i];
#endif
}

}
}

/*class TV3: public MatNM<real,SD,1>
{
public:
 TV3(real,real,real);
}; */

typedef MatNM<real,2,1> TV2;
#define SIGN(x) (x>0?1:(x<0?-1:0))

template <class real,int N>
inline bool operator < (MatNM<real,N,1>& v1,MatNM<real,N,1>& v2)
{
 bool res=false;
 int i;
 for(i=0;i<N;i++)
 {
  res&&=v1(i)<v2(i); 
 }
 return res;
}

//! �������� ������� v �� ������������ ���������, ���������������� ����������� k
inline TV3 proection(const TV3& v, int k, real d=0)
{
 int i,j;
 TV3 res;
 for(i=0,j=0;i<SD;i++)
 {
  if(i!=k)
  {
   res(i)=v(i);
  }
  else
  {
   res(i)=d;
  }
 }
 return res;
}

static TV3 INFPT={1e20,1e20,1e20};
static const TV3 EVX={1,0,0};
static const TV3 EVY={0,1,0};
static const TV3 EVZ={0,0,1};
static const TV3 OOO={0,0,0};
static TV3 ORTS[]={EVX,EVY,EVZ};

template<class T1, class T2>
std::pair<T1,T2> tie(T1& x1, T2& x2)
{
 return std::pair<T1,T2>(x1,x2);
};

template <class T>
class Tsmart_ref
{
 T* p;
public:
 Tsmart_ref():p(0){}
 Tsmart_ref(const Tsmart_ref<T>& sr):p(sr.p){}
 Tsmart_ref(T& r):p(&r){}
 bool operator<(const Tsmart_ref<T>& r)const{return *p<*(r.p);}
 bool idendical(const Tsmart_ref<T>& r)const{return p==r.p;}
 Tsmart_ref<T>& operator=(T& r){p=&r; return *this;}
 operator T&()
 {
  return *p;
 }
 operator const T&()const
 {
  return *p;
 }
 T* operator ->()const{return p;}
};



#endif