#ifndef ran_f_h
#define ran_f_h

#include <iostream>
#include <math.h>
#include <map>
#include "interpolation.h"
#include "phys_const.h"
#include "hqrnd.h"

static hqrndstate STATIC_RND_STATE;

inline void init_rndgen_state()
{
 hqrndrandomize(STATIC_RND_STATE);
}

inline double random_e()
{
// int i=rand();
// double lNewVariable = (double)i/32768;
// std::cout<<i<<' '<<lNewVariable<<std::endl;
// return lNewVariable;
 return hqrnduniformr(STATIC_RND_STATE);
}
inline double random(double a,double b)
{
 double res;
 res=a+random_e()*(b-a);
 return res;
 //return (double)rand()/32768*(b-a)+a;
}

class random_t
{
public:
 inline double operator()(double a, double b)const
 {
  return random(a,b);
 }
};

double gasdev();
//! ��������� ��������� ����� � ���������� ��������������.
class gauss
{
 double a,b;
public:
 gauss(){b=1;a=1;}
 gauss(double x0,double sigma){b=x0;a=sigma;}
 void set(double m,double d){b=m;a=d;}
 inline double operator()(){return a*gasdev()+b;}
};


class plank
{
 static double x[101],f[101];
 static linear_interpolation_1d<double> li;
 double Temper;
public:
 plank()
 {
  if(li.size()==0)
  {
   li.set_data(x,f,sizeof(x)/sizeof(double));
  }
 }
 plank(double t):Temper(t){}
 void set(double t){Temper=t;}
 double operator()()
 {
  double res;
  double ld=random_e();
  if(ld<=0.95)
  {
   res=li(ld);
  }
  else
  {
   res=4.608752070607724+0.6766752869977634/sqrt(1.-ld);
  }
  return res;
 }         
 double get_omega(double T)
 {
  double res=(*this)();
  res*=T*BoltsmanK;
  res/=DirakH;
  return res;
 }
 double get_nu(double T)
 {
  double res=(*this)();
  res*=T*BoltsmanK;
  res/=PlankH;
  return res;
 }
 double get_enr(double T)
 {
  return (*this)()*BoltsmanK*T;
 }
 double get_lam(double T)
 {
  return LigthVelocity/get_nu(T);
 }
};

class expd
{
 double lam;
public:
 expd(double p=1.):lam(p){}
 void set(double p){lam=p;}
 double operator()()
 {
     double rn=random_e();
     double rn1=random(0.,1.);
     int sgn=rn1>0?1:rn1<0?-1:0;
     double lNewVariable;
     lNewVariable = -log(1.-rn)/lam*sgn;
     return lNewVariable;
 }
};


class rnd_gen_hist
{
 std::vector<double> vx;
public:
 template<class R>
 void set_data(R x, int n)
 {
  int i;
  double s;
  for(i=0;i<n;i++)s+=x[i];
  vx.resize(n+1);
  vx[0]=0;
  for(i=1;i<=n;i++)
  {
   vx[i]=x[i]/s+vx[i-1];
  }
 }
 int operator()()const
 {
  double rnd=random_e();
  std::vector<double>::const_iterator l,b;
  b=vx.begin();
  l=std::lower_bound(vx.begin(),vx.end(),rnd);
  return l-b;
 }
};

#include "serialize_inc.h"
/*! ����� ��� ������ ������� �� ����������� �������������, � ������� ������ ��������� �����.
���������� ��� ���������� � ���� Hist=map<some_key,point>. ��� ����� ������ ���� ���������� 
�������� ������������ � ���������.*/

template <class key_type, class val_type>
class get_rnd_distr_hist
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a, int version)
 {
  a & ARC_ADAPTOR(my_hist);
  a & ARC_ADAPTOR(max_sum);
 }
protected:
 typedef std::map<key_type,val_type> Hist;
 Hist my_hist;
 val_type max_sum;
public:
 const get_rnd_distr_hist<key_type, val_type>& operator()(const Hist& h)
 {
  Hist::const_iterator hb,he,hi;
  hb=h.begin();
  he=h.end();
  max_sum=0.;
  for(hi=hb;hi!=he;hi++)
  {
   max_sum+=hi->second;
   my_hist[hi->first]=max_sum;
  }
  return *this;
 }
 key_type operator()()const
 {
  key_type res=0;
  while(res==0)
  {
  Hist::mapped_type q=random(0.,max_sum);
  Hist::const_iterator hb,he,hi;
  hb=my_hist.begin();
  he=my_hist.end();
  for(hi=hb;hi!=he;hi++)
  {
   if(q<hi->second)
   {
    res=hi->first;
    break;
   }
  }
  }
  return res;
 }
};

#endif
