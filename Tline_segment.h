#include <boost/shared_ptr.hpp>
#include <vector>
#include "TV3.h"
#include "line.h"

class Treal_condition
{
public:
 virtual bool operator()(real x)=0;
};

class Treal_negation: public Treal_condition
{
 Treal_condition* x;
public:
 Treal_negation(Treal_condition* a):x(a){}
 bool operator()(real a){return !(*x)(a);}
};

class Treal_gt: public Treal_condition
{
 real x0;
public:
 Treal_gt(real x=0):x0(x){}
 bool operator()(real x){return x>x0;}
};

class Treal_biarg: public Treal_condition
{
 Treal_condition *x1,*x2;
public:
 Treal_biarg(Treal_condition* _x1,Treal_condition* _x2):x1(_x1),x2(_x2){}
};

class Treal_and: public Treal_biarg
{
public:
 bool operator()(real x){return (*x1)(x)&&(*x2)(x);}
};

class Treal_or: public Treal_biarg
{
public:
 bool operator()(real x){return (*x1)(x)||(*x2)(x);}
};

class Treal_complement: public Treal_biarg
{
public:
 bool operator()(real x){return (*x1)(x)&&(!(*x2)(x));}
};


boost::shared_ptr<Treal_and> operator&&(Treal_condition& a, Treal_condition& b)
{
 boost::shared_ptr<Treal_and> res(new Treal_and(&a,&b);
 return res;
}
boost::shared_ptr<Treal_or> operator||(Treal_condition& a, Treal_condition& b)
{
 boost::shared_ptr<Treal_or> res(new Treal_or(&a,&b);
 return res;
}
boost::shared_ptr<Treal_complement> operator-(Treal_condition& a, Treal_condition& b)
{
 boost::shared_ptr<Treal_complement> res(new Treal_complement(&a,&b);
 return res;
}
boost::shared_ptr<Treal_negation> operator-(Treal_condition& a)
{
 boost::shared_ptr<Treal_negation> res(new Treal_negation(&a);
 return res;
}