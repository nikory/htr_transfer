#ifndef CONDITION_H
#define CONDITION_H

#ifndef __MAKECINT__
#include <set>
#include <map>
#include "TV3.h"
#include "line.h"
#include "surface.h"
#include "Tlogical_automat.h"
#endif

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/assume_abstract.hpp>

class Tcondition;
class Tsurf_ge;
class Tnegation;
class Tbi_arg_condition;
class Tand;
class Tor;

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Tcondition)



class Tcondition
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
 }
public:
 Tcondition(){}
 virtual bool operator()(const TV3& )const{return false;}//=0;
 virtual bool operator()(const real* arr)const
 {
  TV3 v;
  v=arr;
  return (*this)(v);
 }//=0;
 virtual void extract_surf(std::set<Tsurf*>&){}//=0;
 virtual void extract_inters(Tline_stp& v, const Tline&){}//=0;
 virtual bool contains(const Tsurf* s)const{return false;}//=0;
};

class Tsurf_ge: public Tcondition
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
  a.register_type<Tplanesurf>();
  a & ARC_ADAPTOR_STR("Tcondition",boost::serialization::base_object<Tcondition>(*this));
  a & ARC_ADAPTOR(s);
 }
 Tsurf* s;
public:
 Tsurf_ge():s(0){}
 Tsurf_ge(Tsurf& q):s(&q){}
 bool operator()(const TV3& v)const{return s->side(v)==1;}
 void extract_surf(std::set<Tsurf*>& v){v.insert(s);}
 void extract_inters(Tline_stp& v, const Tline& l)
 {
  if(s->last_line_id != l.id)
  {
   s->ipoint_info(l);
   s->last_line_id=l.id;
  }
  Tsurf::iiptcon_t& ipv(*s);
  int i,n=ipv.size();
  for(i=0;i<n;i++)
  {
   v.v.push_back(Tpoint_ref(ipv[i].point,Tpoint_ref::ask_type(ipv[i].nlprod),s));
  }
  if(v.v.size()==0)
  {
   int i=0;
  }
  v.update();
 }
 bool contains(const Tsurf* p)const{return p==s;}
};

class Tnegation: public Tcondition
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
  a.register_type<Tsurf_ge>();
  a.register_type<Tnegation>();
  a.register_type<Tand>();
  a.register_type<Tor>();
  a & ARC_ADAPTOR_STR("Tcondition",boost::serialization::base_object<Tcondition>(*this));
  a & ARC_ADAPTOR(x);
 }
 Tcondition *x;
public:
 Tnegation():x(0){}
 Tnegation(Tcondition& _x):x(&_x){}
 bool operator()(const TV3& v)const{return !(*x)(v);}
 void extract_surf(std::set<Tsurf*>& v){x->extract_surf(v);}
 void extract_inters(Tline_stp& v, const Tline& l)
 {
  x->extract_inters(v,l);
  v.sets(!v.fs);
 }
 bool contains(const Tsurf* p)const{return x->contains(p);}
};

class Tbi_arg_condition:public Tcondition
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
  a & ARC_ADAPTOR_STR("Tcondition",boost::serialization::base_object<Tcondition>(*this));
 }
protected:
 Tcondition* x1,*x2;
public:
 Tbi_arg_condition():x1(0),x2(0){}
 Tbi_arg_condition(Tcondition& x1_,Tcondition& x2_):x1(&x1_),x2(&x2_){}
 void extract_surf(std::set<Tsurf*>& v){x1->extract_surf(v);x2->extract_surf(v);}
 bool contains(const Tsurf* p)const{return x1->contains(p)||x2->contains(p);}
};

//! ��� ||
class Tor:public Tbi_arg_condition
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
  a.register_type<Tsurf_ge>();
  a.register_type<Tnegation>();
  a.register_type<Tand>();
  a.register_type<Tor>();
  a & ARC_ADAPTOR_STR("Tbi_arg_condition",boost::serialization::base_object<Tbi_arg_condition>(*this));
  a & ARC_ADAPTOR(x1);
  a & ARC_ADAPTOR(x2);
 }
public:
 Tor():Tbi_arg_condition(){}
 Tor(Tcondition& x1_,Tcondition& x2_):Tbi_arg_condition(x1_,x2_){}
 bool operator ()(const TV3& v)const{return (*x1)(v)||(*x2)(v);}
 void extract_inters(Tline_stp& v, const Tline& l)
 {
  Tline_stp v1,v2;
  Tor_automat automat;
  x1->extract_inters(v1,l);
  x2->extract_inters(v2,l);
  automat(v1,v2,v);
 }
};

//! � &&
class Tand:public Tbi_arg_condition
{
 friend class boost::serialization::access;
 template<class Arch>
 void serialize(Arch &a,int)
 {
  a.register_type<Tsurf_ge>();
  a.register_type<Tnegation>();
  a.register_type<Tand>();
  a.register_type<Tor>();
  a & ARC_ADAPTOR_STR("Tbi_arg_condition",boost::serialization::base_object<Tbi_arg_condition>(*this));
  a & ARC_ADAPTOR(x1);
  a & ARC_ADAPTOR(x2);
 }
public:
 Tand():Tbi_arg_condition(){}
 Tand(Tcondition& x1_,Tcondition& x2_):Tbi_arg_condition(x1_,x2_){}
 bool operator ()(const TV3& v)const
 {
  return (*x1)(v)&&(*x2)(v);
 }
 void extract_inters(Tline_stp& v, const Tline& l)
 {
  Tline_stp v1,v2;
  Tand_automat automat;
  x1->extract_inters(v1,l);
  x2->extract_inters(v2,l);
  automat(v1,v2,v);
 }
};

extern std::set<Tcondition*> FORMULASET;  

#define DECL_FORMULASET std::set<Tcondition*> FORMULASET

inline void kill_formulas()
{
 std::set<Tcondition*>::iterator it,b,e;
 b=FORMULASET.begin();
 e=FORMULASET.end();
 for(it=b;it!=e;it++)
 {
  delete *it;
 }
}

inline Tcondition& operator*(Tcondition& arg1,Tcondition& arg2)
{
 Tcondition* res=new Tand(arg1,arg2);
 FORMULASET.insert(res);
 return *res;
}

inline Tcondition& operator+(Tcondition& arg1,Tcondition& arg2)
{
 Tcondition* res=new Tor(arg1,arg2);
 FORMULASET.insert(res);
 return *res;
}

inline Tcondition& operator-(Tcondition& arg)
{
 Tnegation *res=new Tnegation(arg);
 FORMULASET.insert(res);
 return *res;
}

inline Tcondition& operator/(Tcondition& arg1,Tcondition& arg2)
{
 Tcondition *res=&(arg1*(-arg2));
 FORMULASET.insert(res);
 return *res;
}

inline Tcondition& operator-(Tcondition& arg1,Tcondition& arg2)
{
 Tcondition *res=&(arg1*(-arg2));
 FORMULASET.insert(res);
 return *res;
}


inline Tcondition& operator*(Tsurf& s1, Tsurf& s2)
{
 Tcondition *sg1=&(Tcondition&)s1,*sg2=&(Tcondition&)s2;
 return (*sg1) * (*sg2);
}
inline Tcondition& operator*(Tsurf& s1, Tcondition& c2)
{
 Tcondition *sg1=&(Tcondition&)s1;
 return (*sg1) * c2;
}
inline Tcondition& operator*(Tcondition& c2, Tsurf& s1)
{
 Tcondition *sg1=&(Tcondition&)s1;
 return c2*(*sg1);
}

inline Tcondition& operator+(Tsurf& s1, Tsurf& s2)
{
 Tcondition *sg1=&(Tcondition&)s1,*sg2=&(Tcondition&)s2;
 return (*sg1) + (*sg2);
}
inline Tcondition& operator+(Tsurf& s1, Tcondition& c2)
{
 Tcondition *sg1=&(Tcondition&)s1;
 return (*sg1) + c2;
}
inline Tcondition& operator+(Tcondition& c2, Tsurf& s1)
{
 Tcondition *sg1=&(Tcondition&)s1;
 return c2+(*sg1);
}
inline Tcondition& operator-(Tsurf& s1, Tsurf& s2)
{
 Tcondition *sg1=&(Tcondition&)s1,*sg2=&(Tcondition&)s2;
 return (*sg1) - (*sg2);
}
inline Tcondition& operator-(Tsurf& s1, Tcondition& c2)
{
 Tcondition *sg1=&(Tcondition&)s1;
 return (*sg1) - c2;
}
inline Tcondition& operator-(Tcondition& c2, Tsurf& s1)
{
 Tcondition *sg1=&(Tcondition&)s1;
 return c2 - (*sg1);
}

inline Tcondition& operator-(Tsurf& s)
{
 return -(Tcondition&)s;
}
#endif