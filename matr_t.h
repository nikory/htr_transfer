#ifndef MATR_T_H
#define MATR_T_H

#include <map>
#include "Treg_zone.h"
#include "transf_operator.h"
#include "transf_vector.h"

/*typedef transf_operator<Treg_zone> zzReal_matr_t;
typedef transf_operator<Treg_zone_surf> ssReal_matr_t;
typedef transf_operator<Treg_zone_vol> vvReal_matr_t; */


/*#define zzReal_matr_t transf_operator<Treg_zone> 
#define ssReal_matr_t transf_operator<Treg_zone_surf> 
#define vvReal_matr_t transf_operator<Treg_zone_vol>   */

/*#define zzReal_map_t transf_vector<Treg_zone>;
#define ssReal_map_t transf_vector<Treg_zone_surf>;
#define vvReal_map_t transf_vector<Treg_zone_vol>;*/

typedef transf_operator<Treg_zone>       zzReal_matr_t;
typedef transf_operator<Treg_zone_surf>  ssReal_matr_t;
typedef transf_operator<Treg_zone_vol>   vvReal_matr_t;

typedef transf_vector<Treg_zone> zzReal_map_t;
typedef transf_vector<Treg_zone_surf> ssReal_map_t;
typedef transf_vector<Treg_zone_vol> vvReal_map_t;


#endif
