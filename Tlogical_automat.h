#ifndef TLOGICAL_AUTOMAT_H
#define TLOGICAL_AUTOMAT_H

#include <algorithm>
#include <vector>
#include <iostream>
#include "TV3.h"
#include "surface.h"

class Tpoint_ref
{
public:
 enum inout {INLET, OUTLET};
 Tpoint_ref(){}
 Tpoint_ref(real& w):x(w){}
 Tpoint_ref(real& w, inout ty):x(w),type(ty){}
 Tpoint_ref(real& w, inout ty, Tsurf* q):x(w),type(ty),s(q){}
 Tsmart_ref<real> x;
 Tsurf* s;
 inout type;
 bool operator<(const Tpoint_ref& q)const{return x<q.x;}
 static inout ask_type(real a){return a<0 ? inout::OUTLET : inout::INLET;}
};

inline std::ostream& operator<<(std::ostream& q, const Tpoint_ref& r)
{
 q<<((real)(r.x));
 return q;
}

class Tline_stp
{
 Tpoint_ref::inout ty_l(bool b)
 {
  if(b)return Tpoint_ref::inout::OUTLET;
  else return Tpoint_ref::inout::INLET;
 }
public:
 std::vector<Tpoint_ref> v;
 void clear(){v.clear();}
 Tline_stp& operator=(std::vector<real>& q)
 {
  int i,n=q.size();
  v.reserve(n);
  v.clear();
  for(i=0;i<n;i++)
  {
   v.push_back(Tpoint_ref(q[i]));
  }
  return *this;
 }
 void sets(bool b)
 {
  int i,n,t;
  n=v.size();
  fs=b;
  for(i=0;i<n;i++)
  {
   v[i].type=ty_l(b);
   b=!b;
  }
  update();
 }
 bool fs;
 void update()
 {
  if(v.size()>0)
  {
   if(v.front().type==Tpoint_ref::inout::INLET)
   {
    fs=false;
   }
   else 
   {
    fs=true;
   }
  }
 }
 real length()const
 {
  if(!fs && v.back().type==Tpoint_ref::inout::OUTLET)
  {
   int i;
   int n=v.size();
   real res=0;
   for(i=0;i<n;i+=2)
   {
    res+=v[i+1].x-v[i].x;
   }
   return res;
  }
  else 
  {
   return 0;
  }
 }
 bool find(const Tsurf * s)
 {
  int i,n=v.size();
  for(i=0;i<n;i++)
  {
   if(v[i].s==s)return true;
  }
  return false;
 }
};

inline std::vector<real>& copy(std::vector<real>& r, const Tline_stp& l)
{
 int n=l.v.size(),i;
 r.clear();
 for(i=0;i<n;i++)
 {
  r.push_back(l.v[i].x);
 }
 return r;
}

inline std::ostream& operator<<(std::ostream& s, const Tline_stp& ls)
 {
  int i;
  s<<ls.fs<<std::endl;
  for(i=0;i<ls.v.size();i++)
  {
   s<<ls.v[i]<<"  ";
  }
  return s;
 }

//! ������� ��� �����������/����������� �������� �� ������ �������� (������� �����)
class Tlogical_automat
{
protected:
 struct branch_state
 {
  union {bool left,right;} bs;
  std::vector<Tpoint_ref>::const_iterator p,e; 
 };
 enum states {PP,MM,PM,MP};
 states state;
 void set_state(branch_state& s1, branch_state& s2)
 {
  if(s1.bs.left)
  {
   if(s2.bs.left)state=states::PP;
   else state=states::PM;
  }
  else
  {
   if(s2.bs.left)state=states::MP;
   else state=states::MM;
  }
  return;
 }
 void order(branch_state& s1, branch_state& s2)
 {
  if(*(s1.p) < *(s2.p)) std::swap(s1,s2);
 }
 void push_all(branch_state& s, Tline_stp& res)
 {
  for(;s.p!=s.e;s.p++)
  {
   res.v.push_back(*s.p);
  }
 }
 bool step(branch_state& s1, branch_state& s2)
 {
  s2.p++;
  s2.bs.left=!s2.bs.left;
  return s2.p!=s2.e;
 }
public:
 virtual void operator()(const Tline_stp& v1, const Tline_stp& v2, Tline_stp& res)=0;
};

//! ������� ��� ����������� �������� �� ������ ��������
class Tand_automat: public Tlogical_automat
{
/* bool step(branch_state& s1, branch_state& s2)
 {
  s2.p++;
  s2.bs.left=!s2.bs.left;
  return s2.p!=s2.e;
 }       */
public:
 void operator ()(const Tline_stp& v1, const Tline_stp& v2, Tline_stp& res)
 {
  if(v1.v.size()==0 || v2.v.size()==0)
  {
   res.v.clear();
   return;
  }
  else
  {
   branch_state s1,s2;
   s1.p=v1.v.begin();
   s2.p=v2.v.begin();
   s1.e=v1.v.end();
   s2.e=v2.v.end();
   s1.bs.left=v1.fs;
   s2.bs.left=v2.fs;
   //������� ���� ���� �� �������� �����
   do
   {
    //��������� ��������� � ���������� ������� (������ ����� ������)
    order(s1,s2);
    //���������� ��������� 
    set_state(s1,s2);
    //
    switch(state)
    {
     case states::PP:
      res.v.push_back(*s2.p);
      break;
     case states::PM:
      res.v.push_back(*s2.p);
      break;
    }
   }while(step(s1,s2));
   if(s2.bs.left)
   {
    push_all(s1,res);
   }
   res.update();
   return;
  }
 }
};

class Tor_automat: public Tlogical_automat
{
/* bool step(branch_state& s1, branch_state& s2)
 {
  bool res;
  res=Tlogical_automat::step(s1,s2);
  if(s1.p->x.idendical(s2.p->x))
  {
   res=step(s1,s2);
  }
  return res;
 }   */
public:
 void operator ()(const Tline_stp& v1, const Tline_stp& v2, Tline_stp& res)
 {
  if(v1.v.size()==0 && v2.v.size()==0)
  {
   res.v.clear();
   return;
  }
  if(v1.v.size()==0)
  {
   res=v2;
   return;
  }
  if(v2.v.size()==0)
  {
   res=v1;
   return;
  }
  branch_state s1,s2;
  s1.p=v1.v.begin();
  s2.p=v2.v.begin();
  s1.e=v1.v.end();
  s2.e=v2.v.end();
  s1.bs.left=v1.fs;
  s2.bs.left=v2.fs;
  //������� ���� ���� �� �������� �����
  do
  {
   //�� ��������� ����������� �����
   //��������� ��������� � ���������� ������� (������ ����� ������)
   order(s1,s2);
   //���������� ��������� 
   set_state(s1,s2);
   //
   switch(state)
   {
    case states::MP:
     if(s1.p->x.idendical(s2.p->x))
     {
      step(s2,s1);
      continue;
     }
     res.v.push_back(*s2.p);
     break;
    case states::MM:
     res.v.push_back(*s2.p);
     break;
   }
  }while(step(s1,s2));
  if(!s2.bs.left)
  {
   push_all(s1,res);
  }
  res.update();
  return;
 }
};

#endif