### What is this repository for? ###

This repository contans the sources of the code to model the radiation heat transfer in TPP furnaces.

The code makes possible to model the radiation power distribution in furnaces of arbitrary geometry using the zonal transport approach.
The code uses the Monte-Carlo technique to calculate the geometrical integrals in the
zonal model of the furnace and to take in account the some radiation processes such as absorption and dispersion.

The first part of the radiation code is consists of the geometric module and ray tracing-and-counting algorithm. 
That part of the code works offline to prepare the mathematical model of the furnace using in FSS. The furnace volume is 
describing as a union of the control zones each of them are describing as a Boolean formula combining the surfaces limiting the zone.
The ray tracing-and-counting algorithm generates the random line inside the furnace volume and fixes some values relating with that
ray and zones which it intersects. As a result, we have the statistical estimations of the some geometric integrals related with pair
or triplets of zones. This is the mutual areas between two arbitrary zones, and mean lengths of rays from zone A to zone B and intersecting zone C.

Using such statistically estimated integrals we can to create the effective linear model of the radiation transport model in the furnace
inner volume. In that model we can to reflect some important processes, such as absorption and dispersion, and can to take into account 
the influence on it of the temperatures, gas components and other physical parameters inside the furnace.

The mathematics is described in documents contained in _doc_ folder in source. Only in Russian now, I'm sorry :(

To develop that radiation transport code, the object-oriented approach was used. The code was written on C++ and using the CINT interpreter 
(see https://root.cern.ch/cint) as input language to define the furnace geometry and ray tracing parameters. The model defining examples is stored in 
_examples_ folder.